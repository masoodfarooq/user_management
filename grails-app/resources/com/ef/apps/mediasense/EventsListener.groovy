package com.ef.apps.mediasense
import com.ef.apps.mediasense.ResponseMapper.SubscriptionEventsResponseMapper
import com.ef.apps.mediasense.Utils.LogsManagement
import com.ef.apps.mediasense.recordings.Recordings
import com.ef.apps.recordings.CheckVoiceModelService
import com.ef.apps.recordings.MediaFilesGeneratorService
import user_management.TestingService

import javax.servlet.http.HttpServletRequest
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.Context

@Path('/MediaSenseEvents/SessionEvents')
class EventsListener {
    def checkVoiceModelService=new CheckVoiceModelService();

    @GET
    @Produces("text/html")
    public String getRequest(@Context HttpServletRequest request) {
        //LogsManagement.LogInfoMesg("Get Request Made from: " + request.getRemoteAddr());
        println "Get Method is Invoked...!!!"
        return ("Get Request Made from: " + request.getRemoteAddr());
    }

    @POST
    @Consumes("application/json")
    public String postRequest(String message, @Context HttpServletRequest request) throws IOException {
            String sessionState,sessionId,agentExtension,userExtension

        try {

            //Just to trim the response message (remove additional spaces if exists).
            message = message.trim();
            long req_id = System.currentTimeMillis();
            //Log this message as soon we get this
            LogsManagement.LogInfoMesg("MediaSense Notification with: \r\n Actual Data: " + message);

            //Mapping Mediasense Event Response
            Recordings MediaSenseSession = SubscriptionEventsResponseMapper.MapSessionResponse(message,req_id);

            sessionState = MediaSenseSession.getSessionState();
            sessionId = MediaSenseSession.getSessionId();
            agentExtension = MediaSenseSession.getParTo();
            userExtension = MediaSenseSession.getParFrom();

           // println "session State: "+sessionState+" session Id: "+sessionId;
            if(sessionState!=null && sessionId!=null && agentExtension!=null && userExtension!=null){
                LogsManagement.LogInfoMesg("Extracted sessionState:" + sessionState+ " and sessionId: "+sessionId );

                if(sessionState.equalsIgnoreCase("active")){
                    LogsManagement.LogInfoMesg("INFO: Going to Process Recording: " + MediaSenseSession.sessionId + " for Voice Biometrics, with user extension: "+userExtension+ " and agent extension: "+agentExtension)
                  //  mediaFilesGeneratorService?.generatingFiles(sessionId,deviceReference)
        // commenting above line because now beofore generating media files, we have to check if voice model is there

                    checkVoiceModelService.process(sessionId, userExtension, agentExtension)

                }else{
                    LogsManagement.LogInfoMesg("INFO: Session State is NOT ACTIVE!!!!")
                }
            }else{
                LogsManagement.LogExceptionMesg("Exception!!! SessionId,sessionState,userExtension or agentExtension is null. please check SubscriptionEventsResponseMapper.groovy for their values");
            }

        }

        catch (Exception ex) {
            println "Exception :"+ex;
            LogsManagement.LogExceptionMesg("Exception While Processing Mediasense Notification POST Request: " + message, ex)

        }
    }

}