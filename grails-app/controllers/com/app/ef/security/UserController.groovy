package com.app.ef.security
import grails.converters.JSON
import grails.rest.RestfulController
import grails.transaction.Transactional
import groovy.json.JsonSlurper
import org.apache.shiro.crypto.hash.Sha256Hash

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class UserController extends RestfulController<User> {
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: ["POST","GET"],edit:["get"], update: "PUT", delete: "DELETE"]

    def index() {
        def jsonSorting = new JsonSlurper().parseText(params.sorting)
        def jsonFiltering = new JsonSlurper().parseText(params.filter)
        def resultList = [:]
        def userCount = 0
        def query = "From User u  "
        if(jsonFiltering.username)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.username LIKE '%${jsonFiltering.username}%'"
            else
                query += " WHERE u.username LIKE '%${jsonFiltering.username}%'"

        }
        if(jsonFiltering.fullName)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.fullName LIKE '%${jsonFiltering.fullName}%'"
            else
                query += " WHERE u.fullName LIKE '%${jsonFiltering.fullName}%'"

        }
        if(jsonFiltering.email)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.email LIKE '%${jsonFiltering.email}%'"
            else
                query += " WHERE u.email LIKE '%${jsonFiltering.email}%'"

        }
        if(jsonFiltering.isActive)  {
            def isActive = true
            if(jsonFiltering.isActive.toLowerCase().equals("yes"))
               isActive = true
            else
                isActive = false

            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.isActive = '${isActive}'"
            else
                query += " WHERE u.isActive = '${isActive}'"

        }
        userCount =  User.executeQuery("SELECT COUNT(*) "+query)[0]
        if(jsonSorting.email)
            query += " ORDER BY u.email " + jsonSorting.email
        else if(jsonSorting.username)
            query += " ORDER BY u.username " + jsonSorting.username
        else if(jsonSorting.fullName)
            query += " ORDER BY u.fullName " + jsonSorting.fullName
        else if(jsonSorting.isActive)
            query += " ORDER BY u.isActive " + jsonSorting.isActive

        def userListFiltered =  User.findAll(query,[ max:params.count as int ,
                                                                     offset:( ( params.page as int  ) - 1 ) * (params.count as int ) ])
        resultList.put("usersList", userListFiltered)
        def nameFiltered = false

        resultList.put("usersCount",userCount)

        render resultList as JSON

    }
    def listCount(){
        def listCount = [:]
        listCount.put("total",User.count)
        listCount.put("test",User.list())
        render listCount as JSON
    }
    @Transactional
    def activateOrBlockUser (){
        def userInstance = User.findById(params.id)
        def checked = params.getBoolean("checked")
         if(userInstance == null){
            render status:NOT_FOUND
        }
        else if(userInstance?.username.equals("admin"))
            render status:NOT_MODIFIED
        else{
            if(checked) {
                userInstance?.isActive = true
            }
            else{
                userInstance?.isActive = false

            }
            userInstance?.save()
            render status:OK
        }

    }
    def show(User userInstance) {
        respond userInstance
    }
    def usernameExist(){
        def username = params.username
         if(username){
             def user = User.findByUsername(username)
             if(user)
                 render status:FOUND
             else
                 render status:ACCEPTED
         }
        else
             render status:NOT_ACCEPTABLE

    }
    def emailExist(){
        String email = params.email
          println "email "+email.matches("[a-z0-9!#\$%&'*+/=?^_`{|}~.-]+@[a-z]+.com")
        if(email){
            def user = User.findByEmail(email)
            if(user)
                render status:FOUND
            else
                render status:ACCEPTED
        }
        else
            render status:NOT_ACCEPTABLE

    }
    @Transactional
    def save(User userInstance) {
        if (userInstance == null) {
            render status: NOT_FOUND
            return
        }

        if (!userInstance?.validate()) {
            respond userInstance ,[status: NOT_ACCEPTABLE]
            return
        }
        userInstance?.password = new Sha256Hash(userInstance?.password).toHex()


        userInstance?.save flush:true
        // User can edit his/her profile and has to visit the home page permission by default
        userInstance?.addToPermissions("user:edit:"+userInstance?.id)
        userInstance?.addToPermissions("user:show:"+userInstance?.id)
        userInstance?.addToPermissions("user:update:"+userInstance?.id)
        userInstance?.addToPermissions("user:changePassword"+userInstance?.id)
        userInstance?.addToPermissions("user:isAuthenticate")
        userInstance?.addToPermissions("home:*")
        respond userInstance, [status: CREATED]
    }
    def edit(User userInstance) {
        respond userInstance
    }
    @Transactional
    def update(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance?.hasErrors()) {
            respond userInstance?.errors, view:'edit'
            return
        }

        userInstance?.save flush:true

        withFormat {
            html {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'User.label', default: 'User'), userInstance?.id])
                redirect userInstance
            }
            '*'{ respond userInstance, [status: OK] }
        }
    }
    @Transactional
    def delete(User userInstance) {

        if (userInstance == null) {
            notFound()
            return
        }
         try{
             if(userInstance?.username == "admin")
                 render status:NOT_MODIFIED
            else{
                 userInstance?.delete flush:true
                 render status:OK
             }


         }catch (Exception ex){

             log.error "User Not Deleted : " + ex
             respond userInstance?.errors ,[status:NOT_ACCEPTABLE]
         }


        /*request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'User.label', default: 'User'), userInstance?.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }*/
    }
    @Transactional
    def deleteSelected(){
        def deletedContact = 0
        def notDeletedContact = 0
        def responseJson = [:]
        def deletedIds = params.getList("ids")
        for (id in deletedIds){
            try{
                def user = User.get(id)
                if(user){
                    if(user.username == "admin")
                        notDeletedContact++
                    else{
                        user.delete()
                        deletedContact++
                    }

                }

            }catch(Exception ex){
            log.error "User Not Deleted : " + ex
             notDeletedContact++
            }


        }
        responseJson.put("deletedUser",deletedContact)
        responseJson.put("notDeletedUser",notDeletedContact)
        respond responseJson
    }
    def viewPermissions = {
        def permissions = [:]
        def query = "From Permission s"

        def userPermissionList = []
        def userInstance = User.findById(params.id)
        userInstance?.permissions?.each {
            def uPerm = Permission.findByExpression(it)
            if (uPerm) {
                userPermissionList.add(uPerm)
            }
        }
        def roleUserPermission = []

        userInstance?.roles?.permissions?.each {
            it.each {
                def uPerm = Permission.findByExpression(it)
                if (uPerm) {
                    roleUserPermission.add(uPerm)
                }
            }
        }
        // permissionList = permissionList - userPermissionList
        int permissionListCount  = Permission.executeQuery("Select count(*)"+query)[0]
        def permissionList
        permissionList = Permission.findAll(query,[ max:params.count as int ,
                offset:( ( params.page as int  ) - 1 ) * (params.count as int ) ])
        permissions=[
                permissionList: permissionList,
                permissionListCount: permissionListCount,
                userPermissionList: userPermissionList,
                roleUserPermission:roleUserPermission,
                roleUserPermissionCount:roleUserPermission.size(),
                userPermissionTotal: userPermissionList.size()
       ]
        render permissions as JSON
    }
    def viewRoles = {
        def query = "FROM Role r"
        def roles = [:]
        def userInstance = User.findById(params.id)
        int roleListTotal = Role.executeQuery("Select count(*) "+query)[0]
        def roleList = Role.findAll(query,[ max:params.count as int ,
                offset:( ( params.page as int  ) - 1 ) * (params.count as int ) ])

        def userRoleList = userInstance?.roles
        roles = [
                roleList: roleList,
                userRoleList: userRoleList,
                userInstance: userInstance,
                roleListTotal: roleListTotal,
                userRoleTotal: userRoleList.size()
        ]
        render roles as JSON
    }
    @Transactional
    def assignAndRevokePermission (){
        def permission = Permission.findById(params.permissionId)
        def user = User.findById(params.userId)
        def checked =  params.boolean("checked")

        if(permission && user){
            if(checked)
                user.addToPermissions(permission.expression)
            else
                user.removeFromPermissions(permission.expression)
            render status: OK
        }  else
            notFound()
    }
    @Transactional
    def assignAndRevokeRole (){
        def role = Role.findById(params.roleId)
        def user = User.findById(params.userId)
        def checked =  params.boolean("checked")

        if(role && user){
            if(checked)
                user.addToRoles(role)
            else
                user.removeFromRoles(role)
            render status: OK
        }  else
            notFound()
    }
    @Transactional
    def changePassword (){
         def userId = params.id
        def returnMessage = [:]
        def currentPass = params.currentPassword
        def newPass = params.newPassword
        def userInstance = User.findById(userId)
        if(userInstance){
            if (new Sha256Hash(currentPass).toHex()== userInstance?.password) {
                        try{
                            userInstance?.password = new Sha256Hash(newPass).toHex()
                            userInstance?.save()
                        }catch (Exception ex){
                            println "Error in password update and Error is : " + ex.getMessage()
                            returnMessage = [message:"com.ef.cbr.user.passwordChange.failure.constraint",status:NOT_IMPLEMENTED]
                            render returnMessage as JSON

                        }
           }
            else{
                log.info " Current Password does Not match with old password "
                returnMessage = [message:"com.ef.cbr.user.password.not.found",status:NOT_FOUND]
               render  returnMessage as JSON
            }

        }
        else{
            log.info " User Not found "
            returnMessage = [message:"com.ef.cbr.user.not.found",status:NOT_FOUND]
            render  returnMessage as JSON
        }
        returnMessage = [message:"com.ef.cbr.label.updateSuccessfully",status:OK]
        render  returnMessage as JSON
    }
    def isAuthenticate(){
        def userData = [:]
       String username = params.username
       def user = User.findByUsername(username)
        if(user){
            userData = [
                    username:user.username,
                    id:user.id,
                    email:user.email,
                    userPermissions:user.permissions.join(";")
            ]    as JSON
           render userData
        }
        else
            notFound()
    }
    protected void notFound() {
         render status: NOT_FOUND

    }
}
