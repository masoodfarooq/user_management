package com.app.ef.security
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PermissionController {

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST"]

    def index() {
        def query = "From Permission s"
        def resultList = [:]
        int countPermissionFiltered  = Permission.executeQuery("Select count(*)"+query)[0]
        def permissionFiltered = Permission.findAll(query,[ max:params.count as int ,
                offset:( ( params.page as int  ) - 1 ) * (params.count as int ) ])

        resultList.put("permissionList",permissionFiltered)
        resultList.put("permissionCount",countPermissionFiltered)

        render resultList as JSON
    }


}
