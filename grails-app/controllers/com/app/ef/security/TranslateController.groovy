package com.app.ef.security
import com.app.ef.translate.Language
import com.app.ef.translate.Translation
import grails.converters.JSON

class TranslateController {
    def messageSource
    static defaultAction = "index"

    def testingService
    def index() {
        def languageId = params.lang
       def translationValues = [:]
       def translationText = Translation.findAllByLanguage(Language.findByLanguageCode(languageId))
       for(trans in translationText){
          translationValues.put(trans.translationKey,trans.text)

       }

       render translationValues as JSON
    }

  def testing(){
      println testingService.testing()
     // println processVoiceBiometricsRecService.testing()

      render "reached"
  }
}
