package com.app.ef.translate



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TranslationController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Translation.list(params), model:[translationInstanceCount: Translation.count()]
    }

    def show(Translation translationInstance) {
        respond translationInstance
    }

    def create() {
        respond new Translation(params)
    }

    @Transactional
    def save(Translation translationInstance) {
        if (translationInstance == null) {
            notFound()
            return
        }

        if (translationInstance.hasErrors()) {
            respond translationInstance.errors, view:'create'
            return
        }

        translationInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'translation.label', default: 'Translation'), translationInstance.id])
                redirect translationInstance
            }
            '*' { respond translationInstance, [status: CREATED] }
        }
    }

    def edit(Translation translationInstance) {
        respond translationInstance
    }

    @Transactional
    def update(Translation translationInstance) {
        if (translationInstance == null) {
            notFound()
            return
        }

        if (translationInstance.hasErrors()) {
            respond translationInstance.errors, view:'edit'
            return
        }

        translationInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Translation.label', default: 'Translation'), translationInstance.id])
                redirect translationInstance
            }
            '*'{ respond translationInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Translation translationInstance) {

        if (translationInstance == null) {
            notFound()
            return
        }

        translationInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Translation.label', default: 'Translation'), translationInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'translation.label', default: 'Translation'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
