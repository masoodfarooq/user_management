package com.app.ef.translate



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TranslationKeyController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond TranslationKey.list(params), model: [translationKeyInstanceCount: TranslationKey.count()]
    }

    def show(TranslationKey translationKeyInstance) {
        respond translationKeyInstance
    }

    def create() {
        respond new TranslationKey(params)
    }

    @Transactional
    def save(TranslationKey translationKeyInstance) {
        if (translationKeyInstance == null) {
            notFound()
            return
        }

        if (translationKeyInstance.hasErrors()) {
            respond translationKeyInstance.errors, view: 'create'
            return
        }

        translationKeyInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'translationKey.label', default: 'TranslationKey'), translationKeyInstance.id])
                redirect translationKeyInstance
            }
            '*' { respond translationKeyInstance, [status: CREATED] }
        }
    }

    def edit(TranslationKey translationKeyInstance) {
        respond translationKeyInstance
    }

    @Transactional
    def update(TranslationKey translationKeyInstance) {
        if (translationKeyInstance == null) {
            notFound()
            return
        }

        if (translationKeyInstance.hasErrors()) {
            respond translationKeyInstance.errors, view: 'edit'
            return
        }

        translationKeyInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TranslationKey.label', default: 'TranslationKey'), translationKeyInstance.id])
                redirect translationKeyInstance
            }
            '*' { respond translationKeyInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TranslationKey translationKeyInstance) {

        if (translationKeyInstance == null) {
            notFound()
            return
        }

        translationKeyInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TranslationKey.label', default: 'TranslationKey'), translationKeyInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'translationKey.label', default: 'TranslationKey'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
