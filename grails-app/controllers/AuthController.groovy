import grails.converters.JSON
import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.authc.UsernamePasswordToken
import org.apache.shiro.web.util.SavedRequest
import org.apache.shiro.web.util.WebUtils

import static org.springframework.http.HttpStatus.OK

class AuthController {
    def shiroSecurityManager
    def restService
    def index = { redirect(action: "login", params: params) }

    def login = {
        return [ username: params.username, rememberMe: (params.rememberMe != null), targetUri: params.targetUri ]
    }

    def signIn () {
        def returnInfo = [:]
        def authToken = new UsernamePasswordToken(params.username, params.password as String)
        // Support for "remember me"
        if (params.rememberMe) {
            authToken.rememberMe = true
        }
         def targetUri = params.targetUri ?: "/"
        // Handle requests saved by Shiro filters.
        SavedRequest savedRequest = WebUtils.getSavedRequest(request)
        if (savedRequest) {
            targetUri = savedRequest.requestURI - request.contextPath
            if (savedRequest.queryString) targetUri = targetUri + '?' + savedRequest.queryString
        }
        try{
             SecurityUtils.subject.login(authToken)
            render status: OK
            }
        catch (AuthenticationException ex){
            log.info "Authentication failure for user '${params.username}'."
            def m = [ username: params.username ]
            if (params.rememberMe) {
                m["rememberMe"] = true
            }

            // Remember the target URI too.
            if (params.targetUri) {
                m["targetUri"] = params.targetUri
            }
            returnInfo = [
                    status:'error'
            ]
            respond returnInfo as JSON

        }
    }

    def signOut = {
        // Log the user out of the application.
        SecurityUtils.subject?.logout()
       // webRequest.getCurrentRequest().session = null
        String username = SecurityUtils.subject?.getPrincipal()
         render status:OK
    }

    def unauthorized = {
        render "You do not have permission to access this page."
    }
}
