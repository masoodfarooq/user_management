package com.ef.apps.mediasense.OfflineJobs

/**
 * Developer: Bilal Ahmed Yaseen
 * eMail: bilal.ahmed@expertflow.com
 **/

import grails.util.Holders
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import com.ef.apps.mediasense.Utils.LogsManagement;
import com.ef.apps.mediasense.Connector.MSconnector;
import org.apache.commons.httpclient.methods.PostMethod;
import com.ef.apps.mediasense.EventsManagement.EventSubscription;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import com.ef.apps.mediasense.ResponseMapper.SubscriptionResponseMapper;

class EventsSubscriberJob {

    def properties = Holders.config;
    private String baseURL = properties['BASE_URL'];                        //from app-deelopment-mediasense-config.properties
    private String serverAddress = properties['SERVER_IP_ADDRESS'];         //from app-deelopment-mediasense-config.properties
    private String serverPort = properties['SERVER_PORT'];                 //from app-deelopment-mediasense-config.properties




    private static HttpClient client = new HttpClient();

    static triggers = {

        //Setting time interval for this Job of 5 Seconds.
        //After 5 Seconds this Events Subscription Job will Run, And Will Subscribe the Mediasense Recording Notifications
        //simple repeatInterval: 900_000
        simple repeatInterval: 5_000
    }

    def execute() {
       // def executess() {

        LogsManagement.LogInfoMesg("Going to Subscribe for Mediasense Notifications.")

        try {
            //println "Going to Subscribe for MediaSense Events/Notifications"

            baseURL = baseURL.replace("{mediasense_server_ip}", serverAddress);
            baseURL = baseURL.replace("{mediasense_server_port}", serverPort);

            client = MSconnector.getInstance().getMediaSenseClient()

            boolean sessionExpired = false;
            int sessionExpiryCount = 0;
            String subscriptionID = "";
            String sessionExpiryCode = properties['SESSION_EXPIRED'];
            int sessionExpirationCount = Integer.parseInt(properties['SESSION_EXPIRATION_COUNT'])

            //Subscribing for MediaSense Events (Note: Currently I am subscribing for All Events (Tag, Session etc...))
            String subscriptionResponse = subscribeEvents()

            //This means that some error occurred while subscribing to mediasense recording notifications
            if (subscriptionResponse.equals("")) {
                LogsManagement.LogInfoMesg("Couldn't Get any Response while Subscribing for Mediasense Notifications. See Error Logs for More Details.")
                return;
            }

            SubscriptionResponseMapper subscriptionResponseMapper = new SubscriptionResponseMapper();

            //Mapping the JSON Response of MediaSense Events Subscription
            subscriptionID = subscriptionResponseMapper.MapSubscriptionResponse(subscriptionResponse)

            //If the Session of the MediaSense User gets Expired
            while (subscriptionID.equals(sessionExpiryCode)) {

                if (sessionExpiryCount >= sessionExpirationCount) {
                    subscriptionID = ""
                    break;
                }

                sessionExpiryCount++;
                sessionExpired = true;

                //We are getting new client by LogIn the MediaSense User Again
                client = MSconnector.getInstance(properties['SESSION_EXPIRED']).getMediaSenseClient()

                //Subscribing for MediaSense Events (Note: Currently I am subscribing for All Events (Tag, Session etc...))
                subscriptionResponse = subscribeEvents()

                //This means that some error occurred while subscribing to mediasense recording notifications
                if (subscriptionResponse.equals("")) {
                    LogsManagement.LogInfoMesg("Couldn't Get any Response while Subscribing for Mediasense Notifications. See Error Logs for More Details.")
                    return;
                }

                //Mapping the JSON Response of MediaSense Events Subscription
                subscriptionID = subscriptionResponseMapper.MapSubscriptionResponse(subscriptionResponse)
            }

            //If mediasense Client's session was expired for more then 3 times, then log it as exception
            if (sessionExpired && sessionExpiryCount > 3) {
                LogsManagement.LogExceptionMesg(sessionExpiryCount + " Number of Times Session Got Expired While Subscribing for Mediasense Notifications", null);
            }

            //Indicating there is some other error other then Session Expiration Error
            if (subscriptionID.equals("")) {
                LogsManagement.LogInfoMesg("Couldn't get Valid Response while Subscribing for Mediasense Notification. See Error Logs for More Details.")
                return;
            } else {
                //If we don't get session expiration error and not any other one, then definitely mediasense notifications got subscbribed successfully
                LogsManagement.LogInfoMesg("MediaSense Recording Notifications got Subscribed Successfully with Subscription ID: " + subscriptionID)
            }

        } catch (Exception ex) {
            LogsManagement.LogExceptionMesg("Exception While Subscribing for MediaSense Notifications: ", ex)
        }
    }

    //This method will verify MediaSense Events Subscription that either the subscription has been expired or not.
    //Furthermore, If the subscription has been expired this method will automatically re-subscrisbe the MediaSense Notifications too.
    public String subscribeEvents() throws IOException {

        PostMethod postMethod = null;

        try {

            String contentType = "application/json";

            EventSubscription eventSub = new EventSubscription();

            String requestBody = eventSub.JsonSubscription("ALL_EVENTS");

            String sessionURL = properties["EVENTS_SUBSCRIPTION"];
            String URL = baseURL + sessionURL;

           // println "URL is: "+URL
            postMethod = new PostMethod(URL);

            postMethod.setParameter("Content-Type", contentType);
            //1. Actual Body, 2. Content Type, 3. Character Set
            postMethod.setRequestEntity(new StringRequestEntity(requestBody, contentType, null));

            client.executeMethod(postMethod);

            String responseToReturn = new String(postMethod.getResponseBody());

            //println "Events Subscriber Request Response Returned: " + responseToReturn

            if (responseToReturn == null || responseToReturn.equals("") || responseToReturn.isEmpty()) {
                LogsManagement.LogExceptionMesg("ERROR: Events Subscriber Job: Didn't get any Response: " + postMethod.getStatusLine())
                responseToReturn = ""
            }

            //If status Code is '302' this means that our request has been re-directed
            if (postMethod.getStatusCode() == 302) {

                Header responseHeader = postMethod.getResponseHeader("Location");

                if (responseHeader != null) {

                    //New location(IP) where our request will be re-directed
                    String location = responseHeader.toString().split(" ")[1];

                    LogsManagement.LogInfoMesg("Events Subscriber Job: Going to Redirect Request")

                    //1. URL, 2. Escaped String or Not, 3. Character Set
                    postMethod.setURI(new org.apache.commons.httpclient.URI(location, true, null));
                    client.executeMethod(postMethod);

                    responseToReturn = new String(postMethod.getResponseBody());

                    if (responseToReturn == null || responseToReturn.equals("") || responseToReturn.isEmpty()) {
                        LogsManagement.LogExceptionMesg("ERROR: Events Subscriber Job: Didn't get any Response After Redirection as Well: " + postMethod.getStatusLine())
                        responseToReturn = ""
                    }

                } else {
                    LogsManagement.LogExceptionMesg("ERROR: Events Subscriber Job: Didn't get any Redirect Location")
                }
            }

            //If we didn't get any Exception till here and got response, then this means subscription was successful which shows
            //Mediasense Server is Up and Running



            return responseToReturn;

        } catch (ConnectException connectionException) {
            LogsManagement.LogExceptionMesg("ERROR: Mediasense Server is Down. Exception While Making Connection to Mediasense Server to Subscribe for Recordings Notifications: " + connectionException.getMessage(), null)


            return "";

        } catch (NoRouteToHostException internetConnectionException) {
            LogsManagement.LogExceptionMesg("ERROR: No Internet Connection Available. Exception While Connecting to Internet while Making Connection to Mediasense Server to Subscribe for Mediasense Recordings Notifications:" + internetConnectionException.getMessage(), null)
            return "";
        } catch (Exception ex) {
            LogsManagement.LogExceptionMesg("Exception While Subscribing for MediaSense Recording Notifications: ", ex)
            return "";
        } finally {
            // Release the connection.
            //println "Releasing Connection"
            postMethod.releaseConnection();
            postMethod = null;
        }
    }
}