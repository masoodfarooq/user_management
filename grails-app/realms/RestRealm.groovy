import com.app.ef.security.User
import org.apache.shiro.authc.AccountException
import org.apache.shiro.authc.SimpleAccount
import org.apache.shiro.authc.UnknownAccountException

//import ru.grails.Employee

class RestRealm {
    def restService
    static authTokenClass = com.app.ef.security.RestToken
    def authenticate(authToken) {
        log.info "Attempting to authenticate ${authToken.token} in REST realm..."
        def token = authToken.token
          // Null username is invalid
        if (token == null) {
             throw new AccountException("Null token are not allowed by this realm.")
        }
        // If we don't have user for specified token then user is not authenticated
        def user
        def username = restService.checkTokenUsername(token)
        if(username.equals("token_expired")){
            throw new AccountException("Token has Expired.")
        }
        else if(username.equals("error")){
            throw new AccountException("Error occurred in signIn.")
        }
        else{
             user = (!username) ?: User.findByUsername(username)
            if (!user) {
                throw new UnknownAccountException("No account found for token [${token}]")
            }
            if(!User.findByUsername(username)?.isActive){
                throw new AccountException("com.ef.cbr.user.label.inActive")
            }
        }


        // println  "Found user '${user.username}' in DB"
        //ok. Account is found, user is authenticated
        return new SimpleAccount(user.username, user.password, "RestRealm")
    }

}