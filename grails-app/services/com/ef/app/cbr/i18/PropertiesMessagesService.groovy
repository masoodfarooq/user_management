package com.ef.app.cbr.i18

import grails.transaction.Transactional
import org.codehaus.groovy.grails.context.support.PluginAwareResourceBundleMessageSource
import org.codehaus.groovy.grails.plugins.BinaryGrailsPlugin;
import org.codehaus.groovy.grails.plugins.GrailsPlugin;
import org.springframework.context.support.ReloadableResourceBundleMessageSource.PropertiesHolder

@Transactional
class PropertiesMessagesService extends PluginAwareResourceBundleMessageSource{

    def messageSource
    public Set getAllKeys(Locale locale) {
       PropertiesHolder pluginProps = getMergedPluginProperties(locale)
       Set pluginSet = pluginProps.getProperties().keySet()

        PropertiesHolder props = getMergedProperties(locale)
       Set propsSet = props.getProperties().keySet()

        Set returnSet = [] as Set
        returnSet.addAll(propsSet)
        returnSet.addAll(pluginSet)
        println "MUM______________________--"
         println returnSet.getProperties()
        return returnSet
    }

    public List<PropertiesHolder> getAllProperties(Locale locale) {
        List<PropertiesHolder> holders = []
        holders << getMergedProperties(locale)
        holders << getMergedPluginProperties(locale)

        holders

    }
}
