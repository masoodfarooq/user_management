package com.ef.app.cbr.security
import com.nimbusds.jose.JWSAlgorithm
import com.nimbusds.jose.JWSHeader
import com.nimbusds.jose.JWSSigner
import com.nimbusds.jose.JWSVerifier
import com.nimbusds.jose.crypto.RSASSASigner
import com.nimbusds.jose.crypto.RSASSAVerifier
import com.nimbusds.jwt.JWTClaimsSet
import com.nimbusds.jwt.SignedJWT
import grails.transaction.Transactional

import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey

@Transactional
class RestService {

    def grailsCacheManager
    String generateToken(username, request) {
        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
        keyGenerator.initialize(1024);

        KeyPair kp = keyGenerator.genKeyPair();
        RSAPrivateKey privateKey = (RSAPrivateKey)kp.getPrivate();
        RSAPublicKey publicKey = (RSAPublicKey)kp.getPublic();

        // Create RSA-signer with the private key
        JWSSigner signer = new RSASSASigner(privateKey);
        // set claim set

        JWTClaimsSet jwtClaims = new JWTClaimsSet();
        jwtClaims.setIssuer("${username}");
        jwtClaims.setSubject("alice");
        jwtClaims.setExpirationTime(new Date().plus(10));
        jwtClaims.setIssueTime(new Date());
        SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.RS256), jwtClaims);
         // Compute the RSA signature
        signedJWT.sign(signer);

        String token = signedJWT.serialize();
        return  token
    }

    String checkTokenUsername(token){
        try{
            KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
            keyGenerator.initialize(1024);
            KeyPair kp = keyGenerator.genKeyPair();
            RSAPublicKey publicKey = (RSAPublicKey)kp.getPublic();
            // Create RSA-signer with the private key
            SignedJWT  signedJWT = SignedJWT.parse(token);
            JWSVerifier verifier = new RSASSAVerifier(publicKey);

            def currentDate = new Date();
            if(currentDate > signedJWT.getJWTClaimsSet().getExpirationTime())
                return "token_expired"
            else
                return  signedJWT.getJWTClaimsSet().getIssuer();

        }
        catch (Exception ex){
         log.error "Exception is ..  and Exception is... " + ex
            return "error";
        }

    }

}