package com.ef.app.cbr.security

import grails.transaction.Transactional
import org.apache.commons.lang.RandomStringUtils

@Transactional
class TokenGenerationService {

    def tokenGeneration(username,request){
        String charset = (('A'..'Z') + ('0'..'9')).join()
        Integer length = 16
        String tokenString = RandomStringUtils.random(length, charset.toCharArray())

        def tokenInfo =[token:tokenString,issueDate:new Date(),endDate:new Date().getDay()+30]
        def userInfo = [username:username]
        def userToken = [token:tokenInfo,userData:userInfo]
        return  userToken
    }
}
