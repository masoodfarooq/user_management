package com.ef.apps.recordings

import com.ef.apps.mediasense.Utils.LogsManagement
import com.ef.apps.mediasense.VoiceBiometrics.UsersVoiceModels
import grails.transaction.Transactional

@Transactional
class CheckVoiceModelService {

    //Injecting Service to Publish Messages on AMQ
    def messagePublisherService

    //Injecting Service to manage all operation related to User Voice Model
    def voiceModelService

    def mediaFilesGeneratorService


    def process(String fileName, String userExtension, String agentExtension) {

        //println("File Name: >>>> "+fileName+" , userExtension: >>>> "+userExtension+" , agentExtension: "+agentExtension)

        LogsManagement.LogInfoMesg("INFO: Going to Verify that Either Voice Model Already Exists or Not for User with Extension: " + userExtension)

        //First of All we will check that either we already have voice model for this call or not
        UsersVoiceModels userVoiceModel = voiceModelService.getUserVoiceModel(userExtension)

        //println("userVoiceModelID: >>>> "+userVoiceModel.modelID);
        //Now Checking the Voice Model already exists or not
        if (userVoiceModel != null) {

            //If User Voice Model already exists, So we don't need to create any voice model
            LogsManagement.LogInfoMesg("INFO: Voice Model found for User with Extension: " + userExtension + ". Model ID is: " + userVoiceModel.modelID + ". Going to Process this Call for Voice Recognition.")

            mediaFilesGeneratorService.generatingFiles(fileName, agentExtension, userVoiceModel.modelID)

        } else {

            //If User Voice Model Doesn't exist already

            LogsManagement.LogInfoMesg("INFO: Voice Model not Found for User with Extension: " + userExtension + ". Going to Process Stream into MediaFile for Model Creation if Notified.")

            //We have discussed that we will be sending '0' to notify the agent that we don't have voice model

            String messageToSend = agentExtension + ":" + 0

            LogsManagement.LogInfoMesg("INFO: Going to Publish Message on AMQ Regarding User Voice Model Not Found: " + messageToSend)

            boolean dataSentSuccessfully = messagePublisherService.sendMessage(messageToSend)

            if (!dataSentSuccessfully) {
                LogsManagement.LogInfoMesg("INFO: Some Error Occurred While Publishing Message: " + messageToSend + " to AMQ. See Error Logs for More Details.")
            } else {
                LogsManagement.LogInfoMesg("INFO: Message Published Successfully on AMQ Regarding User Voice Model Not Found: " + messageToSend)
            }

            voiceModelService.generateFileToCreateVoiceModel(fileName, userExtension)
        }
    }
}
