package com.ef.apps.recordings

import com.ef.apps.mediasense.Utils.URLsFormatter

import grails.util.Holders
import com.ef.apps.mediasense.Utils.LogsManagement
import org.codehaus.groovy.grails.web.context.ServletContextHolder

class MediaFilesGeneratorService {


    def processVoiceBiometricsRecService
    //def AMQConnectionManagementService

    //Injecting AMQ Publisher to Publish the Results
    def messagePublisherService

    def generatingFiles(String fileName, String extension, String modelID){


        def properties = Holders.config;
        runAsync{

            long procMaxTimeoutMillis = -1
            long TIMESTAMP_BEFORE_EXECUTE = -1;

            URLsFormatter formatURLs = new URLsFormatter();

            String sampleModelToRecognizeVoice = properties['SAMPLE_SPEAKER_MODEL_ID']
            try {
                String termTimePeriod = properties['MAX_PROCESS_TERMINATION_TIME_PERIOD']
                procMaxTimeoutMillis = Long.parseLong(termTimePeriod)
            } catch (Exception ex) {

                LogsManagement.LogExceptionMesg("Exception Occurred While Processing Max Time Period for Process Termination: ", ex)

                //Default Process termination timeout period
                procMaxTimeoutMillis = 10_000;
            }

            String fileFormat = properties['MEDIA_FILES_FORMAT']
           // println "fileFormat >>>"+fileFormat
            String validatedFileFormat = "." + fileFormat;

            String fileStoragePath = properties['MEDIA_FILES_STORAGE_PATH']
           // println "fileStoragePath: >>> "+fileStoragePath
            String basePath = ServletContextHolder.servletContext.getRealPath("/");
           // println "basePath: >>>"+basePath
            fileStoragePath = basePath + fileStoragePath;

            String tempInitialFile = fileName + validatedFileFormat;
            String destFilePath = fileStoragePath + fileName + "_";
            String RTSP_URL = formatURLs.getRecordingStreamingURL(fileName)
           // println " Streaming URL: " + RTSP_URL

            try {

                int destOffsetFileNumber = 5;
                int processResponse = 0;
                String formattedDestFileName = "";

                while (processResponse == 0) {

                    ProcessBuilder builder = null;

                    if (destOffsetFileNumber == 5) {
                        formattedDestFileName = destFilePath + destOffsetFileNumber + validatedFileFormat
                        System.out.println("Destination File Path: " + formattedDestFileName);
                        builder = new ProcessBuilder("ffmpeg", "-i", RTSP_URL, "-f", fileFormat, "-y", "-t", "5.5", formattedDestFileName);
                    } else {
                        //'-y' option will overwrite file if already exists and in this case exit status will be 0 as well.
                        System.out.println("Destination File Path: " + fileStoragePath + tempInitialFile);
                        builder = new ProcessBuilder("ffmpeg", "-i", RTSP_URL, "-f", fileFormat, "-y", "-t", "5.5", fileStoragePath + tempInitialFile);
                    }

                    builder.redirectErrorStream(true);

                    Process processToExecute = builder.start();

                    LogsManagement.LogInfoMesg("Going to Generate Media File for Recording: " + fileName + " and Offset: " + destOffsetFileNumber)

                    //Time at which the process was started
                    TIMESTAMP_BEFORE_EXECUTE = System.currentTimeMillis();

                    println "Process Execution Start Time: " + TIMESTAMP_BEFORE_EXECUTE

                    boolean hasExited = false;

                    //This loop will destri the process if process not completes its execution within expected time interval
                    while (!hasExited) {

                        try {

                            /******The below line will throw exception 'IllegalThreadStateException', if the subprocess represented by
                             this Process object has not yet terminated******/

                            processResponse = processToExecute.exitValue();
                            hasExited = true;

                        } catch (IllegalThreadStateException ex) {

                            /******We are here because process not terminated yet. So, now we will check either it's
                             execution has crossed expected time limit or not******/

                            if (System.currentTimeMillis() > (TIMESTAMP_BEFORE_EXECUTE + procMaxTimeoutMillis)) {

                                println "Process Forcibly termination Time: " + (System.currentTimeMillis() - TIMESTAMP_BEFORE_EXECUTE)

                                processToExecute.destroy();
                                processResponse = 1;

                                LogsManagement.LogExceptionMesg("Process of Generating Media Files not Terminated Successfully for Recording: " + fileName + " and Offset: " + destOffsetFileNumber + ". So Terminating it Forcibly with Status: " + processResponse)

                                hasExited = true;
                            }
                        }
                    }

                    println "Process Normal termination Time: " + ( System.currentTimeMillis() - TIMESTAMP_BEFORE_EXECUTE )

                    LogsManagement.LogInfoMesg("Process of Generating Media File for Recording: " + fileName + " and Offset: " + destOffsetFileNumber + " Terminated with Status: " + processResponse)

                    //System.out.println("Process of Creating Media Files for Recording: " + fileName + " and Offset: " + destOffsetFileNumber + " Terminated with Status: " + processResponse);

                    //If We have fetched first sample
                    if (destOffsetFileNumber != 5) {

                        String concatQuery = "concat:" + formattedDestFileName + "|" + fileStoragePath + tempInitialFile;

                        LogsManagement.LogInfoMesg("Going to Concatenate Media File: " + fileName + " with Offset " + destOffsetFileNumber)

                        formattedDestFileName = destFilePath + destOffsetFileNumber + validatedFileFormat;

                        ProcessBuilder mediaFilesConcatenator = new ProcessBuilder("ffmpeg", "-i", concatQuery, "-c", "copy", formattedDestFileName);

                        builder.redirectErrorStream(true);

                        Process processToConcatenateMediaFiles = mediaFilesConcatenator.start();

                        int concatenationProcessExitStatus = processToConcatenateMediaFiles.waitFor()

                        LogsManagement.LogInfoMesg("Process of Concatenating Media Files for Recording: " + fileName + " with Offset: " + destOffsetFileNumber + " Terminated with Status: " + concatenationProcessExitStatus);
                    }

                    String fileNameWithExtension = fileName + "_" + destOffsetFileNumber + validatedFileFormat

                    String mediaFilePublicURL = formatURLs.generateMediaFilesPublicURL(fileNameWithExtension)

                    println "mediaFIleURL: "+mediaFilePublicURL + " , modelID: "+modelID + " +extension: "+extension
                  //  processVoiceBiometricsRecService?.recordingProcessForMatchingVoice(mediaFilePublicURL , sampleModelToRecognizeVoice , extension)
                    int matchingProbability=processVoiceBiometricsRecService.recordingProcessForMatchingVoice(mediaFilePublicURL , modelID , extension)
                       println "matching probability: "+matchingProbability
                    //Sending this probability response to finesse
                    //boolean dataWrittenSuccessfully = managingNetSocketService.writeData("Matching Result is: " + matchingProbability)

                    String messageToSend = extension + ":" + matchingProbability
                    println "message to send: "+messageToSend
                    LogsManagement.LogInfoMesg("INFO: Going to Publish Message on AMQ: " + messageToSend)

                    boolean dataSentSuccessfully = messagePublisherService.sendMessage(messageToSend)

                    if (!dataSentSuccessfully) {
                        LogsManagement.LogInfoMesg("INFO: Some Error Occurred While Publishing Message: " + messageToSend + " to AMQ. See Error Logs for More Details.")
                    } else {
                        LogsManagement.LogInfoMesg("INFO: Message Published Successfully on AMQ: " + messageToSend)
                    }



                   // AMQConnectionManagementService.establishConnectionWithAMQ()
                    destOffsetFileNumber += 5;
                }

                println "Process of Generating Media Files and Voice Recognition Completed for recording: " + fileName
                LogsManagement.LogInfoMesg("Process of Generating Media Files and Voice Recognition Completed for recording: " + fileName)

            } catch (Exception ex) {
                LogsManagement.LogExceptionMesg("Exception while Generating and Recognizing Voice using Media Files for Recording: " + fileName, ex);
            }
        }
    }


}
