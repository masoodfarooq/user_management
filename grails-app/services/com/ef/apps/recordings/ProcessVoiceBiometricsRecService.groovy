package com.ef.apps.recordings

import grails.transaction.Transactional

import com.ef.apps.mediasense.Utils.LogsManagement
import com.ef.apps.mediasense.KeyLemon.ProcessKeyLemonRequests
import com.ef.apps.mediasense.ResponseMapper.KeyLemon.KeylemonRequestsResponseMapper

//@Transactional
class ProcessVoiceBiometricsRecService {

    //Injecting Service to send data to finesse
    //def managingNetSocketService



    def hello() {
        return "hello!";
    }

    public int recordingProcessForMatchingVoice(String fileToProcess, String modelID, String extension) {
        int matchingProbability = -1;

        //fileToProcess = "http://85.190.182.81:4040/mediasense/RecordedSessions/6f0e14ba4cf08381.mp3";

        try {
            ProcessKeyLemonRequests process = new ProcessKeyLemonRequests();
            //println "messagePublisherService: " + messagePublisherService;

            String recognitionResponse = process.recognizeVoiceFromGivenModel(fileToProcess, modelID)

            if (recognitionResponse.equals("")) {

                //In this Case Matching Probability Will remain '-1'

                //managingNetSocketService.writeData("Recognition Response Not Found...!!!")
                LogsManagement.LogInfoMesg("Some Error Occurred while processing Media File: " + fileToProcess + " Against Model with ID: " + modelID + ". See Error Logs for Further Details.")

            } else {

                matchingProbability = KeylemonRequestsResponseMapper.fetchProbabilityFromRecognizeVoiceModelResponse(recognitionResponse);

                if (matchingProbability == -1) {
                    LogsManagement.LogInfoMesg("Some Error Occurred While Recognize Media File: " + fileToProcess + " Against Model with ID: " + modelID + " Against Extension: " + extension + ". See Error Logs for More Details.")
                } else {
                    LogsManagement.LogInfoMesg("INFO: Matching Result of Comparing Media File: " + fileToProcess + " Against Model with ID: " + modelID + " Against Extension: " + extension + " is: " + matchingProbability)
                }
            }

        } catch (Exception ex) {
            matchingProbability = -1;
            LogsManagement.LogExceptionMesg("Exception Occurred While Processing Recording: " + fileToProcess + " With Model ID: " + modelID + " Against Extension: " + extension, ex)
        }
        return matchingProbability
    }
}

