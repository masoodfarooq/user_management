package com.ef.apps.mediasense.VoiceBiometrics.UsersVoiceModelsManagement

import com.ef.apps.mediasense.KeyLemon.ProcessKeyLemonRequests
import com.ef.apps.mediasense.ResponseMapper.KeyLemon.KeylemonRequestsResponseMapper
import com.ef.apps.mediasense.Utils.LogsManagement
import com.ef.apps.mediasense.Utils.URLsFormatter
import com.ef.apps.mediasense.Utils.UserIdentifiersUtils
import com.ef.apps.mediasense.VoiceBiometrics.UsersVoiceModels
import grails.transaction.Transactional
import grails.util.Holders
import org.codehaus.groovy.grails.web.context.ServletContextHolder

/*This class will be used to perform all possible operation related to User Voice Model Creation. It Includes:
1. Getting Already Existing Voice Model of Particular User
2. Saving Just Created Voice Model in Database
3. Generating Media File that will be used to create Voice Model
4. Generating Voice model from Media File
*/

@Transactional
class VoiceModelService {

    def properties = Holders.config;

    //This method will tell us that either the voice model of the user already exist or not
    public UsersVoiceModels getUserVoiceModel(String extension) {

        UsersVoiceModels usersVoiceModel = null

        try {

            //Check either we have any model against this number
            usersVoiceModel = UsersVoiceModels.findByExtension(extension)

        } catch (Exception ex) {
            usersVoiceModel = null
            LogsManagement.LogExceptionMesg("Exception Occurred while Fetching Users Voice Model with Extension: " + extension, ex)
        }

        return usersVoiceModel
    }

    //This method will generate the voice model for the particular user
    public boolean saveUserVoiceModel(String modelID, String extension) {

        UsersVoiceModels userVoiceModelToGenerate = null
        boolean userVoiceModelGeneratedSuccessfully = true

        try {

            //We can also provide name to the generated model, so leaving room for the future work, If required
            String modelName = ""

            //Fetching System Generated GUID of the user
            String userID = UserIdentifiersUtils.generateUniqueUserID();

            //Checking either user id has been generated successfully or not
            if (userID.equals("")) {
                userVoiceModelGeneratedSuccessfully = false
            } else {

                //Note: Without this transaction block I was getting the following exception while saving data:
                //No Hibernate Session bound to thread, and configuration does not allow creation of non-transactional one here

                // the Hibernate session isn't available in the new thread. But using withTransaction (even if you're not updating)
                // is a quick fix since it creates and binds a new session and keeps the loaded instances attached for the duration.

                /*All database statements are executed within the context of a physical transaction, even when we don’t
                 explicitly declare transaction boundaries (BEGIN/COMMIT/ROLLBACK). If you don't declare the
                 transaction boundaries, then each statement will have to be executed in a separate transaction.
                 This may even lead to opening and closing one connection per statement, unless your environment can
                 deal with connection-per-thread binding. Declaring a service as @Transactional will give you one
                 connection for the whole transaction duration, and all statements will use that single isolation
                 connection. This is way better than not using explicit transactions in the first place. On large
                 applications you may have many concurrent requests and reducing database connection acquiring request
                 rate is definitely improving your overall application performance.*/

                UsersVoiceModels.withTransaction {
                    userVoiceModelToGenerate = new UsersVoiceModels(userID, modelID, modelName, extension)
                    userVoiceModelToGenerate.save(flush: true)
                }
            }

        } catch (Exception ex) {
            userVoiceModelGeneratedSuccessfully = false
            LogsManagement.LogExceptionMesg("Exception Occurred While Generating Voice Model with ID: " + modelID + " for User With Extension: " + extension, ex)
        }

        return userVoiceModelGeneratedSuccessfully
    }

    //Once we have Media File and Agent has notified us also that he has authenticated user manually and now we can create
    //user's voice model for future use.
    //So, we will be using Creating Voice Model from the publicly accessible URL of Media File
    public String createUserVoiceModel(String fileToCreateModel, String modelName) {

        //fileToProcess = "http://85.190.182.81:4040/mediasense/RecordedSessions/6f0e14ba4cf08381.mp3";

        String modelID = "";

        try {
            ProcessKeyLemonRequests process = new ProcessKeyLemonRequests();

            String voiceModelCreationResponse = process.createUserVoiceModel(fileToCreateModel, modelName)

            if (voiceModelCreationResponse.equals("")) {

                LogsManagement.LogInfoMesg("Some Error Occurred while Creating User Voice Model from File: " + fileToCreateModel + ". See Error Logs for Further Details.")

            } else {
                modelID = KeylemonRequestsResponseMapper.fetchModelIDFromCreateVoiceModelResponse(voiceModelCreationResponse);
            }

        } catch (Exception ex) {
            modelID = ""
            LogsManagement.LogExceptionMesg("Exception Occurred While Processing Recording: " + fileToCreateModel + " With Model ID: " + modelID + " Against Extension: " + extension, ex)
        }

        return modelID
    }

    //This method is used to generate media file that will be used on later (if notified by agent) for user voice model
    // creation. This method will be invoked just then when User makes call for the first time and we don't find any
    //Voice model for this user. So agent perform manual authentication process and in the mean while file is created.
    def generateFileToCreateVoiceModel(String fileName, String extension) {

        //Running Asynchronously so that the normal execution of the application may not affect
        runAsync {

            try {

                //Getting Formatted Streaming URL in Required Form
                URLsFormatter formatURLs = new URLsFormatter();
                String RTSP_URL = formatURLs.getRecordingStreamingURL(fileName)

                //Getting Format of the file in which Media File will be generated
                String fileFormat = properties['MEDIA_FILES_FORMAT'];
                String validatedFileFormat = "." + fileFormat;

                //Getting File Storage Path in which Generated File will be stored
                String fileStoragePath = properties['MEDIA_FILES_STORAGE_PATH']
                String basePath = ServletContextHolder.servletContext.getRealPath("/");

                println "Base Path: " + basePath

                fileStoragePath = basePath + fileStoragePath;

                //Formatting the Name to be assigned to Media File we are going to generate
                String formattedDestFileName = fileStoragePath + extension + validatedFileFormat

                int processResponse = 0
                long procMaxTimeoutMillis = -1
                long TIMESTAMP_BEFORE_EXECUTE = -1

                try {
                    //Getting Maximum Termination Time after Which Process of Generating Media File to create Voice Model will
                    //be terminated forcibly, if not successfully terminated before

                    println "MAX Time: " + properties['MAX_MEDIA_FILE_PROCESS_TERMINATION_TIME_PERIOD_FOR_MODEL_CREATION']

                    procMaxTimeoutMillis = Long.parseLong(properties['MAX_MEDIA_FILE_PROCESS_TERMINATION_TIME_PERIOD_FOR_MODEL_CREATION'])
                } catch (Exception ex) {

                    LogsManagement.LogExceptionMesg("Exception Occurred While Processing Max Time Period for Process Termination Which Processing Media File for Voice Model Creation: ", ex)

                    //Default Process termination timeout period - 150 seconds
                    procMaxTimeoutMillis = 150_000;
                }

                //getting duration of the media file that we are going to generate to create voice model
                String maxFileDurationForModelCreation = properties['MAX_FILE_DURATION_FOR_MODEL_CREATION']

                //Initializing Process to Generate Media File
                ProcessBuilder processBuilder = new ProcessBuilder("ffmpeg", "-i", RTSP_URL, "-f", fileFormat, "-y", "-t", maxFileDurationForModelCreation, formattedDestFileName);

                //We have redirected Error and Output Stream. The Line below Will do two things
                //It will keep on reading buffer so that It keep on getting free with time.
                //It will print the data on the output screen
                processBuilder.redirectErrorStream(true).redirectOutput(ProcessBuilder.Redirect.INHERIT); ;

                //Starting Process of generating Media File
                Process processToExecute = processBuilder.start();

                LogsManagement.LogInfoMesg("Going to Generate Media File of Call: " + fileName + " for Creating Voice Model for User with Extension: " + extension)

                //Time at which the process was started
                TIMESTAMP_BEFORE_EXECUTE = System.currentTimeMillis();

                println "Process Execution Start Time is: " + TIMESTAMP_BEFORE_EXECUTE

                //The Below Commented Code wasn't working because the process was not terminating even after 5 minutes that
                //should be terminated normally after creating 2 mins media file. I think the reason was that data in its
                //buffer was not getting read that's why It wasn't terminating.

                boolean hasExited = false

                //This loop will destroy the process if process not completes its execution within expected time interval
                while (!hasExited) {

                    try {

                        //Stop Execution for 10 Seconds
                        Thread.sleep(10_000)

                        /******The below line will throw exception 'IllegalThreadStateException', if the subprocess represented by
                        this Process object has not yet terminated******/

                        processResponse = processToExecute.exitValue();
                        hasExited = true;

                    } catch (IllegalThreadStateException ex) {

                        /******We are here because process not terminated yet. So, now we will check either it's
                         execution has crossed expected time limit or not******/

                        if (System.currentTimeMillis() > (TIMESTAMP_BEFORE_EXECUTE + procMaxTimeoutMillis)) {

                            println "Process Forcibly termination Time: " + (System.currentTimeMillis() - TIMESTAMP_BEFORE_EXECUTE)

                            processToExecute.destroy();
                            processResponse = 1;

                            LogsManagement.LogExceptionMesg("Process of Generating Media File of Call: " + fileName + " for Creating Voice Model for User with Extension: " + extension + " NOT Terminated Successfully for Recording: So Terminating it Forcibly.")

                            hasExited = true;
                        }
                    }
                }

                //Now in the block of code below we keep on reading buffer so that It keep on getting free with time and we are
                //simply discarding this data as it is of no use for us. And we are also keeping in the default exit scenario
                //If process gets stuck on any place.

//                boolean hasExited = false
//                String responseLineRead = ""
//                BufferedReader br = new BufferedReader(new InputStreamReader(processToExecute.getInputStream()));
//
//                //while ((responseLineRead = br.readLine()) != null) {
//                while (!hasExited) {
//
//                    responseLineRead = br.readLine()
//
//                    if (responseLineRead == null) {
//                        //println "Exiting...."
//                        //hasExited = true
//                    }
//
//                    //println hasExited
//                    //println responseLineRead
//
//                    /******We are here because process not terminated yet. So, now we will check either it's
//                     execution has crossed expected time limit or not******/
//
//                    if (System.currentTimeMillis() > (TIMESTAMP_BEFORE_EXECUTE + procMaxTimeoutMillis)) {
//
//                        println "Process Forcibly termination Time: " + (System.currentTimeMillis() - TIMESTAMP_BEFORE_EXECUTE)
//
//                        processToExecute.destroy();
//                        processResponse = 1;
//
//                        LogsManagement.LogExceptionMesg("Process of Generating Media File of Call: " + fileName + " for Creating Voice Model for User with Extension: " + extension + " NOT Terminated Successfully for Recording: So Terminating it Forcibly.")
//
//                        //Getting out of this Loop
//                        hasExited = true
//                        //break
//                    }
//
//                    //println "BAY"
//                }

                println "Process Normal termination Time: " + (System.currentTimeMillis() - TIMESTAMP_BEFORE_EXECUTE)

                LogsManagement.LogInfoMesg("Process of Generating Media File of Call: " + fileName + " for Creating Voice Model for User with Extension: " + extension + " Terminated with Status: " + processResponse)

            } catch (Exception ex) {
                LogsManagement.LogExceptionMesg("Exception Occurred While Creating Media File for Voice Model Creation from Recording: " + fileName + " for User with Extension: " + extension, ex)
            }
        }
    }
}
