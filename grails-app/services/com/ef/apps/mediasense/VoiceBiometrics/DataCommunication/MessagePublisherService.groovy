package com.ef.apps.mediasense.VoiceBiometrics.DataCommunication

import com.ef.apps.mediasense.Utils.DestinationsNameUtils
import com.ef.apps.mediasense.Utils.LogsManagement
import grails.transaction.Transactional
import org.apache.activemq.command.ActiveMQTopic

import javax.jms.TextMessage
import javax.jms.Topic
import javax.jms.TopicPublisher

@Transactional
class MessagePublisherService {

    //Injecting AMQ Connection Service
    def AMQConnectionManagementService

    //Topic on Which Message will be Publish
    Topic topicToPublishMessages = null

    //Publisher who will publish the message on the specified destination
    TopicPublisher publisher = null

    def boolean initializeMessagePublisher() {

        boolean publisherInitializedSuccessfully = true

        try {

            if (AMQConnectionManagementService.topicSession == null) {
                publisherInitializedSuccessfully = false;
                LogsManagement.LogExceptionMesg("ERROR: Couldn't Initialize Publisher to Publish Messages because Connection was not Established Successfully with AMQ.")
            } else {

                //Getting Destination name to assign to Topic to publish data
                DestinationsNameUtils destinations = new DestinationsNameUtils()
                String destinationName = destinations.getTopicNameToPublishMessages()

                //Creating Topic on which publisher will publish messages
                topicToPublishMessages = (javax.jms.Topic) new ActiveMQTopic(destinationName);

                //Creating publisher to publish the message with the topic as destination we just created
                publisher = AMQConnectionManagementService.topicSession.createPublisher(topicToPublishMessages);
            }

        } catch (Exception ex) {
            publisher = null;
            publisherInitializedSuccessfully = false
            LogsManagement.LogExceptionMesg("Exception Occurred While Initializing AMQ Publisher to Send Data: ", ex)
        }

        return publisherInitializedSuccessfully
    }

    def boolean sendMessage(String messageToSend) {

        boolean messageSentSuccessfully = true

        try {

            if (publisher == null) {

                messageSentSuccessfully = false;
                LogsManagement.LogExceptionMesg("ERROR: Publisher Couldn't Send Message Successfully as Some Error was Occurred While Initializing Publisher.")
            } else {

                TextMessage textMessage = getTextMessage(messageToSend)

                if (textMessage == null) {
                    messageSentSuccessfully = false;
                } else {

                    publisher.send(textMessage)
                }
            }

        } catch (Exception ex) {
            this.publisher = null;
            messageSentSuccessfully = false
            LogsManagement.LogExceptionMesg("Exception Occurred While Publishing Message to AMQ to Send to Finesse regarding Voice Biometrics", ex)
        }

        return messageSentSuccessfully
    }

    public TextMessage getTextMessage(String message) {

        TextMessage textMessage = null

        try {

            textMessage = AMQConnectionManagementService.topicSession.createTextMessage();
            textMessage.setText(message)

        } catch (Exception ex) {
            textMessage = null
            LogsManagement.LogExceptionMesg("Exception Occurred While Creating Text Message to Publish Data:  " + message, ex)
        }

        return textMessage
    }

    def boolean terminatePublisher() {

        boolean publisherTerminatedSuccesfully = true

        try {

            if (this.publisher == null) {
                publisherTerminatedSuccesfully = false;
                LogsManagement.LogExceptionMesg("ERROR: Couldn't Terminate Publisher Successfully as Some Error was Occurred While Initializing Publisher.")
            } else {
                this.publisher.close();
            }

        } catch (Exception ex) {
            this.publisher = null
            publisherTerminatedSuccesfully = false;
            LogsManagement.LogExceptionMesg("Exception Occurred While Terminating AMQ Publisher: ", ex)
        }

        return publisherTerminatedSuccesfully
    }
}
