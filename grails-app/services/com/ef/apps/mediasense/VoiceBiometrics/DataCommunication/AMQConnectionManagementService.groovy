package com.ef.apps.mediasense.VoiceBiometrics.DataCommunication

import com.ef.apps.mediasense.Utils.LogsManagement
import com.ef.apps.mediasense.Utils.URLsFormatter
import grails.transaction.Transactional
import org.apache.activemq.ActiveMQConnectionFactory
import org.apache.activemq.command.ActiveMQTopic

import javax.jms.Topic
import javax.jms.TopicConnection
import javax.jms.TopicConnectionFactory
import javax.jms.TopicSession

@Transactional
class AMQConnectionManagementService {

    Topic myTopic = null
    TopicSession topicSession = null
    def TopicConnection topicConnection = null
    def TopicConnectionFactory topicConnectionFactory = null

    //Setter Methods

    void setMyTopic(Topic myTopic) {
        this.myTopic = myTopic
    }

    void setTopicSession(TopicSession topicSession) {
        this.topicSession = topicSession
    }

    void setTopicConnection(TopicConnection topicConnection) {
        this.topicConnection = topicConnection
    }

    void setTopicConnectionFactory(TopicConnectionFactory topicConnectionFactory) {
        this.topicConnectionFactory = topicConnectionFactory
    }

    //Getter Methods

    Topic getMyTopic() {
        return myTopic
    }

    TopicSession getTopicSession() {
        return topicSession
    }

    TopicConnection getTopicConnection() {
        return topicConnection
    }

    TopicConnectionFactory getTopicConnectionFactory() {
        return topicConnectionFactory
    }

    //This Method will Establish Connection with AMQ
    def boolean establishConnectionWithAMQ() {

        boolean connectionEstablishedSuccessfully = true

        try {

            URLsFormatter formatURLs = new URLsFormatter()
            String brokerURL = formatURLs.getBrokerURL()

            //Getting Topic Connection Factory
            topicConnectionFactory = (TopicConnectionFactory) new ActiveMQConnectionFactory(brokerURL)

            // create a new TopicConnection for pub/sub messaging
            topicConnection = topicConnectionFactory.createTopicConnection();

            //Starting Connection
            //Message delivery does not begin until you start the connection you created by calling its start method
            topicConnection.start();

            // lookup an existing topic
            String destinationName = formatURLs.getDestinationName()
            myTopic = (javax.jms.Topic) new ActiveMQTopic(destinationName);

            // create a new TopicSession for the client
            topicSession = topicConnection.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE);

        } catch (Exception ex) {
            connectionEstablishedSuccessfully = false
            LogsManagement.LogExceptionMesg("Exception Occurred While Establishing Connection With AMQ for Publishing Voice Recognition Results: ", ex)
        }

        return connectionEstablishedSuccessfully
    }

    def boolean terminateConnectionWithAMQ() {

        boolean connectionTerminatedSuccessfully = true;

        try {

            if (this.topicConnection == null) {

                connectionTerminatedSuccessfully = false
                LogsManagement.LogExceptionMesg("ERROR: Couldn't Terminate Connection With AMQ Successfully as Connection wasn't Successfully Established.");

            } else {

                if (this.topicSession != null) {
                    //Terminating Session for Publisher/Consumer for Further Messages
                    this.topicSession.close();
                } else {
                    connectionTerminatedSuccessfully = false
                    LogsManagement.LogExceptionMesg("ERROR: Couldn't Terminate Connection With AMQ Successfully as Session is Null.")
                }

                //Terminating Connection With Provider
                this.topicConnection.close()
            }

        } catch (Exception ex) {
            connectionTerminatedSuccessfully = false
            LogsManagement.LogExceptionMesg("Exception Occurred While Terminating Connection With AMQ: ", ex)
        }

        return connectionTerminatedSuccessfully
    }
}
