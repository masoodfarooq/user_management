package com.ef.apps.mediasense.VoiceBiometrics.DataCommunication

import com.ef.apps.mediasense.Utils.DestinationsNameUtils
import com.ef.apps.mediasense.Utils.LogsManagement
import com.ef.apps.mediasense.Utils.URLsFormatter
import grails.transaction.Transactional
import grails.util.Holders
import org.apache.activemq.command.ActiveMQTopic

import javax.jms.*

@Transactional
class MessageConsumerService {

    //Injecting AMQ Connection Service
    def AMQConnectionManagementService

    //Topic on Which Message will be Publish
    Topic topicToConsumeMessages = null

    //To manage the voice model for existing call
    def voiceModelService

    def properties = Holders.config;

    //Publisher who will publish the message on the specified destination
    MessageConsumer consumer = null

    def boolean initializeMessageConsumer() {

        boolean consumerInitializedSuccessfully = true

        try {

            if (AMQConnectionManagementService.topicSession == null) {
                consumerInitializedSuccessfully = false;
                LogsManagement.LogExceptionMesg("ERROR: Couldn't Initialize Consumer to Consume Messages because Connection was not Established Successfully with AMQ.")
            } else {

                //Getting Source name to assign to Topic to Consume data
                DestinationsNameUtils destinations = new DestinationsNameUtils()
                String destinationName = destinations.getTopicNameForConsumeMessages()

                //Creating Topic on which Consumer will consume messages
                topicToConsumeMessages = (javax.jms.Topic) new ActiveMQTopic(destinationName);

                //Creating publisher to publish the message with the topic as destination we just created
                consumer = AMQConnectionManagementService.topicSession.createConsumer(topicToConsumeMessages);
            }

        } catch (Exception ex) {
            consumer = null;
            consumerInitializedSuccessfully = false
            LogsManagement.LogExceptionMesg("Exception Occurred While Initializing AMQ Consumer to Consume Data: ", ex)
        }

        return consumerInitializedSuccessfully
    }

    def boolean startConsumingMessages() {

        boolean consumerStartedSuccessfully = true

        try {

            MessageListener listener = new MessageListener() {

                public void onMessage(Message message) {

                    try {

                        if (message instanceof TextMessage) {

                            TextMessage textMessage = (TextMessage) message;

                            //Now here we will create and save voice model

                            String userExtension = textMessage.getText()

                            println "Message Consumed from AMQ: " + userExtension

                            //Getting Format of the file whose Voice Model we are going to create
                            String fileFormat = properties['MEDIA_FILES_FORMAT'];

                            println fileFormat

                            String validatedFileFormat = ".mp3" //+ fileFormat;
                            String fileNameWithExtension = userExtension + validatedFileFormat

                            //Getting Media File Public URL to create model
                            URLsFormatter formatURLs = new URLsFormatter();
                            String mediaFilePublicURL = formatURLs.generateMediaFilesPublicURL(fileNameWithExtension)

                            LogsManagement.LogInfoMesg("INFO: Going to Create User Voice Model from Media File: " + mediaFilePublicURL + " With Extension: " + userExtension)

                            //Going to Create Voice Model. It will return the ID of the Model we just created
                            String modelID = voiceModelService.createUserVoiceModel(mediaFilePublicURL, "")

                            //If Some Error Occurred while Creating the Voice Model
                            if (modelID.equals("")) {
                                LogsManagement.LogInfoMesg("ERROR: Some Error Occurred While Processing Media File: " + mediaFilePublicURL + " While Creating Voice Model. See Error Logs for More Details.")
                            } else {

                                //If User Voice Model Created Successfully
                                LogsManagement.LogInfoMesg("INFO: Voice Model Created Successfully from Media File: " + mediaFilePublicURL + ". Model ID is: " + modelID)

                                LogsManagement.LogInfoMesg("INFO: Going to save Created Model: " + modelID + " in our Application database.")

                                //Going to save this Model in our Application database
                                boolean userModelSavedSuccessfully = voiceModelService.saveUserVoiceModel(modelID, userExtension)

                                //If Model Saved Successfully in our Application database
                                if (userModelSavedSuccessfully) {
                                    LogsManagement.LogInfoMesg("INFO: User Voice Model: " + modelID + " Saved Successfully in our Database.")
                                } else {
                                    //Some Error Occurred while saving created Model in our Application Database
                                    LogsManagement.LogInfoMesg("INFO: Some Error Occurred while Saving Just Created User Voice Model: " + modelID + " in our Database. See Error Logs for More Details.")
                                }
                            }
                        }

                    } catch (JMSException e) {
                        LogsManagement.LogExceptionMesg("Exception Occurred While Parsing and Processing Message Consumed from AMQ from Agent: ", e)
                    }
                }
            };

            this.consumer.setMessageListener(listener);

        } catch (Exception ex) {
            consumerStartedSuccessfully = false
            LogsManagement.LogExceptionMesg("")
        }

        return consumerStartedSuccessfully
    }

    def boolean terminateConsumer() {

        boolean consumerTerminatedSuccessfully = true

        try {

            if (this.consumer == null) {
                consumerTerminatedSuccessfully = false;
                LogsManagement.LogExceptionMesg("ERROR: Couldn't Terminate Consumer Successfully as Some Error was Occurred While Initializing Consumer.")
            } else {
                this.consumer.close();
            }

        } catch (Exception ex) {
            this.consumer = null
            consumerTerminatedSuccessfully = false;
            LogsManagement.LogExceptionMesg("Exception Occurred While Terminating AMQ Consumer: ", ex)
        }

        return consumerTerminatedSuccessfully
    }
}