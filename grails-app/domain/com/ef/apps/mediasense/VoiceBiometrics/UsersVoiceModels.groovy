package com.ef.apps.mediasense.VoiceBiometrics

class UsersVoiceModels {

    //System generated ID of the User
    String userID

    //ID of the Voice Model generated for user
    String modelID

    //Name of the Voice Model generated for user
    String modelName

    //The number from which user called to agent
    String extension

    //To maintain the date and time when the record was inserted
    Date dateCreated

    //To maintain the date and time when the record was updated last time
    Date lastUpdated

    static constraints = {
        userID unique: true
    }

    //Parameterized Constructor to Instentiate a new User's Voice Model
    public UsersVoiceModels(String userID, String modelID, String modelName, String extension) {
        this.userID = userID
        this.modelID = modelID
        this.modelName = modelName
        this.extension = extension
    }
}