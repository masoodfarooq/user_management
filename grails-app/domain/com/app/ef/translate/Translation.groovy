package com.app.ef.translate

import com.app.ef.security.User

class Translation {
    String text
    String translationKey
    Language language
    User updatedBy
    User createdBy
    static constraints = {
        text nullable: false, blank: false
        translationKey nullable: false, blank: false
        language nullable: false
        updatedBy nullable: true
        createdBy nullable: true
    }
    static mapping = {
        table("translation")
        text sqlType: 'nvarchar(max)'
    }
    String toString(){
        return text
    }
}
