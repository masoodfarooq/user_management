package com.app.ef.translate

import com.app.ef.security.User

class TranslationKey {
    String key_id
    User updatedBy
    User createdBy
    static constraints = {
        key_id nullable:false, blank: false
        updatedBy nullable: true
        createdBy nullable: true
    }
    static mapping = {
        table("translation_key")
        key_id sqlType: 'nvarchar(max)'
    }

    String toString(){
        return key_id
    }
}
