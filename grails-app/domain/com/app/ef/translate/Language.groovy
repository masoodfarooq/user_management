package com.app.ef.translate

import com.app.ef.security.User

class Language {
     String name
     String description
     String languageCode

    User updatedBy
    User createdBy
    static constraints = {
        name nullable: true, blank: true
        description nullable:  true , blank: true
        languageCode nullable: false, blank: false
        updatedBy nullable: true
        createdBy nullable: true
    }
    static mapping = {
        table("language")
        name sqlType: "nvarchar(max)"
        description sqlType: "nvarchar(max)"
        languageCode sqlType: "nvarchar(max)"
    }
    String toString(){
        return languageCode
    }
}
