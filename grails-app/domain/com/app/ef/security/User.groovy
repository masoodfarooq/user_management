package com.app.ef.security

import grails.rest.Resource

@Resource
class User {
    String username
    String password
    String fullName
    String email
    boolean isActive
    Date lastLogin
    Date dateCreated
    Date lastUpdated
    User createdBy
    User updatedBy

    static hasMany = [roles: Role, permissions: String]

    static constraints = {
        username(nullable: false, blank: false, size: 5..20,matches:"^[a-zA-Z][0-9a-zA-Z_]*\$",unique: true)
        password(minSize: 5, blank: false)
        fullName(maxSize: 100, blank: true,nullable:true)
        email(email: true, blank: true, unique: true,nullable:true,matches:"^[a-z0-9!#\$%&'*+/=?^_`{|}~.-]+@[a-z]+\\.com\$")
        lastLogin(nullable: true)
        createdBy(nullable: true)
        updatedBy(nullable: true)
    }

    String toString() {
        return username
    }

    static mapping = {
        table('cbr_user')
    }
}