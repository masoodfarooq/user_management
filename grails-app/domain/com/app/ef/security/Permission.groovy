package com.app.ef.security

class Permission {

    String name
    String expression
    String description

    static constraints = {
        name(blank: false)
        expression(blank: false, unique: true)
        description(blank: true, maxSize: 1000)
    }

    String toString() {
        return name
    }
}
