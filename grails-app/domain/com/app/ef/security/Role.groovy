package com.app.ef.security

class Role {
    String name
    String description
    Date dateCreated
    Date lastUpdated

    static belongsTo = [User]
    static hasMany = [members: User, permissions: String]

    static constraints = {
        name(size: 5..20, nullable: false, blank: false, unique: true)
        description(blank: true, maxSize: 1000,nullable: true)
    }

    String toString() {
        return name
    }

    String toFootPrintString() {
        def str = new StringBuilder("")

        try {
            str.append('name:' + name).append(', ')
            str.append('description:' + description).append(', ')
            str.append('dateCreated:' + dateCreated).append(', ')
            str.append('lastUpdated:' + lastUpdated).append(', ')
            str.append('members:' + members)
        }
        catch (Exception ex) {
            println 'error: ' + str
            log.info 'An exception occured. ${ex.toString()}'
        }

        return str
    }
}
