
<%@ page import="com.app.ef.translate.Language" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="translateMain">
		<g:set var="entityName" value="${message(code: 'language.label', default: 'Language')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-language" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-language" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'language.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'language.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="languageCode" title="${message(code: 'language.languageCode.label', default: 'Language Code')}" />
					
						<th><g:message code="language.updatedBy.label" default="Updated By" /></th>
					
						<th><g:message code="language.createdBy.label" default="Created By" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${languageInstanceList}" status="i" var="languageInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${languageInstance.id}">${fieldValue(bean: languageInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: languageInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: languageInstance, field: "languageCode")}</td>
					
						<td>${fieldValue(bean: languageInstance, field: "updatedBy")}</td>
					
						<td>${fieldValue(bean: languageInstance, field: "createdBy")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${languageInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
