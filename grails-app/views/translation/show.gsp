
<%@ page import="com.app.ef.translate.Translation" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="translateMain">
		<g:set var="entityName" value="${message(code: 'translation.label', default: 'Translation')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-translation" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-translation" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list translation">
			
				<g:if test="${translationInstance?.text}">
				<li class="fieldcontain">
					<span id="text-label" class="property-label"><g:message code="translation.text.label" default="Text" /></span>
					
						<span class="property-value" aria-labelledby="text-label"><g:fieldValue bean="${translationInstance}" field="text"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${translationInstance?.translationKey}">
				<li class="fieldcontain">
					<span id="translationKey-label" class="property-label"><g:message code="translation.translationKey.label" default="Translation Key" /></span>
					
						<span class="property-value" aria-labelledby="translationKey-label"><g:fieldValue bean="${translationInstance}" field="translationKey"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${translationInstance?.language}">
				<li class="fieldcontain">
					<span id="language-label" class="property-label"><g:message code="translation.language.label" default="Language" /></span>
					
						<span class="property-value" aria-labelledby="language-label"><g:link controller="language" action="show" id="${translationInstance?.language?.id}">${translationInstance?.language?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${translationInstance?.updatedBy}">
				<li class="fieldcontain">
					<span id="updatedBy-label" class="property-label"><g:message code="translation.updatedBy.label" default="Updated By" /></span>
					
						<span class="property-value" aria-labelledby="updatedBy-label"><g:link controller="user" action="show" id="${translationInstance?.updatedBy?.id}">${translationInstance?.updatedBy?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${translationInstance?.createdBy}">
				<li class="fieldcontain">
					<span id="createdBy-label" class="property-label"><g:message code="translation.createdBy.label" default="Created By" /></span>
					
						<span class="property-value" aria-labelledby="createdBy-label"><g:link controller="user" action="show" id="${translationInstance?.createdBy?.id}">${translationInstance?.createdBy?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:translationInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${translationInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
