<%@ page import="com.app.ef.translate.Translation" %>



<div class="fieldcontain ${hasErrors(bean: translationInstance, field: 'text', 'error')} required">
	<label for="text">
		<g:message code="translation.text.label" default="Text" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="text" required="" value="${translationInstance?.text}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: translationInstance, field: 'translationKey', 'error')} required">
	<label for="translationKey">
		<g:message code="translation.translationKey.label" default="Translation Key" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="translationKey" required="" value="${translationInstance?.translationKey}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: translationInstance, field: 'language', 'error')} required">
	<label for="language">
		<g:message code="translation.language.label" default="Language" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="language" name="language.id" from="${com.app.ef.translate.Language.list()}" optionKey="id" required="" value="${translationInstance?.language?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: translationInstance, field: 'updatedBy', 'error')} ">
	<label for="updatedBy">
		<g:message code="translation.updatedBy.label" default="Updated By" />
		
	</label>
	<g:select id="updatedBy" name="updatedBy.id" from="${com.app.ef.security.User.list()}" optionKey="id" value="${translationInstance?.updatedBy?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: translationInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="translation.createdBy.label" default="Created By" />
		
	</label>
	<g:select id="createdBy" name="createdBy.id" from="${com.app.ef.security.User.list()}" optionKey="id" value="${translationInstance?.createdBy?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

