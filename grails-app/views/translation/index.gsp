
<%@ page import="com.app.ef.translate.Translation" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="translateMain">
		<g:set var="entityName" value="${message(code: 'translation.label', default: 'Translation')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-translation" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-translation" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="text" title="${message(code: 'translation.text.label', default: 'Text')}" />
					
						<g:sortableColumn property="translationKey" title="${message(code: 'translation.translationKey.label', default: 'Translation Key')}" />
					
						<th><g:message code="translation.language.label" default="Language" /></th>
					
						<th><g:message code="translation.updatedBy.label" default="Updated By" /></th>
					
						<th><g:message code="translation.createdBy.label" default="Created By" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${translationInstanceList}" status="i" var="translationInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${translationInstance.id}">${fieldValue(bean: translationInstance, field: "text")}</g:link></td>
					
						<td>${fieldValue(bean: translationInstance, field: "translationKey")}</td>
					
						<td>${fieldValue(bean: translationInstance, field: "language")}</td>
					
						<td>${fieldValue(bean: translationInstance, field: "updatedBy")}</td>
					
						<td>${fieldValue(bean: translationInstance, field: "createdBy")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${translationInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
