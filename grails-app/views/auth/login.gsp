<div class="" id="login-box" style="width: 250px; padding-left: 0px;padding-top: 0px " ng-controller="loginController">
    <div class="header hidden"><span translate="com.ef.cbr.signIn"/> </div>
    <div class="error" style="margin-bottom: 20px">
        <div ng-show="loginError() == 'error'"> <span class="label label-danger" translate="com.ef.cbr.login.failed"></span></div>
        <div ng-show="loginError() == 'isNotActive'"> <span class="label label-danger" translate="com.ef.cbr.login.isNotActive"></span></div>

    </div>
    <form name="loginForm" ng-submit="signIn(login.$invalid,login)" method="post">
        <div class="body bg-gray">

            <div class="form-group">
                <input type="text" ng-rerquired="true" ng-model="login.userId" name="userid" class="form-control"
                       placeholder="{{'com.ef.cbr.label.enterText' | translate:{value:translateKey('com.ef.cbr.user.label.username')} }}"/>
            </div>
            <div ng-show="loginForm.$submitted || loginForm.login.userId.$touched">
                <div ng-show="loginForm.login.userId.$error.required"><span translate="com.ef.cbr.form.cannot.null"></span></div>
            </div>
            <div class="form-group">
                <input type="password" name="password" ng-rerquired="true" ng-model="login.password" class="form-control"
                       placeholder="{{'com.ef.cbr.label.enterText' | translate:{value:translateKey('com.ef.cbr.user.label.password')} }}"/>
            </div>
            <div ng-show="loginForm.$submitted || loginForm.login.password.$touched">
                <div ng-show="loginForm.login.password.$error.required"><span translate="com.ef.cbr.form.cannot.null"></span></div>
            </div>
            <div class="form-group">
                <input type="checkbox" ng-model="login.remember"/>
                <span translate="com.ef.cbr.form.rememberMe"></span>
            </div>
        </div>
        <div class="footer ">
            <button type="submit"  ng-click="loginForm.$submitted=true"
                    class="btn bg-light-blue btn-block"><span translate="com.ef.cbr.form.signMeIn"></span></button>

            <p class="hidden"><a href="#">I forgot my password</a></p>

            <a href="register.html" class="text-center hidden">Register a new membership</a>
        </div>
    </form>

    <div class="margin text-center hidden">
        <span>Sign in using social networks</span>
        <br/>
        <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
        <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
        <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>

    </div>
</div>
