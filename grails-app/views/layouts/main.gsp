<!doctype html>
<html lang="en" ng-app="cbrapp">
<head>
    <meta charset="UTF-8">
    <title>${grailsApplication.metadata['app.name']}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="css/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- fullCalendar -->
    <link href="css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- NG TABLES -->
    <link href="js/plugins/ngtable/ng-table.min.css" rel="stylesheet" type="text/css" />
    <link href="css/application.css" rel="stylesheet" type="text/css" />
    <!--X editable -->
    <link href="js/plugins/angular-xeditable/css/xeditable.css" rel="stylesheet">
    <!-- toaster-->
    <link href="css/toaster.css" rel="stylesheet" type="text/html"/>
    <link href="css/bootstrap-switch.min.css" rel="stylesheet" type="text/html"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
</head>
<body class="skin-blue" ng-controller="ApplicationController">
<toaster-container toaster-options="{'limit': 0,'tap-to-dismiss': true,
'position-class': 'toast-bottom-left',
'close-button':true,
'newest-on-top': true,
'show-duration': '100',
'hide-duration': '1000',
'time-out': '5000',
'extended-time-out': '1000',
'show-easing': 'swing',
'hide-easing': 'linear',
'show-method': 'fadeIn',
'hide-method': 'fadeOut'
}"></toaster-container>
<g:render template="/templates/topMenu"/>

<div class="wrapper row-offcanvas row-offcanvas-left">
    <g:render template="/templates/sideMenu"/>
    <g:layoutBody/>

</div>



<!-- jQuery 2.0.2 -->
<script src="js/jquery-2.1.1.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/lodash.js" type="text/javascript"></script>
<script src="js/moment.js" type="text/javascript"></script>
<!-- Morris.js charts -->
<script src="js/raphael-min.js"></script>
<script src="js/plugins/morris/morris.min.js" type="text/javascript"></script>
<!-- Sparkline -->
<script src="js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- jvectormap -->
<script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
<script src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
<!-- fullCalendar -->
<script src="js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- iCheck -->
<script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="js/AdminLTE/app.js" type="text/javascript"></script>
<!-- Angular -->
<script src="js/angular.js" type="text/javascript"></script>
<script src="js/angular-ui.js" type="text/javascript"></script>
<script src="js/angular-animate.min.js" type="text/javascript"></script>
<script src="js/angular-resource.min.js" type="text/javascript">
</script><script src="js/angular-resource.js" type="text/javascript"></script>
<script src="js/angular-route.min.js" type="text/javascript"></script>
<script src="js/plugins/ngtable/ng-table.js" type="text/javascript"></script>
<script src="js/toaster.js" type="text/javascript"></script>
<script src="js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="js/custom-form-elements.js" type="text/javascript"></script>
<script src="js/ngStorage.min.js" type="text/javascript"></script>

<!--X editable -->
<script src="js/plugins/angular-xeditable/js/xeditable.js"></script>
<script src="js/bootstrap-editable.js"></script>
<script src="js/bootstrap-hover.js"></script>
<script src="js/ui-bootstrap.min.js"></script>
<script src="js/angular-translate.min.js"></script>
<script src="js/angular-translate-loadUrl.min.js"></script>
<script src="js/angular-cookies.min.js"></script>
<script src="js/angular-translate-storage-cookies.min.js"></script>
<script src="js/angular-moment.min.js"></script>
<script src="js/angular-bootstrap-switch.min.js"></script>



<!-- Angular App -->
<script src="js/angular/app.js" type="text/javascript"></script>
<!-- Interceptor-->
<script src="js/angular/controllers/auth/AuthInterceptor.js" type="text/javascript"></script>
<!-- Auth -->
<script src="js/angular/controllers/auth/PermissionService.js" type="text/javascript"></script>
<script src="js/angular/controllers/auth/AuthService.js" type="text/javascript"></script>
<script src="js/angular/controllers/auth/Session.js" type="text/javascript"></script>

<script src="js/angular/controllers/mainController.js" type="text/javascript"></script>
<!-- Directivs -->
<script src="js/angular/directives/strip.js" type="text/javascript"></script>
<script src="js/angular/directives/ng-enter.js" type="text/javascript"></script>
<script src="js/angular/directives/ex-editable.js" type="text/javascript"></script>
<script src="js/angular/directives/sideMenuAuthentication.js" type="text/javascript"></script>

<!-- Services-->
<script src="js/angular/services/userResourceService.js" type="text/javascript"></script>
<script src="js/angular/services/roleResourceService.js" type="text/javascript"></script>
<script src="js/angular/services/permissionResourceService.js" type="text/javascript"></script>
<script src="js/angular/services/utils.js" type="text/javascript"></script>
<script src="js/angular/controllers/confirm_dialog_ctrl.js" type="text/javascript"></script>
<!-- User-->
<script src="js/angular/controllers/user/detailController.js" type="text/javascript"></script>
<script src="js/angular/controllers/user/listController.js" type="text/javascript"></script>
<!-- Permission-->
<script src="js/angular/controllers/permission/listController.js" type="text/javascript"></script>
<!-- Role-->
<script src="js/angular/controllers/role/listController.js" type="text/javascript"></script>
<script src="js/angular/controllers/role/createController.js" type="text/javascript"></script>
<script src="js/angular/controllers/role/detailController.js" type="text/javascript"></script>
<!-- Login -->
<script src="js/angular/controllers/loginController.js" type="text/javascript"></script>

<script src="js/angular/controllers/user/addController.js" type="text/javascript"></script>
<script src="js/angular/controllers/controllers.js" type="text/javascript"></script>
<script>
    var appBaseUrl = '/'+'${grailsApplication.metadata['app.name']}';



</script>

</body>
</html>