<header class="header">
<a    class="logo">
    <!-- Add the class icon to your logo image or logo icon to add the margining -->
    <img alt="Expertflow Logo"  src="${resource(dir: 'images', file: 'logo2.png')}">
</a>

<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
<!-- Sidebar toggle button-->
<a  class="navbar-btn sidebar-toggle loginHideShow" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</a>
<div class="navbar-right">
<ul class="nav navbar-nav">
<!-- Messages: style can be found in dropdown.less-->

<!-- Notifications: style can be found in dropdown.less -->

<!-- Tasks: style can be found in dropdown.less -->
%{--<li class="dropdown tasks-menu">
    <a  class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-tasks"></i>
        <span class="label label-danger">9</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have 9 tasks</li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <li><!-- Task item -->
                    <a >
                        <h3>
                            Design some buttons
                            <small class="pull-right">20%</small>
                        </h3>
                        <div class="progress xs">
                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">20% Complete</span>
                            </div>
                        </div>
                    </a>
                </li><!-- end task item -->
                <li><!-- Task item -->
                    <a >
                        <h3>
                            Create a nice theme
                            <small class="pull-right">40%</small>
                        </h3>
                        <div class="progress xs">
                            <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">40% Complete</span>
                            </div>
                        </div>
                    </a>
                </li><!-- end task item -->
                <li><!-- Task item -->
                    <a >
                        <h3>
                            Some task I need to do
                            <small class="pull-right">60%</small>
                        </h3>
                        <div class="progress xs">
                            <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">60% Complete</span>
                            </div>
                        </div>
                    </a>
                </li><!-- end task item -->
                <li><!-- Task item -->
                    <a >
                        <h3>
                            Make beautiful transitions
                            <small class="pull-right">80%</small>
                        </h3>
                        <div class="progress xs">
                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">80% Complete</span>
                            </div>
                        </div>
                    </a>
                </li><!-- end task item -->
            </ul>
        </li>
        <li class="footer">
            <a >View all tasks</a>
        </li>
    </ul>
</li>--}%
<!-- User Account: style can be found in dropdown.less -->
<li class="dropdown user user-menu hidden loginHideShow">
    <a  class="dropdown-toggle" data-toggle="dropdown">
        <i class="glyphicon glyphicon-user"></i>
        <span>{{getUsername()}} <i class="caret"></i></span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li  class="user-header bg-light-blue">
            <img src="img/avatar3.png" class="img-circle" alt="User Image" />
            <p>
                {{getUsername()}}
                <small>Member since Nov. 2012</small>
            </p>
        </li>

        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a  ng-click="profilePage()" class="btn btn-default btn-flat"><span translate="com.ef.cbr.user.label.profile"></span></a>
            </div>
            <div class="pull-right">
                <a  href="#" ng-click="logout()" class="btn btn-default btn-flat">Sign out</a>
            </div>
        </li>
    </ul>

</li>
<li class="dropdown hidden signInHideShow user user-menu">
    <a  class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-sign-in"></i>
        <span><span translate="com.ef.cbr.signIn"></span>  <i class="caret"></i></span>
    </a>
    <ul class="dropdown-menu">
        <li class="user-footer">
            <g:include view="auth/login.gsp"/>
        </li>
    </ul>
</li>
    <li class="dropdown dropdown-btn js-language-dropdown">
        <a class="js-language-link dropdown-toggle" href="#" >
        <img class="" src="${resource(dir: 'img/flags/flags',file: '{{currentLang()}}'+'.png')}"/>
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" role="menu">
        <!-- get list of all locales available due to an existing property-file in /grails-app/i18n (set once in Bootstrap.groovy) -->
            <g:set var="allLocales" value="${grailsApplication.config.grails.i18n.locales}"/>
            <g:each status="i" var="locale" in="${allLocales}">
                <li>
                    <a ng-click="changeLanguage('${locale}')"
                       title="${message(code: 'language.'+locale, default: locale)}" data-lang-code="${locale}">
                <img class="" src="${resource(dir: 'img/flags/flags',file: locale+'.png')}"/>
                        <span translate="default.language.${locale}"></span>
                       </a>
                </li>
            </g:each>
        </ul>
    </li>
</ul>
</div>
</nav>
</header>