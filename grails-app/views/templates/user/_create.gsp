<div ng-controller="addNewUserController">
    <form class="form-horizontal">
        <h1 class="page-header">New User</h1>
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="username">Username</label>
                <div class="controls">
                    <input id="username" type="text" class="input-block-level"
                           required="true" ng-model="user.username">
                </input>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="password">Password</label>
                <div class="controls">
                    <input id="password" type="text" class="input-block-level"
                           required="true" ng-model="user.password">
                </input>
                </div>
            </div>
        </fieldset>
        <div class="form-actions">
            <button class="btn btn-primary" ng-click="saveUser()">Create</button>
        </div>
    </form>
</div>