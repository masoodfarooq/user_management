
    <!-- Left side column. contains the logo and sidebar -->
    <aside   class="left-side collapse-left sidebar-offcanvas" >
               {{permissionsl()}}
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
        <div class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" ng-model="search" autocomplete="off" class="form-control" placeholder="Search..." />
                    <span class="input-group-btn">
                        <button ng-click="searchMenu(search);" class="btn btn-flat"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu search-menu"></ul>
            <ul class="sidebar-menu normal-menu">
                <li class="active">
                    <a href="#/index">
                        <i class="fa fa-home"></i> <span>Dashboard</span>
                    </a>
                </li>
                %{--<li class="treeview">
                    <a >
                        <i class="fa fa-globe"></i>
                        <span translate="com.ef.cbr.label.management" translate-values="{name:translateKey('com.ef.cbr.label.business')}"></span>

                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#/businessUnit"><i class="fa fa-angle-double-right"></i><span translate="con.ef.cbr.label.busineUnit.plural"></span></a><small  ><a class="badge pull-right bg-green align_A" href="#">+</a></small></li>
                        <li><a href="#/service"><i class="fa fa-angle-double-right"></i><span translate="con.ef.cbr.label.service.plural"></span></a><small ><a class="badge pull-right bg-green align_A" href="#">+</a></small></li>
                        <li><a href="#/region"><i class="fa fa-angle-double-right"></i><span translate="con.ef.cbr.label.region.plural"></span></a><small ><a class="badge pull-right bg-green align_A" href="#">+</a></small></li>
                        <li><a href="#/schedule"><i class="fa fa-angle-double-right"></i><span translate="con.ef.cbr.label.event.plural"></span></a><small ><a class="badge pull-right bg-green align_A" href="#">+</a></small></li>
                        <li><a href="#/kpi"><i class="fa fa-angle-double-right"></i><span translate="con.ef.cbr.label.kpi.plural"></span></a><small ><a class="badge pull-right bg-green align_A" href="#">+</a></small></li>
                        <li><a href="#/serviceKpi"><i class="fa fa-angle-double-right"></i><span translate="con.ef.cbr.label.serviceKpi.plural"></span></a><small ><a class="badge pull-right bg-green align_A" href="#">+</a></small></li>
                    </ul>
                </li>
                <li class="active">
                    <a href="index.html">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#/widgets">
                        <i class="fa fa-th"></i> <span>Widgets</span> <small class="badge pull-right bg-green">new</small>
                    </a>
                </li>--}%
                <li class="treeview">
                    <a >
                        <i class="glyphicon glyphicon-user"></i>
                        <span translate="com.ef.cbr.label.management" translate-values="{name:translateKey('com.ef.cbr.label.user')}"></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li ng-if="hasPermission('user:index')"><a href="#/user/list"><i class="fa fa-users"></i><span translate="com.ef.cbr.label.user.plural"></span> </a><small  ><a ng-if="hasPermission('user:create')" class="badge pull-right bg-green align_A" href="#/user/create">+</a></small></li>
                        <li ng-if="hasPermission('role:index')"><a href="#/role/list"><i class="fa fa-book"></i><span translate="com.ef.cbr.label.role.plural"></span> </a><small  ><a ng-if="hasPermission('role:create')" class="badge pull-right bg-green align_A" href="#/role/create">+</a></small></li>
                        <li ng-if="hasPermission('permission:index')"><a href="#/permission/list"><i class="fa fa-pinterest-square"></i><span translate="com.ef.cbr.label.permission.plural"></span> </a></li>
                    </ul>
                </li>
               %{-- <li class="treeview">
                    <a >
                        <i class="fa fa-laptop"></i>
                        <span>UI Elements</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#/general"><i class="fa fa-angle-double-right"></i> General</a></li>
                        <li><a href="#/icons"><i class="fa fa-angle-double-right"></i> Icons</a></li>
                        <li><a href="#/buttons"><i class="fa fa-angle-double-right"></i> Buttons</a></li>
                        <li><a href="#/sliders"><i class="fa fa-angle-double-right"></i> Sliders</a></li>
                        <li><a href="#/timeline"><i class="fa fa-angle-double-right"></i> Timeline</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a >
                        <i class="fa fa-edit"></i> <span>Forms</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#/generalform"><i class="fa fa-angle-double-right"></i> General Elements</a></li>
                        <li><a href="#/advanced"><i class="fa fa-angle-double-right"></i> Advanced Elements</a></li>
                        <li><a href="#/editors"><i class="fa fa-angle-double-right"></i> Editors</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a >
                        <i class="fa fa-table"></i> <span>Tables</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#/simple"><i class="fa fa-angle-double-right"></i> Simple tables</a></li>
                        <li><a href="#/data"><i class="fa fa-angle-double-right"></i> Data tables</a></li>
                        <li><a href="#/ngtable"><i class="fa fa-angle-double-right"></i> NG table</a></li>
                        <li><a href="#/ngtable-advanced"><i class="fa fa-angle-double-right"></i> NG table advanced</a></li>
                        <li><a href="#/ngtable-xeditable"><i class="fa fa-angle-double-right"></i> NG table xeditable</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#/check/test">
                        <i class="fa fa-calendar"></i> <span>Check</span>
                        <small class="badge pull-right bg-red">3</small>
                    </a>
                </li>
                <li>
                    <a href="#/mailbox">
                        <i class="fa fa-envelope"></i> <span>Mailbox</span>
                        <small class="badge pull-right bg-yellow">12</small>
                    </a>
                </li>
                <li class="treeview">
                    <a >
                        <i class="fa fa-folder"></i> <span>Examples</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#/invoice"><i class="fa fa-angle-double-right"></i> Invoice</a></li>
                        <li><a href="pages/examples/login.html"><i class="fa fa-angle-double-right"></i> Login</a></li>
                        <li><a href="pages/examples/register.html"><i class="fa fa-angle-double-right"></i> Register</a></li>
                        <li><a href="pages/examples/lockscreen.html"><i class="fa fa-angle-double-right"></i> Lockscreen</a></li>
                        <li><a href="#/404"><i class="fa fa-angle-double-right"></i> 404 Error</a></li>
                        <li><a href="#/500"><i class="fa fa-angle-double-right"></i> 500 Error</a></li>
                        <li><a href="#/blank"><i class="fa fa-angle-double-right"></i> Blank Page</a></li>
                    </ul>
                </li>--}%
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <!-- /.right-side -->
    %{--<sidemenuauth signedin="isAuthorized1()"/>--}%
