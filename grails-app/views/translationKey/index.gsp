
<%@ page import="com.app.ef.translate.TranslationKey" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="translateMain">
		<g:set var="entityName" value="${message(code: 'translationKey.label', default: 'TranslationKey')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-translationKey" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-translationKey" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="key_id" title="${message(code: 'translationKey.key_id.label', default: 'Keyid')}" />
					
						<th><g:message code="translationKey.updatedBy.label" default="Updated By" /></th>
					
						<th><g:message code="translationKey.createdBy.label" default="Created By" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${translationKeyInstanceList}" status="i" var="translationKeyInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${translationKeyInstance.id}">${fieldValue(bean: translationKeyInstance, field: "key_id")}</g:link></td>
					
						<td>${fieldValue(bean: translationKeyInstance, field: "updatedBy")}</td>
					
						<td>${fieldValue(bean: translationKeyInstance, field: "createdBy")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${translationKeyInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
