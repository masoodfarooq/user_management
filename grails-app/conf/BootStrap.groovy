import com.app.ef.security.Permission
import com.app.ef.security.User
import com.app.ef.translate.Language
import com.app.ef.translate.Translation
import com.app.ef.translate.TranslationKey
import com.ef.apps.mediasense.Utils.LogsManagement
import org.apache.shiro.crypto.hash.Sha256Hash

class BootStrap {

    //Injecting voice recordign service
    def processRecordingVoiceRecognizitonService

    //Injecting Service to Connect to AMQ Server to Send Recording matching results to agent's desktop for Voice Biometrics
    def AMQConnectionManagementService

    //Injecting AMQ Publisher to Publish Voice Recognition Results
    def messagePublisherService

    //Injecting AMQ Consumer to Consume Voice Model Creation Notifications
    def messageConsumerService


    def init = { servletContext ->

        if (!User.findByUsername("admin")) {
            println "Reach in bootstrap class if"
            log.info "-----------------New values entering into table ------------------------"

            // User related Permissions
            new Permission( expression: 'user:*', name: 'com.ef.pcs.bootstrap.all.user.name',
                    description: 'com.ef.pcs.bootstrap.all.user').save()
            new Permission( expression: 'user:show', name: 'com.ef.pcs.bootstrap.show.user.name',
                    description: 'com.ef.pcs.bootstrap.show.user').save()
            new Permission( expression: 'user:save,usernameExist,emailExist,save', name: 'com.ef.pcs.bootstrap.create.user.name',
                    description: 'com.ef.pcs.bootstrap.create.user').save()
            new Permission( expression: 'user:edit,update', name: 'com.ef.pcs.bootstrap.edit.user.name',
                    description: 'com.ef.pcs.bootstrap.edit.user').save()
            new Permission( expression: 'user:index', name: 'com.ef.pcs.bootstrap.list.user.name',
                    description: 'com.ef.pcs.bootstrap.list.user').save()
            new Permission( expression: 'user:delete,deleteSelected', name: 'com.ef.pcs.bootstrap.delete.user.name',
                    description: 'com.ef.pcs.bootstrap.delete.user').save()
            new Permission( expression: 'user:activateOrBlockUser', name: 'com.ef.pcs.bootstrap.activate.user.name',
                    description: 'com.ef.pcs.bootstrap.activate.user').save()
             new Permission( expression: 'user:viewRoles', name: 'com.ef.pcs.bootstrap.role.user.name',
                    description: 'com.ef.pcs.bootstrap.role.user').save()
            new Permission( expression: 'user:assignAndRevokeRole', name: 'com.ef.pcs.bootstrap.assignRole.user.name',
                    description: 'com.ef.pcs.bootstrap.assignRole.user').save()
             new Permission( expression: 'user:viewPermissions', name: 'com.ef.pcs.bootstrap.viewPermission.user.name',
                    description: 'com.ef.pcs.bootstrap.viewPermission.user').save()
            new Permission( expression: 'user:assignAndRevokePermission', name: 'com.ef.pcs.bootstrap.assignPermission.user.name',
                    description: 'com.ef.pcs.bootstrap.assignPermission.user').save()
            new Permission( expression: 'user:changePassword', name: 'com.ef.pcs.bootstrap.changePassword.user.name',
                    description: 'com.ef.pcs.bootstrap.resetPassword.user').save()

            // Role related permissions
            new Permission( expression: 'role:*', name: 'com.ef.pcs.bootstrap.all.role.name',
                    description: 'com.ef.pcs.bootstrap.all.role').save()
            new Permission( expression: 'role:show', name: 'com.ef.pcs.bootstrap.show.role.name',
                    description: 'com.ef.pcs.bootstrap.show.role').save()
            new Permission( expression: 'role:save,nameExist', name: 'com.ef.pcs.bootstrap.create.role.name',
                    description: 'Can create new Role').save()
            new Permission( expression: 'role:edit,update', name: 'com.ef.pcs.bootstrap.edit.role.name',
                    description: 'com.ef.pcs.bootstrap.edit.role').save()
            new Permission( expression: 'role:index', name: 'com.ef.pcs.bootstrap.list.role.name',
                    description: 'com.ef.pcs.bootstrap.list.role').save()
            new Permission( expression: 'role:delete,deleteSelected', name: 'com.ef.pcs.bootstrap.delete.role.name',
                    description: 'com.ef.pcs.bootstrap.delete.role').save()
            new Permission( expression: 'role:viewUsers', name: 'com.ef.pcs.bootstrap.viewMember.role.name',
                    description: 'com.ef.pcs.bootstrap.viewMember.role').save()
            new Permission( expression: 'role:viewPermissions', name: 'com.ef.pcs.bootstrap.viewPermission.role.name',
                    description: 'com.ef.pcs.bootstrap.viewPermission.role').save()
            new Permission( expression: 'role:assignAndRevokePermission', name: 'com.ef.pcs.bootstrap.assignPermission.role.name',
                    description: 'com.ef.pcs.bootstrap.assignPermission.role').save()
            new Permission( expression: 'role:assignAndRevokeUser', name: 'com.ef.pcs.bootstrap.addMember.role.name',
                    description: 'com.ef.pcs.bootstrap.addMember.role').save()
            // Default Administrator User

            //Permission
            new Permission( expression: 'permission:index', name: 'com.ef.pcs.bootstrap.permission.list.name',
                    description: 'com.ef.pcs.bootstrap.permissions.list').save()

            def admin

            admin = new User(username: "admin",
                    password: new Sha256Hash("admiN123!").toHex(),
                    isActive: true
            )
            admin.addToPermissions("*:*")
            admin.save()

        }

        /*** Uncomment Below Lines to Active Communication Through Vert.x Instead of AMQ ***/

        //Connecting to Server to send recording recognition results

        /*LogsManagement.LogInfoMesg("INFO: Going to Connect to Server to Send Recordings Voice Recognition Results in Bootstrap File")

        boolean connectedSuccessfully = establishConnectionService.connect()

        if (!connectedSuccessfully) {
            LogsManagement.LogInfoMesg("INFO: Some Error Occurred while Connecting to Server for Voice Biometrics. See Error Logs for Further Details.")
        }*/

        /*** End Vert.x Module ***/

        /*** AMQ Implementation for Establishing Connection ***/

        LogsManagement.LogInfoMesg("INFO: Going to Establish Connection with AMQ to Send Recordings Voice Recognition Results in Bootstrap File")

        //Establishing Connection With AMQ to Send Recording matching results to agent's desktop for Voice Biometrics
        boolean connectionEstablishedSuccessfully = AMQConnectionManagementService.establishConnectionWithAMQ()

        if (connectionEstablishedSuccessfully) {

            LogsManagement.LogInfoMesg("INFO: Connection Established Successfully with AMQ to Communicate Recordings Voice Recognition Results in Voice Biometrics.")

            //Going to Initializing Publisher to Publish Message

            LogsManagement.LogInfoMesg("INFO: Going to Initialize AMQ Publisher to Publish Recordings Voice Recognition Results in Bootstrap File")

            //Establishing Connection With AMQ to Send Recording matching results to agent's desktop for Voice Biometrics
            boolean publisherInitializedSuccessfully = messagePublisherService.initializeMessagePublisher()

            if (publisherInitializedSuccessfully) {
                LogsManagement.LogInfoMesg("INFO: AMQ Publisher Initialized Successfully to Communicate Recordings Voice Recognition Results in Voice Biometrics.")
            } else {
                LogsManagement.LogInfoMesg("INFO: Some Error Occurred while Initializing AMQ Publisher for Voice Biometrics. See Error Logs for Further Details.")
            }

            //*******************Setting Up Consumer*******************//

            //Going to Initializing Consumer to Consume Messages

            LogsManagement.LogInfoMesg("INFO: Going to Initialize AMQ Consumer to Receive Voice Model Creation Notifications for Voice Biometrics in Bootstrap File")

            //Establishing Connection With AMQ to receive voice model creation related notifications for Voice Biometrics
            boolean consumerInitializedSuccessfully = messageConsumerService.initializeMessageConsumer()

            if (consumerInitializedSuccessfully) {
                LogsManagement.LogInfoMesg("INFO: AMQ Consumer Initialized Successfully to Receive Voice Model Creation Notifications for Voice Biometrics in Bootstrap File.")

                LogsManagement.LogInfoMesg("INFO: Going to Starting AMQ Initialized Consumer to Receive Voice Model Creation Notifications for Voice Biometrics in Bootstrap File.")

                //Once Message Consumer has been initialized successfully we need to start/configure it for consuming messages continuously
                boolean messageConsumerStartedSuccessfully = messageConsumerService.startConsumingMessages()

                //Checking If the message consumer service has been started properly or not
                if (messageConsumerStartedSuccessfully) {
                    LogsManagement.LogInfoMesg("INFO: AMQ Consumer Started Successfully to Communicate Recordings Voice Recognition Results in Voice Biometrics.")
                } else {
                    LogsManagement.LogInfoMesg("INFO: Some Error Occurred while Starting AMQ Consumer to Receive Voice Model Creation Notifications for Voice Biometrics in Bootstrap File. See Error Logs for Further Details.")
                }

            } else {
                LogsManagement.LogInfoMesg("INFO: Some Error Occurred while Initializing AMQ Consumer to Receive Voice Model Creation Notifications for Voice Biometrics in Bootstrap File. See Error Logs for Further Details.")
            }


            //Sending Sample Testing Message
            messagePublisherService.sendMessage("Hello: This is Testing Message...!!!")

        } else {
            LogsManagement.LogInfoMesg("INFO: Some Error Occurred while Establishing Connection with AMQ for Voice Biometrics. See Error Logs for Further Details.")
        }

        /*** End AMQ Implementation for Establishing Connection ***/
        }
    def destroy = {


        /*** Uncomment Below Lines to Active Communication Through Vert.x Instead of AMQ ***/

        /*
        LogsManagement.LogInfoMesg("INFO: Going to Close Connection with Server regarding Voice Biometrics.")

        //Closing Connection
        boolean connectionClosedSuccessfully = establishConnectionService.closeConnection()

        if (connectionClosedSuccessfully) {
            LogsManagement.LogInfoMesg("INFO: Connection Terminated Successfully.")
        } else {
            LogsManagement.LogInfoMesg("INFO: Some Error Occurred while Terminating Connection. See Error Logs for More Details.")
        }
        */

        /*** End Vert.x Module ***/

        /*** AMQ Implementation for Terminating Connection ***/

        //Terminating AMQ Provider Connections
        LogsManagement.LogInfoMesg("INFO: Going to Terminate Connection with AMQ to Send Recordings Voice Recognition Results in Bootstrap File")

        boolean connectionTerminatedSuccessfully = AMQConnectionManagementService.terminateConnectionWithAMQ()

        if (connectionTerminatedSuccessfully) {
            LogsManagement.LogInfoMesg("INFO: Connection Terminated Successfully with AMQ to Communicate Recordings Voice Recognition Results in Voice Biometrics.")
        } else {
            LogsManagement.LogInfoMesg("INFO: Some Error Occurred while Terminating Connection with AMQ for Voice Biometrics. See Error Logs for Further Details.")
        }

        //Terminating AMQ Publisher Connections
        LogsManagement.LogInfoMesg("INFO: Going to Terminate Connection with AMQ to Send Recordings Voice Recognition Results in Bootstrap File")

        boolean publisherTerminatedSuccessfully = messagePublisherService.terminatePublisher()

        if (publisherTerminatedSuccessfully) {
            LogsManagement.LogInfoMesg("INFO: AMQ Publisher Connection Terminated Successfully to Communicate Recordings Voice Recognition Results in Voice Biometrics.")
        } else {
            LogsManagement.LogInfoMesg("INFO: Some Error Occurred while Terminating AMQ Publisher for Voice Biometrics. See Error Logs for Further Details.")
        }

        //Terminating AMQ Consumer Connections
        LogsManagement.LogInfoMesg("INFO: Going to Terminate Consumer Connection with AMQ to Receive Voice Model Creation Notifications in Bootstrap File")

        boolean consumerTerminatedSuccessfully = messageConsumerService.terminateConsumer()

        if (consumerTerminatedSuccessfully) {
            LogsManagement.LogInfoMesg("INFO: AMQ Consumer Connection Terminated Successfully to Receive Voice Model Creation Notifications in Voice Biometrics.")
        } else {
            LogsManagement.LogInfoMesg("INFO: Some Error Occurred while Terminating AMQ Consumer for Voice Biometrics. See Error Logs for Further Details.")
        }

        /*** End AMQ Implementation for Terminating Connection ***/
    }
}
