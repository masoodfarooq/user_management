import org.apache.log4j.DailyRollingFileAppender
import org.apache.log4j.PatternLayout
import org.apache.log4j.RollingFileAppender

// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

//To get the name of the application
//Source: http://stackoverflow.com/questions/9809566/how-to-get-grails-application-name-within-config-groovy
appName = grails.util.Metadata.current.'app.name'


grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination

// The ACCEPT header will not be used for content negotiation for user agents containing the following strings (defaults to the 4 major rendering engines)
grails.mime.disable.accept.header.userAgents = ['Gecko', 'WebKit', 'Presto', 'Trident']
grails.mime.types = [ // the first one is the default format
    all:           '*/*', // 'all' maps to '*' or the first available format in withFormat
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    hal:           ['application/hal+json','application/hal+xml'],
    xml:           ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// Legacy setting for codec used to encode data with ${}
grails.views.default.codec = "html"

// The default scope for controllers. May be prototype, session or singleton.
// If unspecified, controllers are prototype scoped.
grails.controllers.defaultScope = 'singleton'

// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside ${}
                scriptlet = 'html' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        // filteringCodecForContentType.'text/html' = 'html'
    }
}


grails.converters.encoding = "UTF-8"
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

// configure passing transaction's read-only attribute to Hibernate session, queries and criterias
// set "singleSession = false" OSIV mode in hibernate configuration after enabling
grails.hibernate.pass.readonly = false
// configure passing read-only to OSIV session by default, requires "singleSession = false" OSIV mode
grails.hibernate.osiv.readonly = false

environments {
    development {
        grails.logging.jul.usebridge = true
    }
    production {
        grails.logging.jul.usebridge = false
        // TODO: grails.serverURL = "http://www.changeme.com"
    }
}
grails.i18n.locales = ["en","fr"]

//this is for setting default path of jaxr plugin:
org.grails.jaxrs.url.mappings=['/MediaSenseEvents']

// log4j configuration
log4j = {
    def layoutPattern = new PatternLayout("[%d{ISO8601}] %m \r\n")

    def InfoFile = new DailyRollingFileAppender(
            name: "InfoFileAppender",
            layout: layoutPattern,
            //Path of the Log File
            fileName: "C:\\user_management-Logs\\Application\\MSLogs.log",
            datePattern: "'.'dd-MM-yyyy");

    def ExceptionFile = new DailyRollingFileAppender(
            name: "ExceptionFileAppender",
            layout: layoutPattern,
            //Path of the Log File
            fileName: "C:\\user_management-Logs\\Exceptions\\ExceptionLogs.log",
            datePattern: "'.'dd-MM-yyyy");

    //For logging exceptions stack trace
    appenders {
        appender InfoFile
        appender ExceptionFile
    }

    root {
        error 'ExceptionFileAppender'
        info 'InfoFileAppender'
    }

    //To make it sure that It will just Log, Messages by Info Logger
    InfoFile.addFilter(new org.apache.log4j.varia.LevelMatchFilter(levelToMatch: 'INFO', acceptOnMatch: true))
    InfoFile.addFilter(new org.apache.log4j.varia.DenyAllFilter())

    //To make it sure that It will just Log, Messages by ERROR Logger
    ExceptionFile.addFilter(new org.apache.log4j.varia.LevelMatchFilter(levelToMatch: 'ERROR', acceptOnMatch: true))
    ExceptionFile.addFilter(new org.apache.log4j.varia.DenyAllFilter())

    error 'org.codehaus.groovy.grails.web.servlet',         // controllers
            'org.codehaus.groovy.grails.web.pages',          // GSP
            'org.codehaus.groovy.grails.web.sitemesh',       // layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping',        // URL mapping
            'org.codehaus.groovy.grails.commons',            // core / classloading
            'org.codehaus.groovy.grails.plugins',            // plugins
            'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate',
            'grails.app.services.org.grails.plugin.resource',
            'grails.app.taglib.org.grails.plugin.resource',
            'grails.app.resourceMappers.org.grails.plugin.resource'

    off 'grails.app.services.org.grails.plugin.resource',
            'grails.app.taglib.org.grails.plugin.resource',
            'grails.app.resourceMappers.org.grails.plugin.resource'
}
grails.gorm.failOnError=true

security {
    shiro {
        authc.required = false
        filter.config = """
[filters]
# HTTP Basic authentication
restUsernamePasswordAuth = com.app.ef.security.MyRestUsernamePasswordAuthenticationFilter
restTokenAuth = com.app.ef.security.MyRestTokenAuthenticationFilter

[urls]

/auth/login= noSessionCreation
/auth/signOut = noSessionCreation, restTokenAuth
/auth/** = noSessionCreation, restUsernamePasswordAuth
/user/** = noSessionCreation, restTokenAuth
/user/permission = noSessionCreation, restTokenAuth
/role/** = noSessionCreation, restTokenAuth
/permission/** = noSessionCreation, restTokenAuth
/js/** = noSessionCreation
/**.html = noSessionCreation , restTokenAuth
"""
    }
}



if(grails.util.Environment.current.name.equalsIgnoreCase("production")) {
    // External DataSource location
    grails.config.locations = [
            "classpath:app-${grails.util.Environment.current.name}-config.properties"]

    //Externalizing Mediasense Attributes Configuration File
    grails.config.locations << "classpath:app-${grails.util.Environment.current.name}-mediasense-config.properties"

    //Externalizing Keylemon Attributes Configuration File
    grails.config.locations << "classpath:app-${grails.util.Environment.current.name}-keylemon-config.properties"

    //Externalizing Vertex Configurational Properties File
    grails.config.locations << "classpath:app-${grails.util.Environment.current.name}-vertex-config.properties"

    //Externalizing activemq Configurational Properties File
    grails.config.locations << "classpath:app-${grails.util.Environment.current.name}-activemq.properties"


}else {
    // External DataSource location
    grails.config.locations = [
            "file:app-${grails.util.Environment.current.name}-config.properties"]

    //Externalizing Mediasense Attributes Configuration File
    grails.config.locations << "file:app-${grails.util.Environment.current.name}-mediasense-config.properties"

    //Externalizing Keylemon Attributes Configuration File
    grails.config.locations << "file:app-${grails.util.Environment.current.name}-keylemon-config.properties"

    //Externalizing Vertex Configurational Properties File
    grails.config.locations << "file:app-${grails.util.Environment.current.name}-vertex-config.properties"

    //Externalizing activemq Configurational Properties File
    grails.config.locations << "file:app-${grails.util.Environment.current.name}-activemq.properties"
}