/**
 * Created by Sikander on 10/27/2014.
 */

angular.module('cbrapp')
    .controller('listUserController',function($timeout,toaster,$interval,$filter,$scope,$rootScope,$modal,$http,utils,$location,ngTableParams,$routeParams,crudOperationUser,$q,$translate) {
        $scope.users =[];
        $scope.hideShow = false;
        $scope.alerts = [];
        var inArray = Array.prototype.indexOf ?
            function (val, arr) {
                return arr.indexOf(val)
            } :
            function (val, arr) {
                var i = arr.length;
                while (i--) {
                    if (arr[i] === val) return i;
                }
                return -1;
            }
        $scope.isActiveStatus = function(column) {
            var blockLabel="Block";
            var activeLabel="Active";
            $translate("com.ef.cbr.user.label.block").then(function(value){
                blockLabel = value ;
            });
            $translate("com.ef.cbr.user.label.active").then(function(value){
                activeLabel = value ;
            });
            var def = $q.defer();
            var isActiveStatusArray = [];
            isActiveStatusArray.push({'id':'Yes',title:activeLabel});
            isActiveStatusArray.push({'id':'No',title:blockLabel});

            def.resolve(isActiveStatusArray);
            return def;
        };
        var load = function(){



            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    crudOperationUser.getList(params.$params).then( function(response){
                            $scope.users = response.usersList;
                            params.total(response.usersCount);
                            $defer.resolve(response.usersList);
}
                        ,function(error){
                            toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.loadingError"));
                         });
                    $scope.getCheckedUsers =function(){
                        return $filter('filter')($scope.users,{checked:true});
                    };

                    $scope.allChecked=function(){
                        return $scope.getCheckedUsers().length == $scope.users.length ;
                    };
                    $scope.checkAll = function(value){
                        angular.forEach($scope.users,function(bu){
                            bu.checked=value;
                        })
                    };


                    $scope.getCheckedIds = function(){
                        return _.pluck($scope.getCheckedUsers(),'id');
                    };

                }
            });
            $scope.deletedSelected =function(){
                //console.log($scope.getCheckedIds());
                var idsArray = [];
                idsArray = $scope.getCheckedIds();

                utils.confirm($filter("translate")("com.ef.cbr.label.areYouSure")).then(function(){
                    crudOperationUser.selectedDelete({ids:idsArray}
                    ).then(function(response){
                            toaster.pop('danger', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.user.noDeleted",{number:response.notDeletedUser}));
                            toaster.pop('success', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.user.deleted",{number:response.deletedUser}));
                            $scope.tableParams.reload();
                        },
                        function(error)
                        {
                            $location.path("/404");
                            toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), error.message);

                        })

                });

            } ;


        } ;

        load();
        $scope.alerts = null;
        $scope.editUser =function(id){
            $location.path('user/edit/'+id);
        } ;
        $scope.detailsPage =function(id){
            $location.path('user/detail/'+id);
        } ;
        $scope.editEntityUser = function(data,user,prop){
            user[prop]=data;
            var d= $q.defer();
            crudOperationUser.update({id:user.id,fullName:user.fullName,email:user.email,isActive:user.isActive}).then(function(response){
                toaster.pop('info', $filter("translate")("com.ef.cbr.label.user"),$filter("translate")("com.ef.cbr.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.ef.cbr.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.activateOrBlockUser = function(data,user,prop){
            user[prop]=data;
            var d= $q.defer();

            crudOperationUser.activeAndBlockUser({id:user.id,checked:user.isActive}).then(function(response){
                toaster.pop('info', $filter("translate")("com.ef.cbr.label.user"),$filter("translate")("com.ef.cbr.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.ef.cbr.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.deleteUser = function(index){
            var toDel = $scope.users[index];
            utils.confirm($filter("translate")("com.ef.cbr.label.areYouSure")).then(function(){
                crudOperationUser.get_api.delete(toDel,function(response){
                    $scope.users.splice(index, 1);
                    toaster.pop('success',  $filter("translate")("com.ef.cbr.label.user"),  $filter("translate")("com.ef.cbr.label.deleteSuccessfully"));

                },function(error){
                    toaster.pop('warning', $filter("translate")("com.ef.cbr.label.user"),$filter("translate")("com.ef.cbr.label.deleteFail"));

                });
            });


        } ;
        $scope.alerttover = function(id){
           $scope.hideShow = true;

        }
        $scope.alerttleave = function(id){
            $scope.hideShow = false;
        }

        $scope.detailUser = function(id) {
           var modalInstance = $modal.open({

                templateUrl: appBaseUrl+'/pages/user/detail.html',
                controller: 'detailUserController',
                size:'lg',
                backdrop:'false',
                resolve: {
                    id: function(){
                        return id;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.tableParams.reload();
            }, function () {
                $scope.tableParams.reload();
            });
        };
        $scope.blockUser = function(id){
          crudOperationUser.blockUser({id:id}).then(function(response){
              toaster.pop('success', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.user.blockedSuccessfully"));
              $scope.tableParams.reload();
          },function(error){
              if(error.status == "404"){
                  toaster.pop("error",$filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.notFound"));
                 // $scope.$parent.alerts.push({type:'danger',msg:'User not found'});
              }
              else if(error.status == "304")
                  toaster.pop("warning",$filter("translate")("com.ef.cbr.label.user"),$filter("translate")("com.ef.cbr.label.user.blockFail"));
              else{
                  toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.notFound"));
                  $location.path("/500");
              }
              $scope.tableParams.reload();
          });

        };
        $scope.activeUser = function(id){
            crudOperationUser.activeUser({id:id}).then(function(response){
                toaster.pop('success', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.user.activatedSuccessfully"));
                $scope.tableParams.reload();
            },function(error){
                if(error.status == "404"){
                    toaster.pop("error",$filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.notFound"));
                    // $scope.$parent.alerts.push({type:'danger',msg:'User not found'});
                }
                else if(error.status == "304")
                    toaster.pop("warning",$filter("translate")("com.ef.cbr.label.user"),$filter("translate")("com.ef.cbr.label.user.activeFail"));
                else{
                    toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.notFound"));
                    $location.path("/500");
                }
                $scope.tableParams.reload();
            });

        };

    });
