/**
 * Created by Umar on 11/10/14.
 */

angular.module('cbrapp')
    .controller('detailUserController',function($rootScope,$scope,$http,crudOperationUser,$q,$location,toaster,$routeParams,$filter,ngTableParams,$modal){
        $scope.userId = $routeParams['id'] ;
        crudOperationUser.show({id:$scope.userId}).then(function(response){

                     $scope.user = response;

             },function(error){
                // $modalInstance.close();
                 if(error.status == "404")
                      $location.path("/404");
                 else if(error.status == "500")
                     $location.path("/500");
                 /*else
                     $location.path("/400");*/
             });

        $scope.editEntityUser = function(data,user,prop){
            user[prop]=data;
            var d= $q.defer();

            crudOperationUser.update({id:user.id,fullName:user.fullName,email:user.email,isActive:user.isActive}).then(function(response){
                toaster.pop('info', $filter("translate")("com.ef.cbr.label.user"),$filter("translate")("com.ef.cbr.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.ef.cbr.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.activateOrBlockUser = function(data,user,prop){
            user[prop]=data;
            var d= $q.defer();

            crudOperationUser.activeAndBlockUser({id:user.id,checked:user.isActive}).then(function(response){
                toaster.pop('info', $filter("translate")("com.ef.cbr.label.user"),$filter("translate")("com.ef.cbr.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.ef.cbr.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.changePassword = function(id) {
            //   console.log(id);
            var modalInstance = $modal.open({

                templateUrl: appBaseUrl+'/pages/user/changePassword.html',
                controller: 'changePasswordCtrl',
                size:'md',
                backdrop:'false',
                resolve: {
                    id: function(){
                        return id;
                    }
                }
            });


        };
    })
    .controller('userDetailPermissionCtrl',function($scope,$http,crudOperationUser,$q,$location,toaster,$routeParams,$filter,ngTableParams){
       $scope.userId = $routeParams['id'] ;
        $scope.currentPermission = function(){
            $scope.currentPermissions = [];
            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    crudOperationUser.viewPermissions(params.$params,{id:$scope.userId}).then( function(response){
                            $scope.userPermissionList = response.userPermissionList;
                            $scope.permissionList = response.permissionList;
                            params.total(response.permissionListCount);
                            $scope.roleUserPermission = response.roleUserPermission;
                            $scope.checkedAndUncheckedUserPermission();
                            $scope.checkedAndUncheckedUserRolePermission();
                            $scope.permissionList =_.sortBy($scope.permissionList,['disabled','checked'])
                            $defer.resolve($scope.permissionList);
                        }
                        ,function(error){
                            toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.loadingError"));
                        });
                    $scope.getCheckedUsers =function(){
                        return $filter('filter')($scope.users,{checked:true});
                    };

                    $scope.allChecked=function(){
                        return $scope.getCheckedUsers().length == $scope.users.length ;
                    };
                    $scope.checkAll = function(value){
                        angular.forEach($scope.users,function(bu){
                            bu.checked=value;
                        })
                    };
                    $scope.getCheckedIds = function(){
                        return _.pluck($scope.getCheckedUsers(),'id');
                    };

                }
            });

        };
        $scope.checkedAndUncheckedUserPermission  = function(){
            angular.forEach($scope.userPermissionList,function(u_value,u_key){
                angular.forEach($scope.permissionList,function(value,key){
                    if(value['id'] == u_value['id'])
                        $scope.permissionList[key].checked = true;
                })
            })
        };
        $scope.checkedAndUncheckedUserRolePermission = function(){
            angular.forEach($scope.roleUserPermission,function(u_value,u_key){
                angular.forEach($scope.permissionList,function(value,key){
                    if(value['id'] == u_value['id'])
                        $scope.permissionList[key].disabled = true;

                })
            })
        };
        $scope.addOrRevokePermission = function(permissionId,checkedValue){
            crudOperationUser.assignAndRevokePermission({userId:$scope.userId,permissionId:permissionId,checked:checkedValue}).then(function(response){
                    if(checkedValue == true)
                        toaster.pop("success",$filter("translate")("com.ef.cbr.label.user"),$filter("translate")("com.ef.cbr.user.label.assignPermission"));
                    else
                        toaster.pop("info",$filter("translate")("com.ef.cbr.label.user"),$filter("translate")("com.ef.cbr.user.label.revokePermission"));
                }
                ,function(error){

                })
        }
    })
    .controller('userDetailRoleCtrl',function($scope,$http,crudOperationUser,$q,$location,toaster,$routeParams,$filter,ngTableParams){
        $scope.userId = $routeParams['id'] ;
        $scope.roleDetail = function(id){
          $location.path('/role/detail/'+id);
        }
        $scope.currentRole = function(){
            $scope.currentPermissions = [];
            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    crudOperationUser.viewRoles(params.$params,{id:$scope.userId}).then( function(response){
                            $scope.roleList = response.roleList;
                            $scope.userRoleList = response.userRoleList;
                            params.total(response.roleListTotal);
                            $scope.checkedAndUncheckedUserRole();
                            $scope.roleList =_.sortBy($scope.roleList,'checked')
                            $defer.resolve($scope.roleList);
                        }
                        ,function(error){
                            //   $scope.alerts.push({ type: 'danger', msg:'Error in Loading list'});
                            toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.loadingError"));
                            //  alertHandel.setValues({ type: 'danger', msg:'Error in Loading list'});

                        });
                    $scope.getCheckedUsers =function(){
                        return $filter('filter')($scope.users,{checked:true});
                    };

                    $scope.allChecked=function(){
                        return $scope.getCheckedUsers().length == $scope.users.length ;
                    };
                    $scope.checkAll = function(value){
                        angular.forEach($scope.users,function(bu){
                            bu.checked=value;
                        })
                    };
                    $scope.getCheckedIds = function(){
                        return _.pluck($scope.getCheckedUsers(),'id');
                    };

                }
            });

        };
        $scope.checkedAndUncheckedUserRole  = function(){
            angular.forEach($scope.userRoleList,function(u_value,u_key){
                angular.forEach($scope.roleList,function(value,key){
                    if(value['id'] == u_value['id'])
                        $scope.roleList[key].checked = true;
                })
            })
        };
        $scope.addOrRevokeRole =  function(roleId,checkedValue){
            crudOperationUser.assignAndRevokeRole({userId:$scope.userId,roleId:roleId,checked:checkedValue}).then(function(response){
                    if(checkedValue == true)
                        toaster.pop("success",$filter("translate")("com.ef.cbr.label.user"),$filter("translate")("com.ef.cbr.user.label.assignPermission"));
                    else
                        toaster.pop("info",$filter("translate")("com.ef.cbr.label.user"),$filter("translate")("com.ef.cbr.user.label.revokePermission"));
                }
                ,function(error){

                })
        }
    })
    .controller('changePasswordCtrl',function(id,$scope,crudOperationUser,toaster,$modalInstance,$filter,$rootScope){
       $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
        $scope.errorMessage = function(){
          return $scope.update_error || null;
        };
        $scope.changeUserPassword = function(){
            $scope.userId = id;
            crudOperationUser.changePassword({id:$scope.userId,oldPassword:$scope.user.oldPassword,newPassword:$scope.user.newPassword})
                .then(function(success){
                      if(success.status.name == "OK"){
                        $scope.cancel();
                        toaster.pop('success', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")(success.message));
                    }
                    else {
                          $scope.update_error = success.message;
                          toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")(success.message));
                      }


                },function(error){
                    toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")(error.message));

                })
        };
    })