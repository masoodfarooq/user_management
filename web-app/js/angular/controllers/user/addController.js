/**
 * Created by sikraan on 10/28/2014.
 */

 angular.module('cbrapp')
  .controller('addNewUserController',function($scope,$rootScope,$location,$http,crudOperationUser,toaster,$filter){

    $scope.addNew = function(){
        $location.path('/user/create');
    }

    $scope.viewUserList = function(){
        $location.path('/user/index');
    }
    $scope.alerts = [];
    $scope.saveUser = function(invalid){

        if(invalid)
            return;
        if($scope.user.email=="valid")
            $scope.user.email = undefined;
        var params = {username:$scope.user.username,password:$scope.user.password,email:$scope.user.email,fullName:$scope.user.fullName,isActive:$scope.user.isActive};
        crudOperationUser.get_api.save(params,function(response){
          //  $scope.$parent.alerts.push({ type: 'success', msg: 'User successfully created',timeout:5000});
            toaster.pop('info', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.createSuccessfully"));
            $location.path('/user/list');

        },function(error){
            var errors = [];
            errors = error.data.errors;
            if(error.status == "404")
                $location.path("/404");
            else if(error.status == "500")
                $location.path("/500");
            else{
                for(err in errors){
                    if(errors[err]["field"] == "username")
                    toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                    if(errors[err]["field"] == "email")
                        toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                }

            }

        });
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

    }
  }) ;


