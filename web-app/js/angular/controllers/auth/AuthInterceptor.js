/**
 * Created by Umar on 12/26/14.
 */
angular.module('cbrapp.auth')
    .factory('AuthInterceptor',function($rootScope,$q,AUTH_EVENTS){
        return{
            responseError:function(response){
                if (response.status === 401) {
                     if(response.data == "Permission Denied")
                     {
                         $rootScope.$broadcast(AUTH_EVENTS.permissionDenied,
                             response);
                     }
                     else if(response.data.status == "token_error"){
                         $rootScope.$broadcast(AUTH_EVENTS.tokenExpired,
                             response);
                     }
                    else{
                         $rootScope.$broadcast(AUTH_EVENTS.notAuthorized,
                             response);
                     }

                }
                if (response.status === 403) {
                    $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated,
                        response);
                }
                if (response.status === 404) {
                    $rootScope.$broadcast(AUTH_EVENTS.notFound,
                        response);
                }
                if (response.status === 419 || response.status === 440) {
                    $rootScope.$broadcast(AUTH_EVENTS.sessionTimeout,
                        response);
                }
                if (response.status === 306) {
                    $rootScope.$broadcast(AUTH_EVENTS.sessionTimeout,
                        response);
                }
                if (response.status === 500) {
                    $rootScope.$broadcast(AUTH_EVENTS.internalError,
                        response);
                }
                return $q.reject(response);
            }
        }
    });
