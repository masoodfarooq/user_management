/**
 * Created by Umar on 12/26/14.
 */
angular.module('cbrapp.auth')
    .factory('AuthService',function(AUTH_EVENTS,$rootScope,$location,$http,Session,$q){
      var  hideSideMenu = function(){
         $('.left-side').removeClass("collapse-left");
          $(".right-side").removeClass("strech");
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
           $(".loginHideShow").addClass("hidden");
           $(".signInHideShow").removeClass("hidden");

        }
        var serverAuthentication = function(){
            var defer = $q.defer();
            var username = window.localStorage.getItem("username") || window.sessionStorage.getItem("username");

            $http ({
                method:'GET',
                url:appBaseUrl+'/user/isAuthenticate',
                params:{username:username}
            }).success(
                function(data,status,header,config){
                    if(data.status == "error"){
                        $rootScope.$broadcast(AUTH_EVENTS.tokenExpired);
                        Session.destroy();
                        hideSideMenu();
                        window.location.href=appBaseUrl+"/";
                    }
                    else{
                        Session.update(data);
                        showSideMenu();
                        defer.resolve();
                    }

                })
                .error(function(data,status,header,config){
                    Session.destroy();
                    hideSideMenu();
                    defer.reject(data);
                    window.location.href=appBaseUrl+"/";
                }) ;
            return defer.promise;
        };
      var  showSideMenu= function(){
            $('.left-side').removeClass("collapse-left");
            $(".right-side").removeClass("strech");
          $(".loginHideShow").removeClass("hidden");
          $(".signInHideShow").addClass("hidden");
        }
        return{
           signIn:function(credentials){
               var deffer = $q.defer();
                $http({
                   method:'POST',
                   url:appBaseUrl+'/auth/signIn',
                   params:{username:credentials.username,password:credentials.password}
               }).success(function(data,status,header,config){
                       if(data.status == "error"){
                           hideSideMenu();
                           $rootScope.loginError = "error";
                       }
                       else if(data.status == "isNotActive"){
                           hideSideMenu();
                           $rootScope.loginError = "isNotActive";
                       }
                       else{
                           $rootScope.loginError=undefined;
                           Session.create(data,credentials.remember_me);
                           serverAuthentication().then(function(){
                               console.log($rootScope._user.userPermissions);
                               showSideMenu();
                               deffer.resolve();
                              $location.path('/index');

                           });
                       }

                   }).error(function(data,status,header,config){
                      deffer.reject(data);
                    });
              return deffer.promise;
           }     ,
           signOut:function(){
               var username = window.localStorage.getItem("username") || window.sessionStorage.getItem("username");
               return $http({
                   method:"POST",
                   url:appBaseUrl+'/auth/signOut',
                   params:{username:username}
                   }).success(function(data,status,header,config){
                       hideSideMenu();
                       Session.destroy();
                       $location.path("/login");

                   }).error(function(data,status,header,config){
                       $location.path("/login");
                   })
           },
           isAuthenticate:function(){
               var username = window.localStorage.username || window.sessionStorage.username
                return !!username ;
             }  ,
           serverAuthenticate:serverAuthentication,
            showSideMenu:showSideMenu,
           hideSideMenu:hideSideMenu


        }
    })

