/**
 * Created by Umar on 12/26/14.
 */
angular.module('cbrapp.auth')
    .factory('Session',function($rootScope,$http){
        var username= undefined;
        var userEmail= undefined;
        var userId= undefined;
        var userToken= undefined;
        var userPermissions= undefined;
       return{
            create:function(data,rememberMe){
                username= data.username;
                userEmail= data.email;
                userId= data.id;
                userToken= data.token;
                $rootScope._user= data;
                $rootScope.remember_me = rememberMe;
                if(rememberMe){
                    window.localStorage.setItem("username",data.username);// = JSON.stringify(data);
                    window.localStorage.setItem("token",data.token);// = JSON.stringify(data);
                    window.localStorage.setItem("userEmail",data.email);// = JSON.stringify(data);
                }
                else{
                    window.sessionStorage.setItem("token", data.token);
                    window.sessionStorage.setItem("username", data.username);
                    window.sessionStorage.setItem("email", data.email);

                }
                $http.defaults.headers.common["X-AUTH-TOKEN"] = data.token;
            },
            username:function(){
                return username ||  $rootScope._user? $rootScope._user.username:"" ;
            },
            userEmail:function(){
                return userEmail || $rootScope._user?$rootScope._user.email: "";
            },
            userId:function(){
                return userId || $rootScope._user?$rootScope._user.id :"";
            },
            userToken:function(){
                return userToken || $rootScope._user ? $rootScope._user.token :"";
            },
           update:function(data){
               $rootScope._user= data;
               username= data.username;
               userEmail= data.email;
               userId= data.id;
               userPermissions = data.userPermissions;
           },
            destroy : function () {
            window.localStorage.removeItem("username");
            window.localStorage.removeItem("token");
            window.localStorage.removeItem("userEmail");
            window.sessionStorage.removeItem("username");
            window.sessionStorage.removeItem("token");
            window.sessionStorage.removeItem("email");
            username = null;
            userEmail = null;
            userId = null;
            userToken = null;
            $rootScope._user = null;
               // window.location.href = appBaseUrl+'/'
        },
           getUser:function(){
             return  {username:username,email:userEmail,id:userId}
            },
           setAuthToken:function(){
               $http.defaults.headers.common["X-AUTH-TOKEN"] = window.localStorage.getItem("token") || window.sessionStorage.getItem("token");
           }



        }
    })
