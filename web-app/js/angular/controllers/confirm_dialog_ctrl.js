angular.module('cbrapp').controller('dialogController', ['$scope', '$location', '$modalInstance', 'dialog_data','$rootScope','$translate','$filter',
//Copyrighted : Ahmad Ali
//Email : ahmadalibaloch@gmail.com
//Lisence: MIT Lisence
function ($scope, $location, $modalInstance, dialog_data,$rootScope,$translate,$filter) {
    $scope.action = dialog_data.action;
    $scope.object = dialog_data.object;
    $scope.message = dialog_data.message;
    $scope.positive_txt = dialog_data.positive_txt ? dialog_data.positive_txt : $filter("translate")("com.ef.cbr.label.yes");
    $scope.negative_txt = dialog_data.negative_txt ? dialog_data.negative_txt : $filter("translate")("com.ef.cbr.label.no");

    $scope.negative = function () {
        $modalInstance.dismiss('noo');
    };
    $scope.positive = function () {
        $modalInstance.close('yes');
    }
}]);
