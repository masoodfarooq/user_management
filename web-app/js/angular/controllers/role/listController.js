/**
 * Created by Umar on 12/13/14.
 */
angular.module('cbrapp')
    .controller('listRoleController',function(toaster,$filter,$scope,$rootScope,$http,utils,$location,ngTableParams,$routeParams,crudOperationRole,$q,$translate){
        $scope.roles = [];
        var inArray = Array.prototype.indexOf ?
            function (val, arr) {
                return arr.indexOf(val)
            } :
            function (val, arr) {
                var i = arr.length;
                while (i--) {
                    if (arr[i] === val) return i;
                }
                return -1;
            };
        var load = function(){
            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    crudOperationRole.getRoleList(params.$params).then( function(response){
                            $scope.roles = response.rolesList;
                            params.total(response.rolesCount);
                            $defer.resolve(response.rolesList);
                        }
                        ,function(error){
                            toaster.pop('error', $filter("translate")("com.ef.cbr.label.role"), $filter("translate")("com.ef.cbr.label.loadingError"));
                             });
                    $scope.getCheckedRoles =function(){
                        return $filter('filter')($scope.roles,{checked:true});
                    };

                    $scope.allChecked=function(){
                        return $scope.getCheckedRoles().length == $scope.roles.length ;
                    };
                    $scope.checkAll = function(value){
                        angular.forEach($scope.roles,function(bu){
                            bu.checked=value;
                        })
                    };
                    $scope.getCheckedIds = function(){
                        return _.pluck($scope.getCheckedRoles(),'id');
                    };

                }
            });
            $scope.reFreshList = function(){
                $scope.tableParams.reload();
            };
            $scope.deletedSelected =function(){
                //console.log($scope.getCheckedIds());
                var idsArray = [];
                idsArray = $scope.getCheckedIds();

                utils.confirm($filter("translate")("com.ef.cbr.label.areYouSure")).then(function(){
                    crudOperationRole.deletedSelected({ids:idsArray}
                    ).then(function(response){
                            toaster.pop('danger', $filter("translate")("com.ef.cbr.label.role"), $filter("translate")("com.ef.cbr.label.noDeleted",{number:response.notDeletedContact}));
                            toaster.pop('success', $filter("translate")("com.ef.cbr.label.role"), $filter("translate")("com.ef.cbr.label.deleted",{number:response.deletedRole}));
                            $scope.tableParams.reload();
                        },
                        function(error)
                        {
                            $location.path("/404");
                            toaster.pop('error', $filter("translate")("com.ef.cbr.label.role"), error.message);
                        })
                });
            } ;
        } ;

        load();
        $scope.editEntityRole = function(data,role,prop){
            role[prop]=data;
            var d= $q.defer();
            crudOperationRole.update({id:role.id,name:role.name,description:role.description}).then(function(response){
                toaster.pop('info', $filter("translate")("com.ef.cbr.label.role"),$filter("translate")("com.ef.cbr.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    if(errors[err]["rejected-value"].length < 5)
                    toaster.pop('error', $filter("translate")("com.ef.cbr.label.role"), $filter("translate")("com.ef.cbr.label.rang",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                    else
                    toaster.pop('error', $filter("translate")("com.ef.cbr.label.role"), $filter("translate")("com.ef.cbr.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                }
                d.resolve( $filter("translate")("com.ef.cbr.label.updateFail"));


            });

            return d.promise;
        };
        $scope.deleteRole = function(index){
            var toDel = $scope.roles[index];
            utils.confirm($filter("translate")("com.ef.cbr.label.areYouSure")).then(function(){
                crudOperationRole.get_api.delete(toDel,function(response){
                    $scope.roles.splice(index, 1);
                    toaster.pop('success',  $filter("translate")("com.ef.cbr.label.role"),  $filter("translate")("com.ef.cbr.label.deleteSuccessfully"));

                },function(error){
                    toaster.pop('warning', $filter("translate")("com.ef.cbr.label.role"),$filter("translate")("com.ef.cbr.label.deleteFail"));

                });
            });
        };
        $scope.addNew = function(){
            $location.path("#/role/create");
        };
        $scope.detailsPage = function(id){
            $location.path("/role/detail/"+id);
        };
    })

