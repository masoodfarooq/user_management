/**
 * Created by Umar on 12/13/14.
 */
angular.module('cbrapp')
    .controller('addNewRoleController',function($scope,$rootScope,$location,$http,crudOperationRole,toaster,$filter){

        $scope.addNew = function(){
            $location.path('/role/create');
        }

        $scope.viewRoleList = function(){
            $location.path('/role/index');
        }
        $scope.alerts = [];
        $scope.saveRole = function(invalid){
            if(invalid)
                return;
             var params = {name:$scope.role.name,description:$scope.role.description};
            crudOperationRole.get_api.save(params,function(response){
                toaster.pop('info', $filter("translate")("com.ef.cbr.label.role"), $filter("translate")("com.ef.cbr.label.createSuccessfully"));
                $location.path('/role/list');

            },function(error){
                var errors = [];
                errors = error.data.errors;
                if(error.status == "404")
                    $location.path("/404");
                else if(error.status == "500")
                    $location.path("/500");
                else{
                    for(err in errors){
                        if(errors[err]["field"] == "name")
                            toaster.pop('error', $filter("translate")("com.ef.cbr.label.role"), $filter("translate")("com.ef.cbr.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                    }

                }

            });
            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };

        }
    }) ;

