/**
 * Created by Umar on 12/14/14.
 */
angular.module('cbrapp')
    .controller('detailRoleController',function($scope,$http,crudOperationRole,$q,$location,toaster,$routeParams,$filter,ngTableParams){
        $scope.roleId = $routeParams['id'];
        $scope.permissionChange = function(){
            console.log($scope.valueChanged);
            console.log("mu mmmm");
            return $scope.valueChanged;
        }
        crudOperationRole.show({id:$scope.roleId}).then(function(response){
            $scope.role = response;
            // console.log($scope.user);
        },function(error){
            // $modalInstance.close();
            if(error.status == "404")
                $location.path("/404");
            else if(error.status == "500")
                $location.path("/500");
            /*else
             $location.path("/400");*/
        });
        $scope.editEntityRole = function(data,role,prop){
            role[prop]=data;
            var d= $q.defer();
            crudOperationRole.update({id:role.id,name:role.name,description:role.description}).then(function(response){
                toaster.pop('info', $filter("translate")("com.ef.cbr.label.role"),$filter("translate")("com.ef.cbr.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    if(errors[err]["rejected-value"].length < 5)
                        toaster.pop('error', $filter("translate")("com.ef.cbr.label.role"), $filter("translate")("com.ef.cbr.label.rang",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                    else
                        toaster.pop('error', $filter("translate")("com.ef.cbr.label.role"), $filter("translate")("com.ef.cbr.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                }
                d.resolve( $filter("translate")("com.ef.cbr.label.updateFail"));


            });

            return d.promise;
        };


    })
    .controller('detailRolePermissionCtrl',function($scope,$http,crudOperationRole,$q,$location,toaster,$routeParams,$filter,ngTableParams){
        $scope.roleId = $routeParams['id'];

        $scope.currentPermission = function(){
            $scope.currentPermissions = [];
            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    crudOperationRole.viewPermissions(params.$params,{id:$scope.roleId}).then( function(response){
                            $scope.rolePermissionList = response.rolePermissionList;
                            $scope.permissionList = response.permissionList;
                            params.total(response.permissionListTotal);
                            $scope.checkedAndUncheckedRolePermission();
                            $scope.permissionList =_.sortBy($scope.permissionList,'checked')
                            $defer.resolve($scope.permissionList);
                        }
                        ,function(error){
                            //   $scope.alerts.push({ type: 'danger', msg:'Error in Loading list'});
                            toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.loadingError"));
                            //  alertHandel.setValues({ type: 'danger', msg:'Error in Loading list'});

                        });
                }
            });

        };
        $scope.checkedAndUncheckedRolePermission = function(){
            angular.forEach($scope.rolePermissionList,function(u_value,u_key){
                angular.forEach($scope.permissionList,function(value,key){
                    if(value['id'] == u_value['id'])
                        $scope.permissionList[key].checked = true;

                })
            })
        };
        $scope.addOrRevokePermission = function(permissionId,checkedValue){
            crudOperationRole.assignAndRevokePermissions({roleId:$scope.roleId,permissionId:permissionId,checked:checkedValue}).then(function(response){
                    if(checkedValue == true) {
                         toaster.pop("success",$filter("translate")("com.ef.cbr.label.role"),$filter("translate")("com.ef.cbr.user.label.assignPermission"));
                    }
                    else{
                       toaster.pop("info",$filter("translate")("com.ef.cbr.label.role"),$filter("translate")("com.ef.cbr.user.label.revokePermission"));
                    }

                }
                ,function(error){
                    toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.loadingError"));
                })
        }
    })
    .controller('detailRoleUserCtrl',function($scope,$http,crudOperationRole,$q,$location,toaster,$routeParams,$filter,ngTableParams){
        $scope.roleId = $routeParams['id'];
        $scope.userDetail = function(id){
            $location.path('/user/detail/'+id);
        }
        $scope.currentMember = function(){
            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    crudOperationRole.viewUsers(params.$params,{id:$scope.roleId}).then( function(response){
                            $scope.roleUserList = response.roleUserList;
                            $scope.userList = response.userList;
                            params.total(response.userListTotal);
                            $scope.checkedAndUncheckedRoleUser();
                            $scope.userList = _.sortBy($scope.userList,'checked');
                            $defer.resolve($scope.userList);
                        }
                        ,function(error){
                            //   $scope.alerts.push({ type: 'danger', msg:'Error in Loading list'});
                            toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.loadingError"));
                            //  alertHandel.setValues({ type: 'danger', msg:'Error in Loading list'});

                        });
                }
            });
        };
        $scope.checkedAndUncheckedRoleUser = function(){
            angular.forEach($scope.roleUserList,function(value,key){
                angular.forEach($scope.userList,function(u_value,u_key){
                    if(value['id'] == u_value['id'])
                        $scope.userList[u_key].checked = true;

                })
            })

        }
        $scope.addOrRevokeUser=function(userId,checkedValue){
            crudOperationRole.assignAndRevokeUsers({roleId:$scope.roleId,userId:userId,checked:checkedValue}).then(function(response){
                    if(checkedValue == true)
                        toaster.pop("success",$filter("translate")("com.ef.cbr.label.role"),$filter("translate")("com.ef.cbr.user.label.assignUser"));
                    else
                        toaster.pop("info",$filter("translate")("com.ef.cbr.label.role"),$filter("translate")("com.ef.cbr.user.label.revokeUser"));
                }
                ,function(error){
                    toaster.pop('error', $filter("translate")("com.ef.cbr.label.role"), $filter("translate")("com.ef.cbr.label.loadingError"));
                })
        }
    })
