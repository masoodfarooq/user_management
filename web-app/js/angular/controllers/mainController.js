/**
 * Created by Umar on 12/26/14.
 */
 angular.module('cbrapp')
     .controller('ApplicationController', function (PermissionService,toaster,Session,AUTH_EVENTS,AuthService,$scope,$location, $filter, $rootScope,$interval,$translate,$http) {
         $scope.isAuthorized = AuthService.isAuthenticate();
         $scope.isAuthorized1 = AuthService.isAuthenticate;
         if($scope.isAuthorized == false) {
             AuthService.hideSideMenu();
              $location.path("/login");
         }

         else {
             var currentPath = $location.path();
             Session.setAuthToken();
             AuthService.serverAuthenticate().then(function(){
                 $scope.username = Session.username();
                 AuthService.showSideMenu();
                 $location.path(currentPath);
                 $scope.hasPermission = function(permission,id){
                     if(id != undefined)
                     permission = permission.concat(":"+id);

                     return PermissionService.hasPermission(permission);
                 };
             });


         }
         $scope.isAuthorizedIndex = function(){
           if(AuthService.isAuthenticate() == false)
               $location.path("/login");
         };
         $scope.hasPermission = function(permission,id){
              if(id != undefined)
                 permission = permission.concat(":"+id);

             return PermissionService.hasPermission(permission);
         };
         $scope.getUsername = function(){
             return Session.username();
         } ;
         $scope.getUserId = function(){
             return Session.userId();
         };
         $scope.logout = function () {
             AuthService.signOut();
         };
         $scope.profilePage = function(){
             $location.path("/user/detail/"+$scope.getUserId());
         }
         //appBaseUrl = "/grails-scaffolding";
         $scope.alerts = [];
         $scope.$on(AUTH_EVENTS.notAuthenticated, function () {
             $location.path('Auth/401');
             $location.hash('top');
             AuthService.hideSideMenu();
            // window.location.href=appBaseUrl+"/";
             Session.destroy();
         });
         $scope.$on(AUTH_EVENTS.notAuthorized, function () {
             $location.path('/404');
         });
         $scope.$on(AUTH_EVENTS.permissionDenied, function () {
             toaster.pop('info', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.PermissionDenied"));

         });
         $scope.$on(AUTH_EVENTS.tokenExpired, function () {
             toaster.pop('error', $filter("translate")("com.ef.cbr.label.user"), $filter("translate")("com.ef.cbr.label.TokenExpired"));
             $location.path("/login");
             Session.destroy();
             AuthService.hideSideMenu();

         });
         $scope.$on(AUTH_EVENTS.notFound, function () {
             $location.path('/404');
         });
         $scope.$on(AUTH_EVENTS.sessionTimeout, function () {
             $location.path('Auth/419');
         });
         $scope.$on(AUTH_EVENTS.internalError, function () {
             $location.path('Auth/500');
         });
         // var newLength = $scope.alerts.length;
         $scope.closeAlert = function(index) {
             $scope.alerts.splice(index, 1);
         };
         $scope.translateKey = function(value){

             return $filter("translate")(value);
         }
         $scope.changeLanguage = function (langKey) {
             $translate.use(langKey);
         };
         $scope.currentLang = function () {
             return  $translate.use();
         };
         $scope.$watch('search', function (search) {
             $scope.searchMenu(search);
         });
         $scope.searchMenu = function (searchText) {
             //written on 25-09-2014
             //by : ahmadalibaloch@gmail.com
             //get elements
             var searchMenu = angular.element('.search-menu');
             var normalMenu = angular.element('.normal-menu');
             if (searchText && searchText.length > 0) {
                 //set displays
                 normalMenu.css("display", "none");
                 searchMenu.css("display", "block");
                 //get menu items
                 var menuItems = angular.element('.normal-menu .treeview li');
                 var menuItems1 = angular.element('.normal-menu > li');
                 var menuItems2 = angular.element('.normal-menu > .treeview');


                 //add main menu items too
                 menuItems = menuItems.add(menuItems1);

                 //remove headings
                 menuItems = menuItems.remove(menuItems2);


                 //search and add to searchMenu
                 var searchedMenuItems = [];
                 menuItems.each(function (ind, elem) {
                     var spans = elem.getElementsByTagName("span");
                     if (spans && spans.length > 0)
                         if (spans[0].textContent.trim().toLowerCase().indexOf(searchText.toLowerCase()) != -1)
                             searchedMenuItems.push($(this).clone());
                 });
                 //setbody
                 searchMenu.html(searchedMenuItems);
             }
             else {
                 normalMenu.css("display", "block");
                 searchMenu.css("display", "none");
             }
         };
     })
