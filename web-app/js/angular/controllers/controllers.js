//var cbrappControllers = angular.module('cbrappControllers', ['ngTable']);

/*cbrappControllers.controller('ngTableCtrl', function($scope, ngTableParams) {
    var data = [{name: "Moroni", age: 50},
        {name: "Tiancum", age: 43},
        {name: "Jacob", age: 27},
        {name: "Nephi", age: 29},
        {name: "Enos", age: 34},
        {name: "Tiancum", age: 43},
        {name: "Jacob", age: 27},
        {name: "Nephi", age: 29},
        {name: "Enos", age: 34},
        {name: "Tiancum", age: 43},
        {name: "Jacob", age: 27},
        {name: "Nephi", age: 29},
        {name: "Enos", age: 34},
        {name: "Tiancum", age: 43},
        {name: "Jacob", age: 27},
        {name: "Nephi", age: 29},
        {name: "Enos", age: 34}];

    $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10           // count per page
    }, {
        total: data.length, // length of data
        getData: function($defer, params) {
            $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
});
cbrappControllers.controller('ngTableCtrlAdv', function($scope, $filter,ngTableParams) {
    var data = [{name: "Moroni", age: 50,status:'active'},
        {name: "Tiancum", age: 43,status:'active'},
        {name: "Jacob", age: 27,status:'inactive'},
        {name: "Nephi", age: 29,status:'active'},
        {name: "Enos", age: 34,status:'online'},
        {name: "Tiancum", age: 43,status:'deleted'},
        {name: "Jacob", age: 27,status:'active'},
        {name: "Nephi", age: 29,status:'online'},
        {name: "Enos", age: 34,status:'inactive'},
        {name: "Tiancum", age: 43,status:'deleted'},
        {name: "Jacob", age: 27,status:'active'},
        {name: "Nephi", age: 29,status:'inactive'},
        {name: "Enos", age: 34,status:'active'},
        {name: "Tiancum", age: 43,status:'deleted'},
        {name: "Jacob", age: 27,status:'online'},
        {name: "Nephi", age: 29,status:'deleted'},
        {name: "Enos", age: 34,status:'deleted'}];
    $scope.colors = {'active':'blue','inactive':'gray','online':'green','deleted':'red'};
    $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10,           // count per page
        filter: {
            name: ''       // initial filter
        },
        sorting: {
            name: 'asc'     // initial sorting
        }
    }, {
        total: data.length, // length of data
        getData: function($defer, params) {
            //params = params.$params;
            //$defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            // use build-in angular filter
            var filteredData = params.filter() ?
                $filter('filter')(data, params.filter()) :
                data;
            var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                data;
            params.total(orderedData.length); // set total for recalc pagination
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
});
cbrappControllers.controller('ngTableCtrlXedit', function($scope, $filter,ngTableParams) {
    var data = [{name: "Moroni", age: 50},
        {name: "Tiancum", age: 43},
        {name: "Jacob", age: 27},
        {name: "Nephi", age: 29},
        {name: "Enos", age: 34},
        {name: "Tiancum", age: 43},
        {name: "Jacob", age: 27},
        {name: "Nephi", age: 29},
        {name: "Enos", age: 34},
        {name: "Tiancum", age: 43},
        {name: "Jacob", age: 27},
        {name: "Nephi", age: 29},
        {name: "Enos", age: 34},
        {name: "Tiancum", age: 43},
        {name: "Jacob", age: 27},
        {name: "Nephi", age: 29},
        {name: "Enos", age: 34}];
    $scope.ages = [
        {value: 1, text: '23'},
        {value: 2, text: '45'},
        {value: 3, text: '50'},
        {value: 4, text: '53'}
    ];
    $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10           // count per page
    }, {
        total: data.length, // length of data
        getData: function($defer, params) {
            $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
});*/

angular.module('cbrapp')
    .directive('passwordValidate', function() {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function(viewValue) {

                    scope.pwdValidLength = (viewValue && viewValue.length >= 8 ? 'valid' : undefined);
                    scope.pwdHasLetter = (viewValue && /[A-z]/.test(viewValue)) ? 'valid' : undefined;
                    scope.pwdHasNumber = (viewValue && /\d/.test(viewValue)) ? 'valid' : undefined;

                    if(scope.pwdValidLength && scope.pwdHasLetter && scope.pwdHasNumber) {
                        ctrl.$setValidity('pwd', true);
                        return viewValue;
                    } else {
                        ctrl.$setValidity('pwd', false);
                        return undefined;
                    }
                });
            }
        };
    })
    .directive('usernameValidate', function($http) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function(viewValue) {

                    //  var userExist = '';

                    scope.userValidLength = (viewValue && viewValue.length >= 5 ? 'valid' : undefined);
                    scope.userHasNumber = (viewValue &&  !/^[a-zA-Z0-9_]+$/.test(viewValue)) ?  undefined : 'valid';
                    // scope.userExist = scope.userExist;

                    //          console.log((viewValue &&  !/^[a-zA-Z0-9_]+$/.test(viewValue)));
                    if(scope.userValidLength && scope.userHasNumber) {
                        $http({method:'get',url:'/grails-scaffolding/user/usernameExist',params:{username:viewValue}})
                            .success(function(data, status, headers, config){
                                if(status == '202'){
                                    //  ctrl.$setValidity('user', true);
                                    scope.userExist = 'valid';
                                }

                                else{
                                    ctrl.$setValidity('user', false);
                                    scope.userExist = undefined;
                                }

                            }).error(function(data, status, headers, config){
                                ctrl.$setValidity('user', false);
                                scope.userExist = undefined;
                            });

                        ctrl.$setValidity('user', true);
                        return viewValue;
                    } else {
                        ctrl.$setValidity('user', false);
                        scope.userExist = undefined;
                        return undefined;
                    }

                });
            }
        };
    })
    .directive('nameValidate', function($http,$rootScope) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function(viewValue) {
                    scope.nameValidLength = (viewValue && viewValue.length >= 5 ? 'valid' : undefined);
                    if(scope.nameValidLength) {
                        $http({method:'get',url:appBaseUrl+attrs.urlName,params:{name:viewValue}})
                            .success(function(data, status, headers, config){
                                if(status == '202'){
                                    scope.nameExist = 'valid';
                                }
                                else{
                                    ctrl.$setValidity(attrs.className, false);
                                    scope.nameExist = undefined;
                                }

                            }).error(function(data, status, headers, config){
                                ctrl.$setValidity(attrs.className, false);
                                scope.nameExist = undefined;
                            });

                        ctrl.$setValidity(attrs.className, true);
                        return viewValue;
                    } else {
                        ctrl.$setValidity(attrs.className, false);
                        scope.nameExist = undefined;
                        return undefined;
                    }

                });
            }
        };
    })
    .directive('emailValidate', function($http) {
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z]+\.com$/i;

        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {

                ctrl.$parsers.unshift(function(viewValue) {
                    console.log(viewValue);
                    if(viewValue == "")
                    {
                        ctrl.$setValidity('email', true);
                        return 'valid';
                    }
                    else{
                        scope.emailValidator  = viewValue && EMAIL_REGEXP.test(viewValue) ? 'valid' : undefined;
                        if (scope.emailValidator) {
                            $http({method:'post',url:'/grails-scaffolding/user/emailExist',params:{email:viewValue}})
                                .success(function(data, status, headers, config){
                                    if(status == '202'){
                                        ctrl.$setValidity('email', true);
                                        scope.emailExist = 'valid';
                                    }

                                    else{
                                        ctrl.$setValidity('email', false);
                                        scope.emailExist = undefined;
                                    }

                                }).error(function(data, status, headers, config){
                                    ctrl.$setValidity('email', false);
                                    scope.emailExist = undefined;
                                });
                            return viewValue;
                        }
                        else{
                            ctrl.$setValidity('email', false);
                            //scope.emailExist = undefined;
                            return undefined;
                        }
                    }

                });
                // only apply the validator if ngModel is present and Angular has added the email validator


            }
        };
    })
    .directive('userDetailPage', function() {
        return {
            restrict:'EA',
            templateUrl: 'pages/user/detail.html'
        };
    })
    .directive('roleList', function() {
    return {
        restrict:'EA',
        templateUrl: 'pages/user/roleList.html'
    };
})
    .directive('userRoles', function() {
    return {
        restrict:'EA',
        templateUrl: 'pages/user/role.html'
    };
})
    .directive('permissionList', function() {
    return {
        restrict:'EA',
        templateUrl: 'pages/user/permissionList.html'
    }


})
    .directive('roleDetailPage', function() {
        return {
            restrict:'EA',
            templateUrl: 'pages/role/detail.html'
        };
    })
    .directive('rolePermissionList', function() {
    return {
        restrict:'EA',
        templateUrl: 'pages/role/permissionList.html'
    }


})
    .directive('roleMemberList', function() {
        return {
            restrict:'EA',
            templateUrl: 'pages/role/userList.html'
        }


    })