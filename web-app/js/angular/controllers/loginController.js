/**
 * Created by Umar on 12/23/14.
 */
angular.module('cbrapp')
    .controller('loginController',function($scope,$rootScope,$location,AuthService,$translate){
        $scope.login={};
        $scope.isAuthorized = AuthService.isAuthenticate();
        if( $scope.isAuthorized){
            $location.path();
        }
        $scope.loginError = function(){
            return $rootScope.loginError;
        } ;
        $scope.signIn = function(invalid,login){
            if(invalid)
            return;
            $scope.currentLang = function () {
                return  $translate.use();
            };
         AuthService.signIn({username:login.userId,password:login.password,remember_me:login.remember}).then(function(){
             console.log("reached");
         });


        }
    })
