/**
 * Created by Umar on 12/4/14.
 */
angular.module('cbrapp')
    .controller('listPermissionController',function($scope,$rootScope,toaster,ngTableParams,permissionOperation){
        $scope.permissions = [];
        $scope.load = function(){
            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    permissionOperation.getList(params.$params).then( function(response){
                            $scope.permissions = response.permissionList;
                            params.total(response.permissionCount);
                            $defer.resolve(response.permissionList);
                        }
                        ,function(error){
                            toaster.pop('error', "Permission", "Error in Loading list");
                        });
                }
            });
        };

       $scope.load();
    });
