/**
 * Created by Umar on 12/13/14.
 */
     angular.module('cbrapp')
         .factory('crudOperationRole',['$q','$resource','$rootScope',function($q,$resource,$rootScope){
             var roles = $resource(appBaseUrl+'/role/',{id:'@id'},{
                 get_list:{
                     method:'GET',
                     url: appBaseUrl+'/role/index/',
                     isArray:false
                 },
                 deletedSelected:{
                     method:'POST',
                     url:appBaseUrl+'/role/deleteSelected/'
                     ,params:{
                         ids:'@ids'
                     }
                 } ,
                 delete:{
                     method:'DELETE',
                     url:appBaseUrl+'/role/delete/'
                     ,params:{
                     id:'@id'
                    }
                 },
                 save:{
                     method:'POST',
                     url:appBaseUrl+'/role/save'
                 },
                 update:{"method":'PUT',
                     url:appBaseUrl+'/role/update/',
                     params:{
                         id:'@id'
                     }
                 },
                 show:{"method":'GET',
                     url:appBaseUrl+'/role/show/',
                     params:{
                         id:'@id'
                     }
                 } ,
                 viewPermissions:{
                     url:appBaseUrl+'/role/viewPermissions/',
                     params:{
                         id:'@id'
                     }
                 },
                 viewUsers:{
                     url:appBaseUrl+'/role/viewUsers/',
                     params:{
                         id:'@id'
                     }
                 },
                 assignAndRevokePermissions:{
                     url:appBaseUrl+'/role/assignAndRevokePermission/',
                     params:{
                         permissionId:'@permissionId',
                         roleId: '@roleId',
                         checked: '@checked'
                     }
                 },
                 assignAndRevokeUsers:{
                     url:appBaseUrl+'/role/assignAndRevokeUser/',
                     params:{
                         userId:'@userId',
                         roleId: '@roleId',
                         checked: '@checked'
                     }
                 }
             })
             return{
                 get_api:roles,
                 'getRoleList':  function(params){
                     var defered  = $q.defer();
                     roles.get_list(params,function(response){ defered.resolve(response); },
                         function(error)
                         { defered.reject(error);});
                     return defered.promise;
                 },
                 'deletedSelected':  function(ids){
                     var defered  = $q.defer();
                     roles.deletedSelected(ids,function(response){ defered.resolve(response); },
                         function(error)
                         { defered.reject(error);});
                     return defered.promise;
                 } ,
                 'delete':  function(id){
                     var defered  = $q.defer();
                     roles.delete(id,function(response){ defered.resolve(response); },
                         function(error)
                         { defered.reject(error);});
                     return defered.promise;
                 },
                 'update':  function(id,name,description){
                     var defered  = $q.defer();
                     roles.update(id,function(response){ defered.resolve(response); },
                         function(error)
                         { defered.reject(error);});
                     return defered.promise;
                 },
                 'show':  function(id){
                     var defered  = $q.defer();
                     roles.show(id,function(response){ defered.resolve(response); },
                         function(error)
                         { defered.reject(error);});
                     return defered.promise;
                 },
                 'viewPermissions':  function(params,id){
                     var defered  = $q.defer();
                     roles.viewPermissions(params,id,function(response){ defered.resolve(response); },
                         function(error)
                         { defered.reject(error);});
                     return defered.promise;
                 },
                 'viewUsers':  function(params,id){
                     var defered  = $q.defer();
                     roles.viewUsers(params,id,function(response){ defered.resolve(response); },
                         function(error)
                         { defered.reject(error);});
                     return defered.promise;
                 },
                 'assignAndRevokePermissions':  function(id){
                     var defered  = $q.defer();
                     roles.assignAndRevokePermissions(id,function(response){ defered.resolve(response); },
                         function(error)
                         { defered.reject(error);});
                     return defered.promise;
                 } ,
                 'assignAndRevokeUsers':  function(id){
                     var defered  = $q.defer();
                     roles.assignAndRevokeUsers(id,function(response){ defered.resolve(response); },
                         function(error)
                         { defered.reject(error);});
                     return defered.promise;
                 }


             }

         }]);
