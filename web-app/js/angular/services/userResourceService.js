angular.module("cbrapp")
    .factory('crudOperationUser',['$q','$resource','$rootScope',function($q,$resource,$rootScope){
        var users = $resource('../user/',{id : '@id' },  {
            update:{ method:'PUT',
                url:appBaseUrl+'/user/update/',
                params:{
                    id:'@id',
                    name:'@username',
                    email:'@email'
                }

            },
            get_count:{
                method:'GET',
                url: '../user/listCount/',
                isArray:false
            } ,
            get_list:{
                method:'GET',
                url: appBaseUrl+'/user/index/',
                isArray:false
            },
            show:{
                method:'GET',
                url:appBaseUrl+'/user/show/'
                ,params:{
                    id:'@id'
                }
            },
            activeAndBlockUser:{
                method:'POST',
                url:appBaseUrl+'/user/activateOrBlockUser/'
                ,params:{
                    id:'@id',
                    checked:'@checked'
                }
            },
            delete:{
                method:'DELETE',
                url:appBaseUrl+'/user/delete/'

            },
            save:{
                method:'POST',
                url:appBaseUrl+'/user/save'
            } ,
            deletedSelected:{
                method:'POST',
                url:appBaseUrl+'/user/deleteSelected/'
                ,params:{
                    ids:'@ids'
                }
            },
            viewPermissions:{
                method:'POST',
                url:appBaseUrl+'/user/viewPermissions/'
                ,params:{
                    ids:'@ids'
                }
            },
            viewRoles:{
                method:'POST',
                url:appBaseUrl+'/user/viewRoles/'
                ,params:{
                    ids:'@ids'
                }
            } ,
            assignAndRevokePermission:{
                method:'POST',
                url:appBaseUrl+'/user/assignAndRevokePermission/'
                ,params:{
                    userId:'@userId',
                    permissionId:'@permissionId',
                    checked:'@checked'
                }
            },
            assignAndRevokeRole:{
            method:'POST',
                url:appBaseUrl+'/user/assignAndRevokeRole/'
                ,params:{
                userId:'@userId',
                    roleId:'@roleId',
                    checked:'@checked'
            }

        },changePassword:{
                method:'POST',
                url:appBaseUrl+'/user/changePassword/'
                ,params:{
                    id:'@id',
                    currentPassword:'@oldPassword',
                    newPassword:'@newPassword',
                    confirmPassword:'@confirmPassword'
                }
            }

        })
        return{
            'update' : function(user){
                var defered  = $q.defer();
                users.update(user,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'selectedDelete' : function(user){
                var defered  = $q.defer();
                users.deletedSelected(user,function(response){defered.resolve(response);},function(error){ defered.reject(error);});
                return defered.promise;
            },
            'getList':  function(params){
                var defered  = $q.defer();
                users.get_list(params,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
             get_api:users,
            'getCount':  function(user){
                var defered  = $q.defer();
                users.get_count(function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            } ,

            'activeAndBlockUser':  function(id){
                var defered  = $q.defer();
                users.activeAndBlockUser(id,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }
            ,
            'show':function(id){
                var defered  = $q.defer();
                users.show(id,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            } ,
            'viewPermissions':function (params,id){
                var defered  = $q.defer();
                users.viewPermissions(params,id,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }   ,
            'viewRoles':function (params,id){
                var defered  = $q.defer();
                users.viewRoles(params,id,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            } ,
            'assignAndRevokePermission':function (userId){
                var defered  = $q.defer();
                users.assignAndRevokePermission(userId,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'assignAndRevokeRole':function (userId){
                var defered  = $q.defer();
                users.assignAndRevokeRole(userId,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }
            ,
            'changePassword':function (userData){
                var defered  = $q.defer();
                users.changePassword(userData,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }
        }
    }]);

