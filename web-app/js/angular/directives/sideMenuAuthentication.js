/**
 * Created by Umar on 1/2/15.
 */
angular.module('cbrapp.auth')
    .directive('sidemenuauth',function($compile,AuthService){
        return {
            restrict:'EA',
            priority:0,
            replace:true,
            templateUrl:'pages/template/sideMenu.html',
                scope:{
                'signedin':'='
            }              ,
            link : function(scope,element,attres){
              scope.$watch('signedin',function(nv,ov){
                  if(ov==nv)return;
                  $compile(element.contents())(scope);
              })   ;
        }

        }


    })
