angular.module('cbrapp')
//adds the strip at the first column of the table with a given color array against the values
/* $scope.colors = {'active':'blue','inactive':'gray','online':'green','deleted':'red'};
   now an attibute in this format will add colors accordingly.
   strip = {{user.status}} where status can be 'active', 'inactive', 'online', 'deleted'

*/
.directive('strip', function () {
    return {
        scope:{
            'strip':'='
        },
        link: function (scope, element, attrs) {
            scope.$watch('strip', function (newvalue,oldvalue) {
                if (!scope.colors)
                    scope.colors = { 'true': 'green', 'false': 'maroon' };
                var color = scope.colors[scope['strip']];
                element.css("width", "2%");
                element.css("height", "100%");
                element.css("background", color);
                element.css("position", "absolute");
                element.css("left", "0");
                element.css("top", "0");
                var td = element.parent();
                td.css("position", "relative")
                td.css("padding-left", "2%")
            });
            // var table = element.parent().parent().parent();
            //table.css("overflow", "hidden");
        }
    };
});
