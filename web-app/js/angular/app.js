angular.module('cbrapp.auth', []);

var cbrapp = angular.module('cbrapp', [
        'ngRoute',
  'cbrapp.auth',
  'toaster',
  'ngResource',
  'ui.bootstrap',
  'xeditable',
  'bootstrap-switch',
  'ngCookies',
  'angularMoment',
  'pascalprecht.translate',
  'ngTable'])
    .constant('AUTH_EVENTS', {
        loginSuccess: 'auth-login-success',
        loginFailed: 'auth-login-failed',
        logoutSuccess: 'auth-logout-success',
        sessionTimeout: 'auth-session-timeout',
        notAuthenticated: 'auth-not-authenticated',
        notAuthorized: 'auth-not-authorized',
        notFound: 'auth-not-found',
        internalError: 'auth-internal-error',
        permissionDenied: 'permission-denied',
        tokenExpired:'token-expired'


    })
.run(function(editableOptions,$http,$location) {
        editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
})
    .config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
	 when('/widgets', {
        templateUrl: 'pages/widgets.html'
      }).
	  //Charts
	   when('/login', {
        templateUrl: 'pages/auth/login.html'
      }).
        //user
	   when('/user/index/', {
        templateUrl: 'pages/user/list.html'
      }).
      when('/user/create', {
            templateUrl: 'pages/user/create.html'
      }).
      when('/user/list', {
         templateUrl: 'pages/user/list.html'
      }).when('/permission/list',{
         templateUrl:'pages/permission/list.html'
        }).
        when('/user/detail/:id',{
            templateUrl:'pages/user/userDetail.html'
        })
        .when('/inline', {
        templateUrl: 'pages/charts/inline.html'
      }).
        //Role
        when('/role/index/', {
            templateUrl: 'pages/role/list.html'
        }).
        when('/role/create', {
            templateUrl: 'pages/role/create.html'
        }).
        when('/role/detail/:id', {
            templateUrl: 'pages/role/roleDetail.html'
        }).
        when('/role/list', {
            templateUrl: 'pages/role/list.html'
        })
	  //UI
        .when('/general', {
        templateUrl: 'pages/UI/general.html'
      }).
	   when('/buttons', {
        templateUrl: 'pages/UI/buttons.html'
      }).
      when('/icons', {
        templateUrl: 'pages/UI/icons.html'
      }).
	   when('/jquery-ui', {
        templateUrl: 'pages/UI/jquery-ui.html'
      }).
	   when('/sliders', {
        templateUrl: 'pages/UI/sliders.html'
      }).
      when('/timeline', {
        templateUrl: 'pages/UI/timeline.html'
      }).
	   //Forms
	   when('/generalform', {
        templateUrl: 'pages/forms/general.html'
      }).
	   when('/editors', {
        templateUrl: 'pages/forms/editors.html'
      }).
      when('/advanced', {
        templateUrl: 'pages/forms/advanced.html'
      }).
	  //Tables
	  when('/simple', {
        templateUrl: 'pages/tables/simple.html'
      }).
      when('/data', {
        templateUrl: 'pages/tables/data.html'
      }).
	  when('/ngtable', {
        templateUrl: 'pages/tables/ngtable.html'
      }).
	  when('/ngtable-advanced', {
        templateUrl: 'pages/tables/ngtable-advanced.html'
      }).
	  when('/ngtable-xeditable', {
        templateUrl: 'pages/tables/ngtable-xeditable.html'
      }).
	  //Examples
	  when('/404', {
        templateUrl: 'pages/errors/404.html'
      }).
      when('/500', {
        templateUrl: 'pages/errors/500.html'
      }).
	  when('/blank', {
        templateUrl: 'pages/examples/blank.html'
      }).
	  when('/invoice', {
        templateUrl: 'pages/examples/invoice.html'
      }).
	  //other
	  when('/check', {
        templateUrl: 'check/'
      }).
      when('/check/test', {
        templateUrl: 'check/test'
      }).
      when('/mailbox', {
        templateUrl: 'pages/mailbox.html'
      }).
      otherwise({
        redirectTo: '/index',
		templateUrl: 'pages/home.html'
      });
  }])
    .config(['$httpProvider',function($httpProvider){
        $httpProvider.interceptors.push([
            '$injector', function ($injector) {
                return $injector.get('AuthInterceptor');
            }
        ]);
    }])
    .config(['$translateProvider',function($translateProvider){

        $translateProvider.useUrlLoader('/grails-scaffolding/translate');
        $translateProvider.preferredLanguage("en");

        //Remember language
        $translateProvider.useCookieStorage();
        $translateProvider.fallbackLanguage('en');
    }])



