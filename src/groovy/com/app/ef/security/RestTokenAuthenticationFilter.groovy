package com.app.ef.security
import com.ef.app.cbr.security.RestService
import grails.converters.JSON
import org.apache.shiro.authc.AuthenticationToken
import org.apache.shiro.web.filter.authc.AuthenticatingFilter
import org.apache.shiro.web.util.WebUtils

import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletResponse

abstract class RestTokenAuthenticationFilter extends AuthenticatingFilter {
    private RestService restService

    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) {
        return new RestToken(token: getToken(request))
    }

    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        //this request should be handled by onAccessDenied
        return false;
    }

    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) {
         JSON userData = [:]
        if (executeLogin(request, response)) {

            return true
        }
        else {
            String token = getToken(request)
            def message = getTokenInfo(token)

            userData = [
                    message:message,
                    status:'token_error'
            ]
            //failed to authenticate. Return 401
            HttpServletResponse httpResponse = WebUtils.toHttp(response);
            httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            httpResponse.writer << userData

            return false
        }
    }
     private getTokenInfo(String token){
         def user
         def username = getRestService().checkTokenUsername(token)
         if(username.equals("token_expired")){
             return "token_expired"
         }
         else if(username.equals("error")){
             return "error"
         }
         else{
             user = (!username) ?: User.findByUsername(username)
             if (!user) {
                 return "not_found"
             }
             if(!User.findByUsername(username)?.isActive){
                 return "not_active"
             }
         }
     }
    protected abstract String getToken(request)
    private getRestService() {
        if (!this.@restService) {
            this.@restService = SpringUtils.getBean("restService")
        }

        return this.@restService
    }
}

