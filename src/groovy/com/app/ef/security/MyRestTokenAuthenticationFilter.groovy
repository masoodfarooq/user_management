package com.app.ef.security

import org.apache.shiro.web.util.WebUtils

import javax.servlet.http.HttpServletRequest

class MyRestTokenAuthenticationFilter extends RestTokenAuthenticationFilter {

    protected String getToken(request) {
       // return request.headers["X-AUTH-TOKEN"]

        HttpServletRequest httpRequest = WebUtils.toHttp(request);

       return httpRequest.getHeader("X-AUTH-TOKEN")
    }
}
