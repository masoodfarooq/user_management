package com.app.ef.security

import com.ef.app.cbr.security.RestService
import grails.converters.JSON
import org.apache.shiro.authc.AuthenticationToken
import org.apache.shiro.authc.UsernamePasswordToken
import org.apache.shiro.web.filter.authc.AuthenticatingFilter
import org.apache.shiro.web.util.WebUtils

import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletResponse

abstract class RestUsernamePasswordAuthenticationFilter extends AuthenticatingFilter {

    private RestService restService

    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) {
        def username = getUsername(request)
        def password = getPassword(request)

        return new UsernamePasswordToken(username as String, password as String)
    }

    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        //this request should be handled by onAccessDenied
        return false;
    }
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) {
        JSON userData = [:]

        if (executeLogin(request, response)) {
            def username = getUsername(request)
            def user  = User.findByUsername(username)
            if(user.isActive){
                def token = generateAndSaveTokenForUser(username, request)

                userData = [
                        token:token,
                        username:user.username,
                        id:user.id,
                        email:user.email
                ]
                //return token as answer
                response.outputStream << userData
            }
            else{
                HttpServletResponse httpResponse = WebUtils.toHttp(response);
                httpResponse.setStatus(HttpServletResponse.SC_OK);
                userData =[
                        status:'isNotActive'
                ]
                httpResponse.writer << userData
            }

            return false
        }
        else {
             //failed to authenticate. Return 401
            HttpServletResponse httpResponse = WebUtils.toHttp(response);
            httpResponse.setStatus(HttpServletResponse.SC_OK);
            userData =[
                    status:'error'
            ]
            httpResponse.writer << userData

            return false;
        }
    }

    private generateAndSaveTokenForUser(username, request) {
        def token = null

         //generate unique token
        while (!token) {
            token = getRestService().generateToken(username, request)
        }
        return token
    }

    protected abstract String getUsername(request)

    protected abstract String getPassword(request)

    private getRestService() {
        if (!this.@restService) {
            this.@restService = SpringUtils.getBean("restService")
        }

        return this.@restService
    }

}
