package com.ef.apps.mediasense.Beans.LogInBeans

/**
 * Developer: Bilal Ahmed Yaseen
 * eMail: bilal.ahmed@expertflow.com
 **/

public class LoginRequestBean {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String generateJSON() {
        String u = getUsername()
        String body = "{\"requestParameters\": {\"username\":" + getUsername() + ",\"password\":" + getPassword() + "}}";
        return body;
    }
}