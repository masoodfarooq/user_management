package com.ef.apps.mediasense.recordings

class RecordingsTrackParticipant {

    private String participantStartDate;
    private String participantDuration;
    private String isConference;
    private String xRefCi;
    private String deviceId;
    private String deviceRef;

    static belongsTo = [sessionTrack:RecordingsTrack]

    static mapping = {
        participantStartDate column:'participantStartDate'
        participantDuration column:'participantDuration'
        isConference column:'isConference'
        xRefCi column:'xRefCi'
        deviceId column:'deviceId'
        deviceRef column:'deviceRef'
    }

    public String getDeviceRef() {
        return deviceRef;
    }

    public void setDeviceRef(String deviceReference) {
        this.deviceRef = deviceReference;
    }

    public String getParticipantStartDate() {
        return participantStartDate;
    }

    public void setParticipantStartDate(String participantStartDate) {
        this.participantStartDate = participantStartDate;
    }

    public String getParticipantDuration() {
        return participantDuration;
    }

    public void setParticipantDuration(String participantDuration) {
        this.participantDuration = participantDuration;
    }

    public String getIsConference() {
        return isConference;
    }

    public void setIsConference(String isConference) {
        this.isConference = isConference;
    }

    public String getxRefCi() {
        return xRefCi;
    }

    public void setxRefCi(String xRefCi) {
        this.xRefCi = xRefCi;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    static constraints = {
    }
}