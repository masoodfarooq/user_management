package com.ef.apps.mediasense.recordings

class RecordingsTrack {

    private String trackNumber;
    private String trackStartDate;
    private String trackDuration;
    private String trackMediaType;
    private String downloadUrl;
    private String trackCodec;

    static hasMany = [sessionTrackparticipant:RecordingsTrackParticipant]
    static belongsTo = [session:Recordings]

    static mapping = {

        //sessionTrackparticipant cascade: "all-delete-orphan"

        trackNumber column:'trackNumber'
        trackStartDate column:'trackStartDate'
        trackDuration column:'trackDuration'
        trackMediaType column:'trackMediaType'
        downloadUrl column:'downloadUrl'
        trackCodec column:'trackCodec'
    }

    public String getTrackCodec() {
        return trackCodec;
    }

    public void setTrackCodec(String trackCodec) {
        this.trackCodec = trackCodec;
    }

    public String getTrackNumber() {
        return trackNumber;
    }

    public void setTrackNumber(String trackNumber) {
        this.trackNumber = trackNumber;
    }

    public String getTrackStartDate() {
        return trackStartDate;
    }

    public void setTrackStartDate(String trackStartDate) {
        this.trackStartDate = trackStartDate;
    }

    public String getTrackDuration() {
        return trackDuration;
    }

    public void setTrackDuration(String trackDuration) {
        this.trackDuration = trackDuration;
    }

    public String getTrackMediaType() {
        return trackMediaType;
    }

    public void setTrackMediaType(String trackMediaType) {
        this.trackMediaType = trackMediaType;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    static constraints = {
    }
}