package com.ef.apps.mediasense.recordings

class Recordings {

    //Recordings Attributes
    private String sessionId;
    private Date sessionStartDate;
    private Date sessionEndDate;
    private long sessionDuration;
    public String sessionState;
    private String sessionCCID;
    private String callControllerIP;
    private String sessionCallControllerType;
    String tag;

    // Recordings URLs
    private String httpUrl;
    private String rtspUrl;
    private String convertedLinkURL;

    //Other Attributes
    private sessionAgentName;
    private sessionAgentID;

    //Participants Info
    private String parTo;
    private String parFrom;
    private String ciscoID;

    //Is it a combined session or part of any combined session
    private boolean isCombined;

    //Attribute indicating that either this recording has been downloaded successfully or not
    //private boolean downloadedSuccessfully;

    //I want to check the date and time when the record was inserted
    Date dateCreated

    //static hasMany = [sessionTracks:RecordingsTrack]

   // static belongsTo = [combinedSessions: Calls]

   // static auditable = true
    public Recordings() {

        this.sessionAgentName = "";
        this.sessionAgentID = "";
        this.parFrom = "";
        this.parTo = "";
        this.convertedLinkURL="";
        //this.downloadedSuccessfully = false;
    }

    public void setConvertedLinkURL(String convertedLink) {
        this.convertedLinkURL = convertedLink;
    }

    /*void setDownloadedSuccessfully(boolean downloadedSuccessfully) {
        this.downloadedSuccessfully = downloadedSuccessfully
    }

    boolean getDownloadedSuccessfully() {
        return downloadedSuccessfully
    }*/

    public String getConvertedLinkURL() {
        return this.convertedLinkURL;
    }

    public void setIsCombinedSession(boolean isCombined) {
        this.isCombined = isCombined;
    }

    public boolean getIsCombinedSession() {
        return this.isCombined;
    }

    public void setParTo(String called) {
        this.parTo = called;
    }

    public String getParTo() {
        return this.parTo
    }

    public void setCiscoID(String ciscoIDs) {
        this.ciscoID = ciscoIDs;
    }

    public String getCiscoID() {
        return this.ciscoID
    }

    public void setParFrom(String calling) {
        this.parFrom = calling;
    }

    public String getParFrom() {
        return this.parFrom
    }

    public void setsessionAgentName(String agentName) {
        this.sessionAgentName = agentName;
    }

    public String getsessionAgentName() {
        return this.sessionAgentName;
    }

    public void setsessionAgentID(String agentID) {
        this.sessionAgentID = agentID;
    }

    public String getsessionAgentID() {
        return this.sessionAgentID
    }

    public String getCallControllerIP() {
        return this.callControllerIP;
    }

    public void setCallControllerIP(String sessionCallControllerIP) {
        this.callControllerIP = sessionCallControllerIP;
    }

    public String getSessionCallControllerType() {
        return sessionCallControllerType;
    }

    public void setSessionCallControllerType(String sessionCallControllerType) {
        this.sessionCallControllerType = sessionCallControllerType;
    }

    public void setCcid(String sessionCCID) {
        this.sessionCCID = sessionCCID;
    }

    public String getCcid() {
        return sessionCCID;
    }

    public String getSessionTag() {
        return tag;
    }

    public void setSessionTag(String sessionTag) {
        this.tag = sessionTag;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Date getSessionStartDate() {
        return sessionStartDate;
    }

    public void setSessionStartDate(Date sessionStartDate) {
        this.sessionStartDate = sessionStartDate;
    }

    public Date getSessionEndDate() {
        return sessionEndDate;
    }

    public void setSessionEndDate(Date sessionEndDate) {
        this.sessionEndDate = sessionEndDate;
    }

    public long getSessionDuration() {
        return sessionDuration;
    }

    public void setSessionDuration(long sessionDuration) {
        this.sessionDuration = sessionDuration;
    }

    public String getSessionState() {
        return sessionState;
    }

    public void setSessionState(String sessionState) {
        this.sessionState = sessionState;
    }

    public String getHttpUrl() {
        return httpUrl;
    }

    public void setHttpUrl(String httpUrl) {
        this.httpUrl = httpUrl;
    }

    public String getRtspUrl() {
        return rtspUrl;
    }

    public void setRtspUrl(String rtspUrl) {
        this.rtspUrl = rtspUrl;
    }
}