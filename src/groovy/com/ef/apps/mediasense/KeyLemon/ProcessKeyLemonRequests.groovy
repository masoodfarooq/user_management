package com.ef.apps.mediasense.KeyLemon

import com.ef.apps.mediasense.ResponseMapper.KeyLemon.KeylemonRequestsResponseMapper
import com.ef.apps.mediasense.Utils.LogsManagement
import grails.util.Holders
import org.apache.commons.httpclient.HttpClient
import org.apache.commons.httpclient.methods.PostMethod

public class ProcessKeyLemonRequests {

    private HttpClient client;
    private PostMethod postMethod;

    private String user = null;
    private String key = null;

    def properties

    private final String recognizeVoiceURL;
    private final String keyLemonEntryPoint;
    private final String createVoiceModelURL;

    public ProcessKeyLemonRequests() {

        properties = Holders.config;

        this.key = properties["KEYLEMON_API_KEY"]
        this.user = properties["KEYLEMON_API_USER"]

        this.createVoiceModelURL = properties["CREATE_SPEAKER_MODEL"]
        this.recognizeVoiceURL = properties["RECOGNIZE_SPEAKER_VOICE"]
        this.keyLemonEntryPoint = properties["KEYLEMON_API_ENTRY_POINT"]

        this.postMethod = null;
        this.client = new HttpClient();
    }


    //This will create model from specified sample voice and will return the response of model creation
    // which contains ID of the generated mode
    public String createUserVoiceModel(String voiceFileURL, String modelName) {

        String voiceModelCreationResponse = "";

        try {

            String completeCreateVoiceModelURL = keyLemonEntryPoint + createVoiceModelURL;

            postMethod = new PostMethod(completeCreateVoiceModelURL);

            if (modelName != null && !modelName.equals("") && !modelName.isEmpty()) {
                postMethod.setParameter("name", modelName);
            }

            postMethod.setParameter("urls", voiceFileURL);

            postMethod.setParameter("user", this.user);
            postMethod.setParameter("key", this.key);

            client.executeMethod(postMethod);

            int responseCode = postMethod.getStatusCode();
            voiceModelCreationResponse = new String(postMethod.getResponseBody())

            //Response Code between 200 and 299 is considered as successful remaining others are unsuccessful
            if (responseCode < 200 || responseCode > 299) {
                LogsManagement.LogInfoMesg("Some Error Occurred While Generating Voice Model from Media File: " + voiceFileURL + ".\n Response Code: " + responseCode + ". Response Message: " + voiceModelCreationResponse)
                voiceModelCreationResponse = ""
            } else {
                LogsManagement.LogInfoMesg("Response Received While Generating Voice Model from Media File: " + voiceFileURL + " is: " + voiceModelCreationResponse)
            }

        } catch (Exception ex) {
            voiceModelCreationResponse = ""
            LogsManagement.LogExceptionMesg("Exception Occurred While Creating Voice Model for Media File: " + voiceFileURL, ex);
        }

        return voiceModelCreationResponse
    }



     //this method will perform voice recognition with the specified model
    public String recognizeVoiceFromGivenModel(String voiceToRecognize, String modelID) {

        String voiceRecognizitionResponse = "";

        LogsManagement.LogInfoMesg("INFO: Going to Recognize Media File: " + voiceToRecognize + " Against Model with ID: " + modelID)

        try {

            String completeCreateVoiceModelURL = keyLemonEntryPoint + recognizeVoiceURL;
            println "completeCreateViceModelURL: .>>> "+completeCreateVoiceModelURL

            postMethod = new PostMethod(completeCreateVoiceModelURL);

            postMethod.setParameter("models", modelID);
            postMethod.setParameter("urls", voiceToRecognize);

            postMethod.setParameter("user", this.user);
            postMethod.setParameter("key", this.key);
            println "modelID: "+modelID+" , voice to recognize: "+voiceToRecognize+ " , user: "+this.user+" , key: "+this.key
            client.executeMethod(postMethod);

            int responseCode = postMethod.getStatusCode();
            voiceRecognizitionResponse = new String(postMethod.getResponseBody())

            //Response Code between 200 and 299 is considered as successful remaining others are unsuccessful
            if (responseCode < 200 || responseCode > 299) {
                LogsManagement.LogExceptionMesg("Some Error Occurred While Recognize Media File: " + voiceToRecognize + " Against Model with ID: " + modelID + ": \n" + voiceRecognizitionResponse)
                voiceRecognizitionResponse = ""
            } else {
                LogsManagement.LogInfoMesg("Response Received While Recognize Media File: " + voiceToRecognize + " Against Model with ID: " + modelID + " is: " + voiceRecognizitionResponse)
            }

        } catch (Exception ex) {
            voiceRecognizitionResponse = ""
            LogsManagement.LogExceptionMesg("Exception Occurred While Recognize Media File: " + voiceToRecognize + " Against Model with ID: " + modelID, ex)
        }

        return voiceRecognizitionResponse
    }
}