package com.ef.apps.mediasense.Utils

/**
 * Developer: Bilal Ahmed Yaseen
 * eMail: bilal.ahmed@expertflow.com
 */

import org.apache.log4j.Logger

class LogsManagement {

    public static Logger logger = Logger.getLogger("rollingInfoFileAppender");

    //To log normal info messages
    public static void LogInfoMesg(String infoMesg) {
        //println "Info Message to Log: "+infoMesg;
        logger.info(infoMesg);
    }

    //To log Exceptions
    public static void LogExceptionMesg(String exceptionMesg, Throwable stackTrace) {

        //println "Exception to Log: "+ exceptionMesg

        if (stackTrace != null) {
            logger.error(exceptionMesg, stackTrace);
        } else {
            logger.error(exceptionMesg);
        }
    }

    //To log Exceptions
    public static void LogExceptionMesg(String exceptionMesg) {

        //println "Exception to Log: "+ exceptionMesg
        logger.error(exceptionMesg);
    }
}