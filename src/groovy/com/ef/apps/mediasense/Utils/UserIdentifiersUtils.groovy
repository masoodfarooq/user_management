package com.ef.apps.mediasense.Utils

class UserIdentifiersUtils {

    //This method will return the Unique Identifier that will be used for the user ID
    public synchronized static String generateUniqueUserID() {

        String generatedUserID = ""

        try {

            generatedUserID = UUID.randomUUID().toString()

            println "User's Unique Identifier Generated: " + generatedUserID

        } catch (Exception ex) {
            generatedUserID = ""
            LogsManagement.LogExceptionMesg("Exception Occurred While Generating Unique User ID: ", ex)
        }

        return generatedUserID
    }
}