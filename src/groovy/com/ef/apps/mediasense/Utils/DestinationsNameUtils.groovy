package com.ef.apps.mediasense.Utils

import grails.util.Holders

/**
 * Created by Biilal Ahmed Yaseen on 3/13/15.
 */
class DestinationsNameUtils {

    def properties = Holders.config;

    //This method will return the Name of the topic from which model creation notifications will be consumed
    public String getTopicNameForConsumeMessages() {

        String sourceName = properties['SOURCE_NAME']

        println "AMQ Source Name is: " + sourceName

        return sourceName
    }

    //This method will return the Name of the topic on which messages will be published
    public String getTopicNameToPublishMessages() {

        String destinationName = properties['DESTINATION_NAME']

        println "AMQ Destination Name is: " + destinationName

        return destinationName
    }
}
