package com.ef.apps.mediasense.Utils

import grails.util.Holders

class URLsFormatter {

    def properties = Holders.config;

    //This method will format Media Files Public URL
    public String generateMediaFilesPublicURL(String fileNameWithExtension) {

        String applicationName = properties['appName']
        String publicMachinePort = properties['PUBLIC_MACHINE_PORT']
        String publicMachineIPAddr = properties['PUBLIC_MACHINE_IP_ADDR']
        String mediaFilesPublicURLFormat = properties['MEDIA_FILE_PUBLIC_URL']
        String mediaFilesStorageDirectoryName = properties['MEDIA_FILES_STORAGE_DIRECTORY_NAME']
        mediaFilesPublicURLFormat = mediaFilesPublicURLFormat.replace("{APP_NAME}", applicationName)
        mediaFilesPublicURLFormat = mediaFilesPublicURLFormat.replace("{PUBLIC_MACHINE_PORT}", publicMachinePort)
        mediaFilesPublicURLFormat = mediaFilesPublicURLFormat.replace("{RECORDING_FILE_NAME}", fileNameWithExtension)
        mediaFilesPublicURLFormat = mediaFilesPublicURLFormat.replace("{PUBLIC_MACHINE_IP_ADDR}", publicMachineIPAddr)
        mediaFilesPublicURLFormat = mediaFilesPublicURLFormat.replace("{MEDIA_FILES_STORAGE_DIRECTORY_NAME}", mediaFilesStorageDirectoryName);


        return mediaFilesPublicURLFormat
    }

    //This Method will format Streaming URL
    public String getRecordingStreamingURL (String fileNameWithOutExtension) {

        String mediasenseUserName = properties['USERNAME'];
        //println "mediasenseUserName: >>> "+mediasenseUserName
        String mediasensePassword = properties['PASSWORD'];
        //println "mediasensePassword: >>>"+mediasensePassword
        String mediasenseServerIP = properties['SERVER_IP_ADDRESS'];
        //println "mediasenseServerIP: >>> "+mediasenseServerIP
        String RTSP_URL = properties['STREAMING_URL_FORMAT'];
        //println "RTSP_URL1: >>> "+RTSP_URL
        RTSP_URL = RTSP_URL.replace("{username}", mediasenseUserName)
        RTSP_URL = RTSP_URL.replace("{password}", mediasensePassword)
        RTSP_URL = RTSP_URL.replace("{SERVER_IP_ADDRESS}", mediasenseServerIP)
        RTSP_URL = RTSP_URL.replace("{RECORDING_FILE_NAME}", fileNameWithOutExtension)
        //println "RTSP_URL2: >>>> "+RTSP_URL
        return RTSP_URL
    }

    //This Method Will format Broker URL
    public String getBrokerURL() {

        String brokerURL = properties['BROKER_URL']

        String AMQServerIP = properties['ACTIVE_MQ_SERVER_IP']
        String AMQServerPort = properties['ACTIVE_MQ_SERVER_PORT']

        brokerURL = brokerURL.replace("{active_mq_server_ip}", AMQServerIP)
        brokerURL = brokerURL.replace("{active_mq_server_port}", AMQServerPort)

        println "Broker URL is: " + brokerURL

        return brokerURL
    }

    public String getDestinationName() {

        String destinationName = properties['DESTINATION_NAME']

        println "AMQ Destination Name is: " + destinationName

        return destinationName
    }
}