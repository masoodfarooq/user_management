package com.ef.apps.mediasense.ResponseMapper.KeyLemon

import com.ef.apps.mediasense.Utils.LogsManagement
import net.sf.json.JSONArray
import net.sf.json.JSONObject
import net.sf.json.JSONSerializer

/**
 * Created by Biilal Ahmed Yaseen on 2/19/15.
 */

class KeylemonRequestsResponseMapper {

    public static String fetchModelIDFromCreateVoiceModelResponse(String createVoiceModelResponse) {

        String modelID = "";

        try {

            JSONObject jsonObj = (JSONObject) JSONSerializer.toJSON(createVoiceModelResponse);

            modelID = jsonObj.optString("model_id")

        } catch (Exception ex) {
            modelID = ""
            LogsManagement.LogExceptionMesg("Exception Occurred while Parsing Voice Model Creation Response: " + createVoiceModelResponse, ex)
        }

        return modelID;
    }

    public static int fetchProbabilityFromRecognizeVoiceModelResponse(String recognizeVoiceModelResponse) {

        int probability = -1;

        //Sample JSON Response Model
        /*{
            "audios": [
                {
                    "audio_md5": "f2e27c6f3735f3a910859c7a75781ed5",
                    "results": [
                        {
                            "model_id": "692d1cb8-8626-423a-a789-d7bb3c103e85",
                            "name": "",
                            "score": 98
                        }
                    ]
                }
            ]
        }*/

        try {

            JSONObject json = (JSONObject) JSONSerializer.toJSON(recognizeVoiceModelResponse);

            JSONArray audios = json.optJSONArray("audios");

            JSONObject audioJSON = (JSONObject) JSONSerializer.toJSON(audios.get(0));

            JSONArray results = audioJSON.optJSONArray("results");

            JSONObject resultJSON = (JSONObject) JSONSerializer.toJSON(results.get(0))

            String matchingResult = resultJSON.optString("score")

            probability = Integer.parseInt(matchingResult)

        } catch (Exception ex) {
            probability = -1;
            LogsManagement.LogExceptionMesg("Exception Occurred while Parsing Voice Recognition Response: " + recognizeVoiceModelResponse, ex)
        }

        return probability;
    }
}