package com.ef.apps.mediasense.ResponseMapper

import com.ef.apps.mediasense.Utils.LogsManagement
import net.sf.json.JSONArray
import com.ef.apps.mediasense.recordings.Recordings
import com.ef.apps.mediasense.recordings.RecordingsTrack
import com.ef.apps.mediasense.recordings.RecordingsTrackParticipant
import net.sf.json.JSONException

/**
 * Developer: Bilal Ahmed Yaseen
 * eMail: bilal.ahmed@expertflow.com
 */
import net.sf.json.JSONObject
import net.sf.json.JSONSerializer

class SubscriptionEventsResponseMapper {

    public static Recordings MapSessionResponse(String jsonSessionResponse, long req_id) {

        String eventType = "";
        String eventAction = "";
        String forwardedEvents = "";

        //In this Object extracted information about session will be stored that will be returned to the calling function
        Recordings sessionResponse = new Recordings();

        try {

            JSONObject json = (JSONObject) JSONSerializer.toJSON(jsonSessionResponse);

            eventType = json.optString("eventType", "");
            eventAction = json.optString("eventAction", "");



            forwardedEvents = json.optString("forwardedEvent", "");

            JSONObject responseBodyJSON = (JSONObject) JSONSerializer.toJSON(json.optString("eventBody", ""));

            //Firstly, Extracting Recordings Information i.e. id, date, duration, state, ccid, callControllerIP

            //Extracting Recordings State
            String sessionState = responseBodyJSON.optString("sessionState", "");

            if (sessionState.equalsIgnoreCase("CLOSED_NORMAL")) {
                sessionState = "RECORDED";
            }

            if (sessionState.equalsIgnoreCase("CLOSED_ERROR")) {
                sessionState = "ERROR";
            }

            sessionResponse.setSessionState(sessionState);

            String duration;

            //Extracting Recordings's total Duration
            if (sessionResponse.getSessionState().equalsIgnoreCase("ACTIVE")) {
                sessionResponse.setSessionDuration(0);

            } else {
                duration = responseBodyJSON.optString("sessionDuration", "");

                long millisec = Long.parseLong(duration);
                sessionResponse.setSessionDuration(millisec);

                try {
                } catch (NumberFormatException e) {
                    //Just ignoring the exception
                    sessionResponse.setSessionDuration("");
                }
            }

            //Extracting Recordings's Starting Date
            String sessionStartDate = responseBodyJSON.optString("sessionStartDate", "");

            //Converting Date into Specific format such as Month Day, Year
            //Date formattedDate = TimeDatetoMillis.formatIntoDate(sessionStartDate);
            long millis = Long.parseLong(sessionStartDate);
            Date formattedDate = new Date(millis);

            sessionResponse.setSessionStartDate(formattedDate);

            //Setting Session's Ending Date and Time (If Session's completed)
            //Note: This was commented out because we were getting validation exceptions that recording endDate can't be null.
            //But now I have made changed in design to accomodate null in sessionEndDate
            if (!sessionResponse.getSessionState().equalsIgnoreCase("ACTIVE")) {

                long totalMillis = millis + sessionResponse.getSessionDuration();
                formattedDate = new Date(totalMillis);

                sessionResponse.setSessionEndDate(formattedDate);
            }

            //Setting Session's Start and End Time

            //Date sessionBeginTime = TimeDatetoMillis.formatIntoTime(sessionStartDate);
            //sessionResponse.setSessionBeginTime(sessionBeginTime);

            //Date sessionEndTime = TimeDatetoMillis.formatIntoLongTime(sessionStartDate, duration)
            //sessionResponse.setSessionEndTime(sessionEndTime);

            //Extracting Recordings's ID
            sessionResponse.setSessionId(responseBodyJSON.optString("sessionId", ""));

            //Extracting Recordings's CCID
            sessionResponse.setCcid(responseBodyJSON.optString("ccid", ""));

            //Extracting Recordings's Recordings Controller IP
            sessionResponse.setCallControllerIP(responseBodyJSON.optString("callControllerIP", ""));

            //Extracting Recordings's Recordings Controller Type
            sessionResponse.setSessionCallControllerType(responseBodyJSON.optString("callControllerType", ""));

            boolean attributesRetreived = false;

            try {
                JSONObject URLs = null;
                //Extracting URLs Information of Recordings
                try {
                    URLs = ((JSONObject) JSONSerializer.toJSON(responseBodyJSON.optString("urls")));
                } catch (GroovyCastException) {

                    attributesRetreived = true;
                    JSONArray URLArray = responseBodyJSON.optJSONArray("urls");
                    // When Closed Session is updated and Some URL's are added in that

                    for (Object url : URLArray) {

                        JSONObject URLArrayJSON = (JSONObject) JSONSerializer.toJSON(url);

                        if (URLArrayJSON.optString("rtspUrl", "").equals("")) {
                            //Extracting Recordings's HTTP URL
                            sessionResponse.setHttpUrl(URLArrayJSON.optString("httpUrl", ""));
                        } else {
                            //Extracting Recordings's RTSP URL
                            sessionResponse.setRtspUrl(URLArrayJSON.optString("rtspUrl", ""));
                        }
                    }
                }

                if (!attributesRetreived) {
                    //Extracting Recordings's RTSP URL
                    sessionResponse.setRtspUrl(URLs.optString("rtspUrl", ""));

                    //Extracting Recordings's HTTP URL
                    sessionResponse.setHttpUrl(URLs.optString("httpUrl", ""));
                }

            } catch (JSONException ex) {
                //If URLs are not given in the JSON Response

                //Setting Recordings's RTSP URL
                sessionResponse.setRtspUrl("");

                //Setting Recordings's HTTP URL
                sessionResponse.setHttpUrl("");
            }

            //Extracting Recordings's Track's Information
            JSONArray tracks = responseBodyJSON.optJSONArray("tracks");

            String trackNumber = "";
            String ciscoID = "";
            String deviceReference = "";

            int tempCallRefID = -1;
            //In Case If tracks information is not provided in the JSON Response
            try {
                for (Object track : tracks) {

                    //To maintain the information of each track in a session
                    RecordingsTrack sessionTrack = new RecordingsTrack();

                    //Converting track object to generic JSON Object
                    JSONObject trackJSON = (JSONObject) JSONSerializer.toJSON(track);

                    sessionTrack.setTrackCodec(trackJSON.optString("codec", ""));
                    sessionTrack.setDownloadUrl(trackJSON.optString("downloadUrl", ""));
                    trackNumber = trackJSON.optString("trackNumber", "")
                    sessionTrack.setTrackNumber(trackNumber);
                    sessionTrack.setTrackDuration(trackJSON.optString("trackDuration", ""));
                    sessionTrack.setTrackStartDate(trackJSON.optString("trackStartDate", ""));
                    sessionTrack.setTrackMediaType(trackJSON.optString("trackMediaType", ""));

                    try {
                        //Extracting Recordings's Track's Information
                        JSONArray participants = trackJSON.optJSONArray("participants");

                        for (Object participant : participants) {
                            //JSONObject participants = (JSONObject) JSONSerializer.toJSON(trackJSON.optString("participants", ""));

                            //To store Participant information of session's particular track
                            RecordingsTrackParticipant trackParticipant = new RecordingsTrackParticipant();

                            JSONObject participantJSON = (JSONObject) JSONSerializer.toJSON(participant);

                            ciscoID = participantJSON.optString("xRefCi", "");
                            trackParticipant.setxRefCi(ciscoID);
                            trackParticipant.setDeviceId(participantJSON.optString("deviceId", ""));
                            deviceReference = participantJSON.optString("deviceRef", "");
                            trackParticipant.setDeviceRef(deviceReference);
                            trackParticipant.setIsConference(participantJSON.optString("isConference", ""));
                            trackParticipant.setParticipantDuration(participantJSON.optString("participantDuration", ""));
                            trackParticipant.setParticipantStartDate(participantJSON.optString("participantStartDate", ""));

                            //If this is a Conference Call, then we need to change its device reference number
                            if (trackParticipant.isConference.equalsIgnoreCase("true")) {
                                //If we will change "Confrn" text here, we will have to change in "Process Recording Service"
                                //file too where it finds the agent information of the called number associated with call
                                deviceReference = "Confrn";
                            }

                            if (tempCallRefID == -1) {
                                tempCallRefID = Integer.parseInt(ciscoID);
                                //Setting xRefCi of calling number
                                sessionResponse.setCiscoID(ciscoID);
                                sessionResponse.setParTo(deviceReference);
                            } else {
                                int newCallRefID = Integer.parseInt(ciscoID);

                                //The numerically smaller xRefCi almost always refers to the calling party's track
                                if (tempCallRefID < newCallRefID) {
                                    String parFrom = sessionResponse.getParTo();
                                    sessionResponse.setParFrom(parFrom);
                                    sessionResponse.setParTo(deviceReference);
                                    sessionResponse.setCiscoID(ciscoID);
                                } else {
                                    sessionResponse.setParFrom(deviceReference);
                                }
                            }

                            //Adding participant information in the track
                          //  sessionTrack.addToSessionTrackparticipant(trackParticipant);
                        }
                    } catch (JSONException ex) {
                        //Just Ignore if no participant is mentioned in track
                    }

                    //Adding track information in session
                   // sessionResponse.addToSessionTracks(sessionTrack)
                }
            } catch (JSONException ex) {
                //Just Ignore if no track is mentioned in session
            }

        } catch (Exception ex) {
            LogsManagement.LogExceptionMesg("Exception While Parsing MediaSense Notification with Request ID : " + req_id + " | " + jsonSessionResponse, ex);
            sessionResponse = null;
        }

        return sessionResponse;
    }

}
