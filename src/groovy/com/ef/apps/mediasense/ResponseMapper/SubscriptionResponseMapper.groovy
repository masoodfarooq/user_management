package com.ef.apps.mediasense.ResponseMapper

/**
 * Developer: Bilal Ahmed Yaseen
 * eMail: bilal.ahmed@expertflow.com
 */

import grails.util.Holders
import net.sf.json.JSONObject
import net.sf.json.JSONSerializer
import com.ef.apps.mediasense.Utils.LogsManagement

class SubscriptionResponseMapper {

    public String MapSubscriptionResponse(String jsonSubscriptionResponse) {

        try {
            def properties = Holders.config

            JSONObject json = (JSONObject) JSONSerializer.toJSON(jsonSubscriptionResponse);

            //Checking status of recently executed API call
            String responseCode = json.optString("responseCode", "")

            String sessionExpiryCode = properties['SESSION_EXPIRED'];
            String successfullApiCallResponseCode = properties["SUCCESSFUL_API_CALL_RESPONSE_CODE"];

            //If the session of the client we are using expired due to any reason
            if (responseCode.equals(sessionExpiryCode)) {
                return responseCode;
            }

            //If request not got processed successfully, then we just log details of failure unless reason is not expiration of session.
            int intResponseCode = Integer.parseInt(responseCode);

            //Successful Response varies from 2000 to 2006
            if (intResponseCode >= 2000 && intResponseCode <= 2006) {

                // 2005 - Success: The subscription with the given subscriptionUri already exists.
                if (intResponseCode != 2000 && intResponseCode != 2005) {
                    LogsManagement.LogInfoMesg("Response of Subscription for Mediasense Notifications: " + responseCode);
                }

                JSONObject responseBodyJSON = (JSONObject) JSONSerializer.toJSON(json.optString("responseBody", ""));

                //Extracting Subscription ID
                String subscriptionID = "";
                subscriptionID = responseBodyJSON.optString("subscriptionId", "");

                return subscriptionID

            } else {
                String failureReason = json.optString("responseMessage", "");
                LogsManagement.LogInfoMesg("ERROR: Invalid Response Received while Subscribing Mediasense Recordings Notifications: " + responseCode);
                return "";
            }

        } catch (Exception ex) {
            LogsManagement.LogExceptionMesg("Exception While Parsing MediaSense Notification Subscription Response: " + jsonSubscriptionResponse + " | "+ex.getMessage(), ex)
            return "";
        }
    }

    public static String MapSubscriptionVerificationResponse(String jsonSubscriptionResponse) {

        def properties = Holders.config

        JSONObject json = (JSONObject) JSONSerializer.toJSON(jsonSubscriptionResponse);

        //Checking status of recently executed API call
        String responseCode = json.optString("responseCode", "")

        String sessionExpiryCode = properties['SESSION_EXPIRED']

        if (responseCode.equals(sessionExpiryCode)) {
            return "Session Failed!!!"
        }

        return ""
    }
}
