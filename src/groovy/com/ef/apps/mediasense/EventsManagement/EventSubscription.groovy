package com.ef.apps.mediasense.EventsManagement

/**
 * Developer: Bilal Ahmed Yaseen
 * eMail: bilal.ahmed@expertflow.com
 **/

import grails.util.Holders;

public class EventSubscription {

    private String serverPort = "";
    private String serverAddress = "";
    private String subscriptionURL = "";

    private static def properties

    public EventSubscription() {

        properties = Holders.config;

        subscriptionURL = properties['SUBSCRIPTION_URL'];

        serverAddress = properties["SUBSCRIPTION_SERVER_IP_ADRRESS"];
        serverPort = properties["SUBSCRIPTION_SERVER_PORT"];

        subscriptionURL = subscriptionURL.replace("{mediasense_subscription_server_ip}", serverAddress);
        subscriptionURL = subscriptionURL.replace("{mediasense_subscription_server_port}", serverPort);

        //System.out.println(subscriptionURL+" : "+serverAddress+" : "+serverPort);
    }

    public String JsonSubscription(String eventToSubscribe) {

        String jsonStr = "{\n" +
        "\"requestParameters\": {\n" +
        "\"subscriptionType\": \"http\",\n" +
        "\"subscriptionUri\": \""+subscriptionURL+"\",\n" +
        "\"subscriptionFilters\": [\""+eventToSubscribe+"\"]\n" +
        "}\n" +
        "}";

        return jsonStr;
    }

    public String JsonUnSubscription(String subscriptionID, String eventToSubscribe) {

        String jsonStr = "{\n" +
        "requestParameters: {\n" +
        "\"subscriptionId\": \""+subscriptionID+"\",\n" +
        "\"subscriptionFilters\": [\""+eventToSubscribe+"\"]\n" +
        "}\n" +
        "}";

        return jsonStr;
    }

    public String JsonVerifySubscription(String subscriptionID) {

        String jsonStr = "{\n" +
        "requestParameters: {\n" +
        "\"subscriptionId\": \""+subscriptionID+"\",\n" +
        "}\n" +
        "}";

        return jsonStr;
    }
}