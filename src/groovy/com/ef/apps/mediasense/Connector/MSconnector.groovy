package com.ef.apps.mediasense.Connector

import com.ef.apps.mediasense.Utils.LogsManagement

/**
 * Developer: Bilal Ahmed Yaseen
 * eMail: bilal.ahmed@expertflow.com
 **/

import grails.util.Holders
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import com.ef.apps.mediasense.Beans.LogInBeans.LoginRequestBean;
import org.apache.commons.httpclient.methods.StringRequestEntity;

//Singleton Class
public class MSconnector {

    private String userName = "";
    private String password = "";
    private String serverPort = "";
    private String serverAddress = "";

    static def properties;
    private String baseURL = "";
    private String contentType = "";

    private HttpClient client;
    private String JSessionID = "";

    private static MSconnector connector

    private MSconnector() {

        properties = Holders.config;

        serverAddress = properties["SERVER_IP_ADDRESS"];
        serverPort = properties["SERVER_PORT"];

        userName = properties["USERNAME"];
        password = properties["PASSWORD"];

        contentType = "application/json";
        baseURL = properties["BASE_URL"];

        baseURL = baseURL.replace("{mediasense_server_ip}", serverAddress);
        baseURL = baseURL.replace("{mediasense_server_port}", serverPort);

        client = new HttpClient();
    }

    public static MSconnector getInstance(String code) {

        if (connector == null) {

            LogsManagement.LogInfoMesg("Creating new Client for Mediasense Server.");

            connector = new MSconnector();
            connector.login();
            return connector;
        } else{

            //In case if session has been expired due to any reason then we need to LogIn again
            //We have placed Empty check because we will be sending some code if we intended to LogIn again to MediaSense User otherwise,
            //We will be sending an empty string.
            if ((code != null) && (!code.equals("") || !code.isEmpty())) {

                if(code.equals(properties['SESSION_EXPIRED'])) {

                    LogsManagement.LogInfoMesg("INFO: API User's Session has been Expired... Attempting to LogIn Again: " + code);

                    connector = new MSconnector();
                    connector.login();
                    return connector
                }
            }

            return connector;
        }
    }

    public String login() {

        String LogInURL = properties["SIGNIN"];
        String URL = baseURL + LogInURL;

        try {

            //to prepare request body required by SignIn Process
            LoginRequestBean lrb = new LoginRequestBean();
            lrb.setUsername(userName);
            lrb.setPassword(password);
            String requestBody = lrb.generateJSON();

            PostMethod postMethod = new PostMethod(URL);

            //Adding required parameters
            postMethod.setParameter("username", userName);
            postMethod.setParameter("password", password);
            postMethod.setParameter("Content-Type", contentType);

            //1. Actual Body, 2. Content Type, 3. Character Set
            postMethod.setRequestEntity(new StringRequestEntity(requestBody, contentType, null));

            client.executeMethod(postMethod);

            Header responseHeader = postMethod.getResponseHeader("Location");

            //If status Code is '302' this means that our request has been re-directed
            if (postMethod.getStatusCode() == 302) {

                //New location(IP) where our request has been re-directed
                String location = responseHeader.toString().split(" ")[1];
                //System.out.println("location of redirect: " + location);
                //1. URL, 2. Escaped String or Not, 3. Character Set
                postMethod.setURI(new org.apache.commons.httpclient.URI(location, true, null));
                client.executeMethod(postMethod);
            }

            // Retrieving Jsession id returned by SignIn API Recordings and that will be used in consequent calls
            Header head = postMethod.getResponseHeader("Set-Cookie");

            if (head != null) {

                String header_value = head.toString();
                System.out.println(header_value);

                String[] session_variables = header_value.split(";");
                JSessionID = ((session_variables[0].split("="))[1]);

                if (JSessionID == null || JSessionID.equals("") || JSessionID.isEmpty()) {
                    LogsManagement.LogExceptionMesg("ERROR: JSession ID not Found while Making API User LogIn in Mediasense Server. Header Value is: " + header_value)
                } else {
                    LogsManagement.LogInfoMesg("INFO: API User Successfully LoggedIn in Mediasense Server with JSession ID: " + JSessionID)
                }
            } else {
                JSessionID = "";
                LogsManagement.LogExceptionMesg("ERROR: JSession ID not Found while Making API User LogIn in Mediasense Server. Response is: " + postMethod.getResponseBodyAsString())
            }

        } catch (ConnectException connectionException) {
            LogsManagement.LogExceptionMesg("Exception While Making Connection to Mediasense Server to LogIn:" + connectionException.getMessage(), null)
            JSessionID = ""
        } catch (NoRouteToHostException internetConnectionException) {
            LogsManagement.LogExceptionMesg("Exception While Connecting to Internet while Making Connection to Mediasense Server to LogIn:" + internetConnectionException.getMessage(), null)
            JSessionID = ""
        } catch (Exception ex) {
            LogsManagement.LogExceptionMesg("Exception While Making Attempt to LogIn:" + ex.getMessage(), ex)
            JSessionID = ""
        }

        return JSessionID;
    }

    public void logOut() throws IOException {

        String LogOutURL = properties["SIGNOUT"];
        String URL = baseURL + LogOutURL;

        PostMethod postMethod = new PostMethod(URL);
        client.executeMethod(postMethod);
    }

    public HttpClient getMediaSenseClient() {
        return client;
    }

    public String getJSessionID() {
        return JSessionID;
    }
}